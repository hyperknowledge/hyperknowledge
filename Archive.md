# Hyperknowledge project

## Goals

We want to establish a Web of Knowledge. This allows to see how any
idea is connected with other ideas in debates, through evidence,
citations, influence, consequences, etc.

There have been, and still are many attempts to create such a Web of
Knowledge, and rather than be one more such attempt on our own, we want
to help unify the forces of knowledge workers, both individuals and
organizations, into an interoperable mesh of knowledge where everyone
can contribute. We also want to make it practical to develop knowledge
applications that consume and expose knowledge on this mesh.

The protocol supports interoperability of knowledge at personal and
organisational levels and federation of knowledge sources so that
knowledge can be synthesised to augment our ability to learn,
understand, to be understood, solve problems and achieve goals.

## Conceptual model

The goal is that any component on the ecosystem can both expose and consume knowledge, and enrich their own knowledge from the knowledge developed in other components. We believe that knowledge is both situated and dynamic; elements of knowledge are enriched one statement at a time, from an explicitly stated perspective. Knowledge applications must consume knowledge as a flux of situated statements, and be able to combine those disparate sources of knowledge elements into a coherent view, when possible, or point out incoherences in a meaningful way so people can work on resolving them.

We want to make it easy to explore new, alternative perspectives which are grounded in existing perspectives, and to compare perspectives with one another. We want comparison to be possible, both across disparate technical data models and human world views. For this, we need the ability to reference arbitrary data elements as topics, situated in time and origin.

These are the key concepts:

1. Topics: Knowledge is about topics. Everything is a topic.

2. Statements: Knowledge is composed of statements. Statements express properties of topics or relations between topics. Statements are topics, and can be the object of further statements. Topics in general and statements in particular form a generalized hypergraph.

3. Realms: Any source of knowledge defines a realm, which acts both as a namespace and knowledge repository. Knowledge elements live in realms, and have an identity within that realm. Cross-reference is explicit. Each realm can have its own storage model, with explicit support for distributed web options.

4. Perspective: Statements are introduced in an event queue, and the statements in a queue constitute a perspective on a set of topics. A realm can host a multiplicity of perspectives, which can hold potentially contradictory statements. A perspective can build on other perspectives, and encompass events from those perspectives until a set point in time. Merging events between perspectives is how perspectives are reconciled.

5. Data model: Knowledge elements (topics) follow a basic node/attribute model with some uncommon assumptions, mostly taken from Topic Maps. Like in RDF, node have a type system with multiple inheritance. Attribute types also have inheritance. However, relationships between nodes are not an attribute subtype, but a node subtype: Some nodes are N-ary relations. Some of those nodes' attributes are references to other nodes, and are called roles (and the referenced node is an actor). For some roles, many actors can play that role in one relationship, and those actors form an ordered set. Roles, being attributes, are typed. (Typing is a distinguished relation.) Some other characteristics of the model are: Merge nodes, live collections. (Described later.)

6. Event model: Operations on the data model are represented using a few basic events. Components should expose the event stream, and ideally allow subscriptions. We will aim for the event structure and data model to work as a CRDT. Merging of event streams to form a composite data state, like github merges, is an explicit goal. Higher level events can be introduced dynamically, as long as they are also translated to basic events. This translation can be done by an external component, which makes it possible for any component to interpret any new event type dynamically.

7. Transclusion: We have an explicit model of transclusion. When a realm incorporates a knowledge element from another realm, that element is transcluded, i.e. acquires a local identity in the transcluding realm. The transcluding realm should never modify the transcluded node directly, but include it as a component of a Merge Node, where local modifications can be applied. That way, it is possible to update the transcluded node from the originating realm's event stream, and changes can be merged or rebased in the Merge Node.

8. Documents: It is possible to represent documents using the graph properties of knowledge elements. Document fragments are either basic knowledge elements, or a collection of document fragments and view specifications.

9. External representation: Though our model is quite different in some ways, we share enough assumptions with RDF that it is convenient to reuse some of the same machinery, especially the JSON-LD representation.

## Realms
### Curation ecosystem

This hypergraph is distributed. There is never a single owner of a concept; anyone may make statements about features of concepts, or how they are related. We are proposing an ecosystemic approach to knowledge, where many processes produce and consume knowledge, and maintain distinct knowledge bases, which we call knowledge realms.

More precisely, each knowledge process consumes and produces immutable (Knowledge) Events, representing assertion or retraction of statements about concepts. Events originate in a Realm of knowledge, and a Realm can also maintain a local copy of concepts it refers to. The same concept can have very different contents in different realms. Social and technical rules direct which of those events are accepted within a given knowledge Realm, maintained by a given knowledge process. In many cases, events streams coming from another domain will have to be merged explicitly into a given domain, much like git change streams are merged from a branch to trunk. The rules of how events are merged into a domain's event stream will be specific to the domain. It is possible to know a domain's state by reading its event stream, and affect it by posting change event proposals to its inbox.

### Identity


Resource identity is complicated. When I refer to a resource, do I refer to the resource as it exists now, or as it existed last time I looked it up? As I understand it or as some other source of data I used undertands it (or used to)?
I propose making some of those references more precise, using concepts from the memento project, in the context of linked data.
All this only makes sense for mutable data; immutable data (in particular events) need none of this and can always use a single @id.
A few considerations:
1. Data exists in realms (repositories, databases, servers, RDF graphs, namespaces...) Two realms can hold resources that are meant to refer to "the same" entity, but may hold different (or even divergent) information about that entity. In the RDF world, it is common practice to use the shared name as a handle to the data, but it is misleading in some ways.
2. Event sourcing: some servers will express the latest version of a resource in terms of the latest known event ID instead of the time of change.
3. Distributed web: some URLs are content-addressable references (using DAT, IPFS etc.) rather than server-based, so some Memento assumptions about HTTP protocol headers need not hold.


So a resource is uniquely defined by a combination of some of those elements:

1. A resource's `lid` is always a dereferencable URL that will give me the current state, *L*ocal to a specific realm. (I will sometimes use the term `flid` for a foreign local ID, i.e. a reference in realm A to a `lid` local to realm B.)
2. A resource's `gid` is a global URI that may be known to many data sources. It is not bound to a specific time or server.
3. A resource's `tid` is a dereferencable URL of a specific (local) version of a resource at a given time. (This may be a stored snapshot or reconstituted from the event stream.) A given server may not offer a timegate; a timegate proxy service must be offered.
4. A resource's `eid` is the URL of the last (local) event that modified that resource (or more generally a Lamport clock URI specific to the data source?)
5. A resource's `ts` is the timestamp of the last modification to that resource
6. A resource belongs to a realm, which has an `rid` (`@id`), ideally a dereferencable URL that gives information about the realm and how to access its timegate.

The linked data `@id` usually refers to the `gid`, but I propose we tie it formally to the `lid`. This makes it clear that we refer to what is known (and believed) about the resource in a given data repository. Some mechanisms (IPLD) always use `tid` in references. This has the advantage of immutable references, but I believe it makes it impossible to include backlinks in resource representations (or tid change would propagate to the whole network endlessly.) It is possible to selectively present core data using `tid` forward links, and backlinks using `lid`; but I also believe it will create a number of useless updates, and I think it is easier to generally use `lid`, adjoining a realm-aware timegate service which can give us the `tid` at need, when given a combination of `rid` + (`lid` | `gid`) + (`eid` | `ts`).

Of course, when desired, a resource can refer to a `tid`; it should be easy to distinguish, within a certain realm, whether a give URL is a `tid` or `lid`. The representation of a resource (obtained through a `lid` or `tid`) should always mention the `gid` separately, and at least one of the `eid` or `ts`.

#### Transclusion

When we refer to a resource from another realm, RDF claims we keep it as an external reference (akin to transclusion), and it is sometimes the case; but in practice, most systems will import the data into a local representation, to improve both access time and reliability. Thus, the foreign `rid`+`flid` becomes a local `lid`. However, we should keep a record of the importation event that mentions the full reference of the imported data, (`rid`+ `flid` + (`eid`|`ts`).), and it should be part of the resource representation. (If that foreign record was itself imported from a third source, we might keep the chain of importation events.) Moreover, there should be mention of whether that local copy of the foreign data entity was locally modified since that importation. (There could be an exception for changes to back-references only.)

If I keep a local copy of a foreign data resource, I may want to update it; but updates should be under my control. If I have locally modified the resource, updates result in a merge. (We could also use a rebase, with a new event chain replacing the previous one; git semantics apply.)

However, if I keep a local copy of many resources from the same foreign data realm, it would be a misrepresentation to keep different resources in different states. So updates must be coordinated. If a new resource is brought in later, either it must be imported in its state as of the last known timestamp/event, or I must update all resources from that data realm while adding the new resource. (OTH, if a process handles many data realms, each may have different representations of the same foreign realm at different times.)
 
Adding gid's to resources is not terribly difficult; what is more difficult is creating the timegate. To make that easier, we should create a timegate-in-a-box, to which we can submit [rid+lid+gid+(tid|ts)+tid] tuples periodically, possibly with the content snapshot, and which will keep a running index for us.


### Realms and identity

Any data resource (topic) exists within a data repository (realm). The realm holds the data, and is in charge of maintaining identity, providing access, managing attributes and relationships, and resource evolution in the case of a mutable data resource.

The data resource has attributes, and at least one identifying attribute that uniquely identifies it within a given realm.

Another realm can reference to the resource in the original realm using some kind of URI. It is understood that a resource may have many URIs, but two resources having the same URI are identical.

In reality, in the case of a URL, the resource may change; unless the URL also has versioning information built-in (maybe using Memento), it is not identified uniquely by its URL.

In the case of URNs, the data resource can be copied into another realm, which may reuse the same URN (or even the original URL) to refer to it; but when it's been copied, it already has a local identifier in the second realm, and it may begin to add local information to the resource, thus altering it. (Note that putting it in relation to other resources is already changing it, as meaning is contextual.)

So a cross-reference should contain more than just a URN or URL; it should have a reference to the realm of origin (by hypothesis a URL), a URI that allows retrieval in that foreign realm (whether URL or combination URN+query endpoint), the time of retrieval, etc.

In many cases, it is safer for a realm making a cross-reference to make a local copy of the original resource, e.g. in case the original realm disappears. I call this transclusion, but it may be safer to call it embedding, as the local copy is not expected to auto-update. The embedded copy would have the complete cross-reference as provenance information. We must distinguish the case where the resource is further modified locally from when the embedding is still intact as retrived; I suggest a practice of keeping two separatet resources in that case, the original embedded copy and a modified shadowing resource with its own event stream. (Note that the same mechanism would be used for merged nodes and shadowing.) The advantange of that is that further evolution of the original resource (as a whole or through events) can trigger a three-way merge with the shadowing resource, using the previous embedded version as a base. Further, this allows a UI representation of which attributes were modified locally, and comparison with the original value.
### Architectural aspects

Knowledge applications can be classified by how they maintain their knowledge in a Realm. There are four basic scenarios:

1. A realm holder maintains its own realm, and may exchange data with other realm holders by exchanging events through event inbox and outbox, using an event inbox and outbox, as in ActivityPub (we will probably use ActivityPub for a first implementation.) The inbox and outbox will be exposed as capabilities, with appropriate access control. The holders may hold local caches of the other's state while doing so. Sharing the event stream may use a variety of mechanisms (notably P2P, pubsub, websockets...). Realm holders may be in a dependency relationship, where a Hub holds a community's realm, and personal knowledge application holds a dependent personal fork of that realm; but they're still both managing their own realm. This is the primary sharing mechanism of the ecosystem.

2. An event-aware client keeps a local cache of a foreign realm held by a distinct realm holder, and edits it directly by applying events locally. The events (semantic or elementary) are first generated and applied in the client, transmitted to the realm holder (using some transport), applied there and exposed in the realm holder's outbox.

3. A state-aware client keeps a local cache of a foreign realm held by a distinct realm holder, and edits it directly by mutating state and using some mechanism of state sharing (incl. P2P), outside the scope of this protocol. Either the client or (more likely) the realm holder must convert the state changes to an elementary event stream, so those can eventually be exposed in the realm holder's outbox. This is less preferred because we do not have access to high-level events corresponding to user intent.

4. A dumb realm client does not maintain its own copy of the realm's state, but continuously queries the realm holder for the state. Edition should still take the form of mutation events sent to the realm holder's inbox.

There are usage scenarios where the client (scenarios 2-4) has permission to edit a collective realm, and others where the client asks a realm holder to host and manage a private realm on the client's behalf.

In theory, it is possible for a knowledge application to expose its knowledge graph without exposing the edition events, but that is strongly discouraged, as many components of the ecosystem (especially recommendation systems) expect to work with events (preferably semantic events). It is possible to derive elementary events from two state snapshot, but semantic events will be lost.

#### Decentralization

The system should offer decentralized capabilities. Components (Knowledge apps) can operate in three modes: offline, peer to peer, client-server.
Offline components periodically publish data to the peer-to-peer graph (which uses content addressing), or to server applications. They can refer to components by identity, and a given state (and/or event stream) for a given resource identity can be retrieved from either the peer-to-peer graph or server applications.Some server applications can act as gateways to the peer-to-peer graph. The peer-to-peer graph enables navigating the graph locally; some server applications will keep indexes of the global structure, and are able to answer global queries.

Operations themselves will be distributed; they can be invoked on a server, a transformation operation can be distributed as code (raises security issues), or a transformation request can be put on a queue and processed by some other entity.

Depending on the subset of components considered, we can view the ecosystem as operating in  three apparently distinct Knowledge Management Network Topologies

* Standalone (offline first, Personal)
* Peer to Peer
* Client Server
* Federation Hubs
* Knowledge Apps
* Entity Hubs


The picture is even more complicated when we consider the possibility of multiplicity of federation servers, each potentially acting as a hub for Knowledge Sources which themselves can be other Hubs or Knowledge Apps, or Entity Hubs

#### Personal

Can have two types: Pocket Cloud Apps and a local peer to peer network connecting the users devices, smartphone. Tablets and laptops

One is i like to call a personal cloud app. This is the case when the user’s personal knowledge app is running on a single mobile device, typically the smart phone in daily use.
All other devices can connect over the local Wifi network to a single source of “wisdom”.

With the advent of IPFS, this setup can be considered obsolete or used to pin IPFS files so that they guaranteed to be available whenever the device is online.


So, the personal is envisaged to be a peer to peer setup, where even an individual is “collaborating” with himself across her devices.
In this setup, as in the real peer to peer topology, every participant in the network can be offline  any time. This is very much the case when a real collaborator is working offline and goes on line for an occasional sync.

## Data model
### nutshell version

The hyperknowledge graph consists of entities which express concepts through data structures (known as subject proxies or topics in the topic map literature), and can be associated to documents. Those data structures are basically key-attribute values. Some concepts express (n-ary) relations between other concepts, where each member of the relation plays a role. Natural language statements often correspond to (possibly many) such associations.

Anything that can be thought can be referenced as the subject of a statement; in particular, associations, attribute values, etc. can be member of associations. In other words, the entities form a [generalized hypergraph](https://en.wikipedia.org/wiki/Hypergraph#Generalizations).

Also, some concepts act as types for other concepts, or roles in relations. (The meta layer in a concept graph.) It is possible to refer to concepts in the meta layer by name as well as by ID.

This is not a traditional graph, where nodes and edges are disjoint sets. Here are a few base assumption:

1. Everything named below is a topic, and has a local identifier. (Local to the realm, or namespace.)
2. Any topic can have attributes, identified by a single type. The individual binding of an attribute value to a topic is itself a topic. (We call those Attributes)
3. All Relations are N-ary, and can point to many other topics. In that context, we refer to those topics as actors in the relation. The individual binding of a topic to a relation (the arrow of the line) is known as a RoleBinding, and has an identity and a single type.
4. Topics (including Relations) can otherwise have multiple types, as typing is simply a distinguished relation.
5. Binary relations are a common case, and there is a distinguished role type for source and target in that case.
6. Everything that happens is represented in the event stream.

### Diagram

This is a diagram that represents the key elements of the data model.

![main diagram](https://gitlab.com/conversence/hyperknowledge/raw/master/main_diagram.png)

This is an implementation diagram, some of the conceptual underpinnings might be a bit obscured. A few relevant notes:

* `BaseTopic` is an abstract class for anything that can be referenced. As such, it has a local ID and belongs to a realm.
* `SimpleTypeTopic` is a convenience class for some topics that will never have multiple root types (though their types have multiple inheritance.) In particular, this avoids the infinite regress of type relations needing type relations. A SimpleTypeTopic may not have attributes beyond its type.
* `Concept` is the base class for most topics, having attributes, multiple types, etc.
* `Realms` are topics. The Realm is aware of its set of `Concept`s and its `EventStream`.

## Event model

In general, an event first introduced to an event stream will be expanded into sub-events by the event processor(s) in charge of that event stream (namely the realm or some other command processors known to that realm.) The sub-events can be decomposed in turn, until we reach base events. It allows to add new derived command types, which will be ignored upon replay.

The event model is in flux, because it was supposing a very strict separation between commands and events and that separation is slowly being collapsed; but as things currently stand, base events come in pairs: the command asking for something to happen, and the event saying that it has happened. One reason for this separation is that commands can come from outside, but chosing topic identifiers is an internal concern of the realm. (When recording your own event stream, you're the one in control of the topic identifiers. The event queue is in charge of the event identifiers.)

### Elementary events and CRDTs

Ideally, the elementary events should act on CRDT structures.
Using terminology from this [Inria report](https://hal.inria.fr/inria-00555588/document)

The concepts in a realm form a graph, and as such could be modeled using a 2P2P-Graph;
but this supposes a sharp distinction between concepts and edges, blurred in our model.
We will instead model the graph with a single OR-set.
Attribute values, can be modeled by either a LWW-Register
(last-winner-wins, requires realms to have a lamport clock) or MV-Register (Multiple value),
depending on attribute arity. Arity is thus a fundamental property of attributes.
(Note that, to maintain arity in proxies, a resolution procedure has to be applied in case of conflict.)
Some Attributes may also want to preserve internal order, and should be modeled as such. (Probably for later.)

RoleBindings are attached to the concepts playing the roles, and not to the relation themselves, so as to preserve relation ordering.
Ideally, we would like to have a total ordering, but that makes for complex CRDTs. This is simplified in our case, since we need an oredered set and not a list, i.e. there are no repetition. For a first implementation, we might relax the full ordering constraint and use a Add-Remove Partial Order data type. Arity preservation is also a concern for RoleBindings.


Some roles should be marked as constitutive in a relation; the deletion of the concept and its bindings should thus entail the deletion of the Relation.
Of course, the deletion of a relation must entail the deletion of all its bindings.

VirtualProxies and Collections are more interesting: I hesitate between the clarity of the OR-Set and the versatility of the PN-Set. The advantage of the latter is that it's easier to notice conflicts.

![event types](https://gitlab.com/conversence/hyperknowledge/raw/master/event_diagram.png)


Unsolved use cases:

1. transclusion (vs fork. Will sometimes resolve to a fork later, but not always. Is it just a rule of auto-accepting changes? Possibly.)
2. Concept C in Realm R forked to D in S, which remembers C as origin (written D <-o- C here), equivalence (D <-=- C) and states an event update rule (D <-e- C). May be reported back to R, which may decide to record the symmetric links  C <-=- D and C <-e- D. Changes made to D. Owner of R decides that D is now more akin to E in R (or may create on the spot.) So breaks C <-=,e- D links, creates E <-e,=- D links. (May be equivalent to a split event... or not. Split event plays with origin links.)

so distinguish: fork origin, split origin, equivalence, event update rule...

Document edition events require a fully working ordered set, and are thus punted.

### References

Many aspects of an event are references to other events or topics in the realm where the events occur.
To save space, all events are considered to be JSON-LD, and the base URL may be omitted in many cases. (Also, some basic types can be represented by their name in the [JSON-LD context](http://purl.org/hyperknowledge/context).)
Also, it is possible to refer to topics or events belonging to an external realm; then the reference is replaced by a `{"rid":..., "realm":...}` construction.

So let's start with the most fundamental command/event pairs:

### Topic creation

```json
{"@id":"ev/0.0",
 "@type":"ccr"}
{"@id":"ev/0.1",
 "@type":"ecr",
 "fromCommand":"ev/0.1",
 "keyId":"http://example.com/realm/topic1"}
```

Here, the first event (`ccr`) is a Command to CReate a topic; the second one `ecr` is a sub-Event stating that the topic was CReated, as per the command above, and the resulting topic identifier is given as `keyId`.

### Binding creation

```json
{"@id":"ev/0.11",
 "@type":"car",
 "topicId":"http://example.com/realm/topic4",
 "role":"source"}
{"@id":"ev/0.12",
 "@type":"ear",
 "fromCommand":"ev/0.11",
 "keyId":"http://example.com/realm/topic4",
 "topicId":"http://example.com/realm/topic5"}
{"@id":"ev/0.13",
 "@type":"eenr",
 "fromCommand":"ev/0.11",
 "keyId":"http://example.com/realm/topic3",
 "topicId":"http://example.com/realm/topic5"}
```

Here, the first event (`car`) is a Command to Add a RoleBinding between topic4 (the relation topic) and topic5 (the actor).

The derived events mark the binding, first from the view point of the relation (EventAddRoleTarget, or `ear`): in that context, the keyId is the relation and the topicId is the newly created binding. The second event (`eenr`) marks the binding from the viewpoint of the actor ENtering the Relation (keyId is the actor, topicId is the relation.)

#### Foreign roles or actors

Note that here, we use one of the fundamental roles (`source` and `target`) mentioned in the JSON-LD context. If the role came from some other realm or ontology, we'd replace the role with an external reference, thus:

```json
{"@id":"ev/0.11",
 "@type":"car",
 "topicId":"http://example.com/realm/topic4",
 "role":{
    "rid":"http://purl.org/hyperknowledge/topic#sourceRole",
    "realm":"http://purl.org/hyperknowledge/hk#rootRealm"},
 "actor":"http://example.com/realm/topic3"}
```

It is possible to refer to an actor in another realm, as well. The syntax would be similar to that of the foreign role:

```json
{"@id":"ev/0.11",
 "@type":"car",
 "topicId":"http://example.com/realm/topic4",
 "role":"source",
 "actor":{
    "rid":"http://example.com/realm/topic3",
    "realm":"http://another.realm.com/"}}
```

#### Ordering

Relations being n-ary, it is possible for many actors to fulfill a role.
Actors for a given role form an ordered set. New bindings are normally appended to the set of actors for a role; but you can specify that an actor comes before another, thus: 

```json
{"@id":"ev/0.11",
 "@type":"car",
 "topicId":"http://example.com/realm/topic4",
 "role":"source",
 "actor":"http://example.com/realm/topic3",
 "before":"http://example.com/realm/topic6"}
```

### Attribute creation

```json
{"@id":"ev/0.2",
 "@type":"caa",
 "topicId":"http://example.com/realm/topic1",
 "attribute":"title",
 "value":{
    "@lang":"en",
    "@value":"The topic's title"},}
{"@id":"ev/0.3",
 "@type":"eaa",
 "topicId":"http://example.com/realm/topic2",
 "fromCommand":"ev/0.2"}
```

Here, a new attribute binding (topic2) is added to an existing topic (topic3.) The value given as an example is a string with linguistic attributes, as is common in JSON-LD.

### Deletion

```json
{
 "@id":"ev/4",
 "@type":"cdl",
 "topicId":"http://example.com/realm/topic2",
}
{
 "@id":"ev/5",
 "@type":"edl",
 "fromCommand":"ev/4",
}
```
    
Note that attribute and role bindings are deleted in the same way, since they have an identity. Deletion of a RoleBinding may also trigger an `eexr`, Event of EXit Relation.

#### Note on attribute and role identies

If your system does not give identities to attribute and role bindings, you should compose parseable identites based on the underlying topic (or relation respectively), the type of binding, and possibly an ordinal value.

### Importation

```json
{
 "@id":"ev/3",
 "@type":"cim",
 "fromCommand":"ev/2",
 "originalTopic":{
    "realm":"http://fingolfin.local:3001/first/",
    "rid":"http://fingolfin.local:3001/first/topic/1"}}
{
 "@id":"ev/4",
 "@type":"eim",
 "fromCommand":"ev/3",
 "keyId":"http://localhost:3000/here/topic/1",
 "topicId":"http://localhost:3000/here/topic/1",
 "sourceTopic":{
    "realm":"http://fingolfin.local:3001/first/",
    "rid":"http://fingolfin.local:3001/first/topic/1"}}
```

This is a variant of topic creation, but where the new topic is imported from another realm. To expand, under heavy development. Basically, a foreign concept can be in one of four states:

1. external reference
2. static transclusion (after having been imported locally)
3. dynamic transclusion (transclusion with assumption of constant updates)
4. shadowed (the topic was altered locally, so a local merge topic shadows the static transclusion.)

### Errors

Errors have their own event type: `eer`. The presence of an error sub-event derived from another event invalidates all events derived from that event. If that event is itself a sub-event of another event, all the sub-event tree of the top event are invalidated.

### Work in progress

Among things being worked on: Collections; Merge/Unmerge/Split of topics; shadowing of transcluded nodes; alternate identities (aliases); node substitutions.

## Collections
### Live queries

Most publish and subscribe models deal with topics, and those topics may correspond to individual resources (documents, etc.) or sets of resources (all documents relative to a topic, etc.) The number of topics is generally fixed by the publisher, and it is not generally specified how a subscriber could generate new topics. We would like to treat topics like a "live query", whereby a client could subscribe to all resources that correspond to a certain search pattern. There are three layers to allow this: the query specification language, the data model, and implementation considerations.

We have not attempted to create a generic query specification language, but use a functional language tied to implementations. Functional blocks have names and named parameters; named parameters have values which may be references to resources (including the contents of a container, or the results of another query.)

The important point is the impact on the data model. According to LD principles, everything is a resource, and each resource (identified by a URI, ideally a URL according to the principles of linked data) would belong to a primary container (itself a resource, as described in LDP.) Most representations of the resource would specify the primary container. A resource will typically belong to many other containers, each a subset of the primary container. Some properties of the resources may be defined by/on derived containers. (Terminology note: Containers correspond to what is called Collections in most other systems, but container is the LDP terminology we adopted.)

Subscribers would receive events of three fundamental types: a resource's properties changed, the resource entered a container, the resource exited a container. (Creation and Deletion of the resource can be represented as entering and exiting the resource's primary container.) Enter/Exit operations would be keyed on the container's identity first, and the target resource second.

The key twist here is that every application of a functional block with bound parameters becomes a named container in the system. Actually, we expect most non-primary containers will be tied to a functional block. A functional block can be considered in a dual way: as a consumer/emitter of events, and as the resulting container state (list of contained resources, and their representation) which can be queried. (REST, GraphQL... Certainly as JSON-LD)

(It should be trivial to develop generic shims that translate bidirectionally between a representation of resource membership as a stream of membership events or a resource list; that way function blocks would only need to work on event streams, yet resource lists would be available for interoperability with REST clients, and other caching purposes below.)

Function parameter bindings that refer to another container (functional or not) translate as container subscriptions. Thus, units of computational work will receive events, and the results will take the form of further derived events: those may take the form of further property changes on the original target resource, events focused on that resource entering/exiting themselves as a container, or change/enter/exit events regarding another (derived) resource.

At the implementation level, each functional container will live in a computational knowledge space; knowledge spaces are nested. The finest computational knowledge space corresponds to database transactions; so success or failure of computations living in the same transactional knowledge space would be atomic. This level of interdependence is a property of the subscription between the containers. Enclosing knowledge spaces can be defined at same-process, same-system, or inter-system level. Each knowledge space could have its own mechanism for event propagation and persistence (and changing event propagation mechanisms necessarily defines a knowledge space.)

Optimization note: within a knowledge space, update events on the same resource may be gathered and combined before being handed off to the enclosing knowledge space. In general, handoff of events to enclosing knowledge space is aware of subscriptions and can alter or combine events accordingly.

One important case is that an interprocess knowledge space can use a stream (such as a Kafka queue) for event propagation and persistence. In that case, events are not narrow-cast only to the interested container. Microservices in charge of certain containers would consume the stream and dispatch those events to corresponding functional containers, first to update the internal state (broadcast to all microservices), second to apply the function's computation and generate new events (single consumer microservice.) (The dispatch could use a lower-layer rule-based dispatch for event routing.)

There is a difficulty in that case: if the resource's property changes in a given container, that event will only mention the computational unit where the change happened, and the primary container of the resource. Suppose that this resource is also considered part of a third container, derived from one of those but distinct from both. How will a given computational unit that subscribed to this third container know that this property change is relevant?  It would be possible, but potentially wasteful, for the third container to generate an event for the resource change keyed to its own identity. But such events could overwhelm the system. Instead, each process (possibly overseeing multiple containers) could track of membership change in all containers that its own containers subscribed to. Thus, for each generic resource update, it can determine affected containers in its purview and trigger appropriate units of work, though the dependent container may not be named in the event. It would have to query each dependent's container current state at startup, which is possible because containers are addressable and membership list can be queried.

(Further optimization: Processes may do so fuzzily with a bloom filter, and query the container in its owning processes for actual membership. It would need to query the owning process to bootstrap the membership list.)

This optimization (erasing of change events on derived containers) would have to be undone at the broader (inter-system) knowledge space; subscribers would expect the change events to mention the appropriate derived containers, and the knowledge space handoff machinery should handle this. Change events with the same resource target should be combined into one event that would mention more than one resource container.

Inter-system knowledge spaces would use more conventional pubsub mechanisms, including perhaps new abstractions such as WebPub/ActivityPub. However, there is a slight impedance mismatch in that the events defined in this architecture include lower level events that those defined in these architectures, and we'd have to ensure that some functional units would filter/combine/transform those events into the appropriate level of abstraction.

Another implementation note: avoiding infinite event propagation cascades is probably impossible in principle; we can at least restrict circular dependencies between functional containers, but that may not enough, and they may even be useful in some cases. However, detecting such cyclic change cascades should be possible, especially if each event keeps the ID of the originating event at the root of the event cascade.

### Knowledge spaces

Each concept and association can belong to one or many knowledge spaces, aka Collections. (aka Context, but there’s more name collisions that way.) (OR: knowledge spaces are subsets of concepts and associations.)

Use cases of knowledge space include, but are not limited to:
1. Distinguishing one person’s beliefs from commonly held beliefs (or sub-community's beliefs)
    1. This extends naturally to knowledge servers, in so far as they can “belong” to a single community
    2. Conclusions of an algorithm should belong in a knowledge space, so it is easy to examine the algorithm for validity.
    3. See the section on divergent versions
2. Hypotheticals: If X were true, then Y would follow. The X hypothesis defines a knowledge space for belief in Y.
3. Reported belief: X says that Y believes that Z… each of those is a nested knowledge space.
4. Qualified knowledge: This was true at a given date, in a given location, etc. (See wikidata qualifiers)
5. Provenance information: Information gathered from sentence X of document Y.

Q: Note that the last case is about topic occurence, and may be handled differently from the previous ones. In particular, topic occurrences have a single context, whereas a concept may live in multiple knowledge spaces (Y may independently be a consequence of antecedents X and Z; may independently be believed by P and Q; etc.) On the other hand, document provenance does define a valid knowledge space.

Relationships between knowledge spaces is going to be an important field of study, and probably cannot be fixed yet. If I believe that X->Y, Z->¬Y, I would no want either Y or ¬Y to leak to my beliefs, though the hypothetical knowledge spaces are obviously sub-knowledge spaces to the knowledge space of the set of my beliefs. OTH, in a hypothetical subknowledge space, I will often re-use knowledge from the broad knowledge space of my beliefs transparently; so in the case of hypotheticals, concepts tend to leak down to subknowledge spaces but not upwards. (I may however contradict one of my beliefs in a hypothetical, so leaking should always be explicit. In Conceptual Graphs, statements have a single graph but are cloned across graphs. I think that is too limiting, but certainly transclusion must be explicit.)
In the case of provenance, however, statements taken from a sentence leak “upwards” in the paragraph and document knowledge spaces. Q: How much of this needs to be shared knowledge in the ecosystem?
Finally, some knowledge spaces will be explicitly defined, others will be the result of computations with parameters (see Live Queries below.) As a consequence, it is not going to be possible in general to know how many knowledge spaces a given statement is part of; since it would require exploring the parameter space of complex queries. (We can certainly list explicit knowledge spaces, and query memberships in a computed knowledge space for given parameters; even subscribe for membership changes in the latter.)

In the case of knowledge-domain knowledge spaces (see section on divergent versions), it is essential to know which knowledge-domains have a given version, or all versions of a concept.

Q: Should knowledge spaces be modeled as graphs?
Pro:
    * there is RDF syntax support.
Cons:
    * Graph implementation is heavyweight in some RDF bases, but that should not matter.
    * There is no support for an assertion belonging to multiple graphs.
    * In the case if complex associations, we have to make sure each element (e.g. each role association) is ported to the graph.

## External representation and APIs

* Each realm must expose its topics as mutable JSON-LD objects, using either a basic RESTful model or some distributed web mechanism such as IPNS. (A URL must be provided in either case.)
* Each realm must have a representation of itself accessible as JSON-LD at a HTTP URL. (It may be an IPNS-based, but it must still be an HTTP URL.) This representation must include endpoints for resolving local IDs (URNs) to representations. Note that those representation may be partial and subject to authorization and authentication. (TODO: Rethink wrt capabilities.)
* Each realm should offer access to the event stream underlying its data model, quite probably using something like http://activitystrea.ms or http://activitypub.rocks (but our event types are quite different from those in ActivityPub.)
* Each realm should offer a way to subscribe to this event stream, or ideally a subset of this event stream relevant to some topics.
* Each realm may offer a way to obtain past versions of a given topic, using the memento protocol.

It should be possible to express a HK stream as a RDF graph.
The representation may be based on the event or not, but any claim should be materialized.
so explicit SPO for literal properties; materialized associations in most cases with materialized role bindings; it may be possible to materialize object properties without an association, but that is actually problematic.
So a binary link would be 7 triples:
binding1 subject assoc1;
         predicate p:source;
         binding1 object source.
binding2 subject assoc1;
         predicate p:target;
         object target.
assoc1 a p:Assoc1.
(SIGH.)
If part of an event, we'd also have 
event1 claim binding1.
event2 claim binding2.
Ok, can we do better?
we can have a shortcut
assoc1 p:source source;
       p:target target.
and bindings at need. Ugh. How to navigate that? I need to give an idetnity to the bindings.
and it does amount to 
source p:Assoc1 target.
if we had quads, we can
binding1 assoc1 p:source source.
binding2 assoc1 p:target target.
or even
assoc1 source p:Assoc1 target.
(Note that a quad store is not enough, since I want the stream ID, so I'd need a quint store! It's what I'll have in postgres.)
An idea: combine assoc and role into an identifier.
So... <bindingId> <assoc1+role1> <topic> But unless I declare assoc1+role1 subproperty of role1, it's unusable. Could do it in internal index, and even then...


harumph. Take it from the other side: how would I express a graph as a HK stream?
I would assign an event and an Id to each triple, of course. (Event would be time of observation. A triple disappearing would be replaced by... nothing?)
So... quad like, that way.

### JSON-LD basics

We should use JSON-LD 1.0 for now, though 1.1 exists.

1. Atomic RDF statements take the form of triples: subject, predicate, object. (Those triples often exist in the context of a graph, which makes them quads.)
2. Graph, subject and predicate have an identity expressed through a URI. Subjects (etc.) are said to be resources. Objects may be another resource (URI) or a literal. Literals may be typed and, in the case of strings, carry a language marker.
3. Those URI are often shortened as [CURIE](https://www.w3.org/TR/curie/)s, where most of the URL is given a name and acts as a namespace, followed with a colon the last segment of the URL. (eg.: `foaf:Person` is a shorthand for `http://xmlns.com/foaf/0.1/Person`, i.e. the `Person` class in the namespace called `foaf`.)
4. In the case of linked data, URIs should ideally be dereferenceable, and content negotiation should be possible.
5. [JSON-LD contexts](https://json-ld.org/spec/latest/json-ld/#the-context) further give names to such resources, especially the URIs that designate properties; this allows those names to be used as keys in JSON-LD.

One non-obvious consequence is that there are often many ways to express a triple in JSON-LD.
Let us start with a few basic triples:

```n3
<http://dbpedia.org/resource/Tim_Berners-Lee> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person>
<http://dbpedia.org/resource/Tim_Berners-Lee> <http://xmlns.com/foaf/0.1/name> "Tim Berners-Lee"@en
<http://dbpedia.org/resource/Tim_Berners-Lee> <http://dbpedia.org/property/occupation>  <http://dbpedia.org/resource/Computer_scientist>
```

or, in Turtle syntax with appropriate CURIEs:

```turtle
dbr:Tim_Berners-Lee a foaf:Person,
    foaf:name "Tim Berners-Lee"@en,
    dbp:occupation dbr:Computer_scientist.
```

In JSON-LD, we'd probably write (`@context` omitted, which would contain the namespaces)

```json
{
    "@id": "dbr:Tim_Berners-Lee",
    "@type": "Person",
    "name": {
        "@value": "Tim Berners-Lee",
        "@language": "en"
    },
    "occupation": "dbr:Computer_scientist"
}
```

Note that the prefixes were elided for name and occupation, but not for the profession; those are arbitrary but plausible choices. The identity and `rdf:type` operator (which indicates class instances) have a special expression in both turtle (`a`) and JSON-LD (`@id`, `@type`).

The name could have been a bare string, an array of strings, an array of language string structures; with some contexts it could even be a dictionary of languages to values. Those are all valid ways to express a string literal. A non-string literal will often be expressed with a combination of `@value` and `@type`.

This is the RDF layer; we are building something above it, so keep in mind that RDF properties will not strictly correspond to Hyperknowledge Relations and Features, and not all RDF resources will necessarily be hyperknowledge objects (concepts).

### LDP

Web services in the ecosystem that expose RESTful interfaces to topics should be consistent with behaviours described in the [Linked Data Platform](https://www.w3.org/TR/ldp/) specification.

### Memento

We need to link live and snapshots representations of the same data. One standard way to do this is to use TimeGates as defined in the [Memento](http://mementoweb.org/guide/quick-intro/) protocol.

Concepts will generally only have a TimeGate, Documens may also offer TimeMap functionality.

## Distributed documents format

This is an attempt to specify a protocol for distributed documents.

The main goal is to allow composition of document fragments without presuming of the underlying document format.

### Hyperdocument

(Note: this is very much work in progress.)

HyperDocuments are composite structures, and a composite’s subdocuments may either be composites again or document fragments. Document fragments have a mimetype and optionally an inherent viewspec. The containing document may override the subdocument’s viewspec. In the case where the subdocument is itself a composite, the parent may even override the traversal strategy of the subdocument (to identify the subdocument’s fragments), so it would seem to override the content.

Some document fragments refer to concepts, either with RDFaLite spans, because a WebAnnotation anchored in that document fragment refers to that concept, or because the document fragment is supposed to stand in for that concept.

### Document fragments, composites and augmented HTML

A composite document is made of sub-documents. Those sub-documents may be document fragments or another composite document, recursively. Document fragments may be of any mime type, including HTML. Composite documents will be a new mimetype, probably based on JSON.

Most documents have two forms: raw (any mimetype) and augmented HTML fragment. The latter uses HTML and RDFaLite to convert and/or wrap other documents in a HTML fragment containing its own metadata. (media objects are mostly wrapped, textual formats are converted.) In the case of a composite document, the components are resolved to their augmented HTML fragment recursively.

(Note: almost everything above could be rewritten with conversion to augmented PDF fragments instead of augmented HTML fragments, but the composition would likely be less straightforward.)

The augmented HTML fragment also has a structure that allows applying viewspecs explicitly, and external WebAnnotations are resolved to spans if appropriate. Edition operations, on the other hand, are generally done on raw documents.

Edition of augmented HTML involves a family of Javascript operators, according to enclosed document types; those are naturally applied on the complete augmented HTML document, a final wrapping step for a root augmented HTML.

Each document fragment is a RDF resource, and is treated as a concept for document composition purposes. Some document fragments may also be said to illustrate a particular concept (`foaf:primaryTopic`), in some cases canonically (`hk:localRepresentation`). Thus, it is possible to create a linear document representation of a knowledge graph from a tree traversal specification of said graph, using canonical representations. Conversely, a linear document (as represented by a root composite document) implicitly contains at least a concept graph consisting of the document-fragments-as-concepts, and potentially another one referring to the primary topics of the concepts.

It is possible to include a reference to a raw concept directly in a composite document; its augmented HTML fragment representation would then either use the canonical illustrative document if any, or alternatively wrap the JSON-LD with some textual or graphical representation of the local knowledge graph. (Choosing which is probably a viewSpec concern.)

Document is an overloaded term (please help offer alternate terminology.) In particular, it refers both to the file format and to its presentation to a human (which may use a distinct presentation file format.) The other aspect is that we are dealing with composite documents, that incorporate document fragments (e.g. images) which each have their own datatype (in the sense defined by MIME datatypes.)

In our terms: Document fragments use application-specific data-types and file formats, and are assembled into composite documents. Presentation of those fragments or of the whole may require requires conversion of some other format to a presentation format like HTML or PDF. This conversion is made by a document processor, a web service which implements some ViewSpec.

### Document data and metadata

Some document formats allow inclusion of metadata; some do not. In general, a document (or document fragment) may be divided in data and metadata document, though composite document formats will exist.

We propose using RDF as a base metadata formats, with appropriate terms taken from appropriate vocabularies (discussed below.) Separate metadata documents should be expressed as JSON-LD, and systems providing documents containing embedded metadata must provide a mechanism that will extract that metadata as JSON-LD. (In particular, HTML documents can embed JSON-LD, and/or offer a conversion of RDFa-Lite to JSON-LD; those options would be implicit.) Another combined form allows to have the data embedded in the metadata document.

Documents may be published online on given URLs; if so, the metadata must also be published online, and contain a canonical URL where the latest version of the document can always be retrieved. The latest version of the metadata, if separate, can also be given in the metadata, or in a `describedby` HTTP Link on the document (as per LDP). The metadata document will also offer a URL for the latest version of the data document if distinct.

The documents may be versioned, and if so must be retrievable following the [Memento protocol](http://mementoweb.org/guide/rfc/). If the data and metadata are separate, each must offer versioning separately, though the versions will often be coordinated.

### Document types and processors

Document (fragments) always have a mime type, and may have subtypes defined in the metadata. A distributed document will be made aware (by configuration) of processors that can be applied to various mime types or subtypes; and the document metadata may specify further processors. Processors will often be defined as web services (i.e. Restful RPC endpoints), but can also be installed locally for performance or privacy reasons.

Some processors (known as ViewSpecs) will generate displayable versions of the document, in various well-known formats such as HTML, PDF, etc. (Such output should keep a pointer to the original document's metadata, and the processor used.) This process may also involve transformation of data within a given format, usually to enrich it. The output may also be cached and/or versioned by a web-based processor.

### Metadata items

The following metadata terms will be defined in the metadata:

#### meta-metadata

These fields will be defined on the metadata document, if separate, and pertain to that document. Otherwise meta-metadata will be conflated with data, in one block.

* `@id`: the RDF resource identifier of the metadata document. (mandatory)
* `data`: the block of metadata that refers to the document data itself, containing further values

If the metadata document is distinct from the document (i.e. neither embeds the other), it may also contain the following:

* `url`: the canonical URL for retrieval of the metadata document, if different from the `@id`. (It is better practice if the metadata's `@id` is itself a URL and not a URN, then the url is not needed.)
* `timegate`: The memento timegate, if available, for the document metadata. Can also be retrieved in the metadata HTTP Link.
* `dc:last_modified`: The timestamp of the metadata's last modification, to be used with its timegate.

#### metadata (in the `data` block)

* `@id`: the RDF resource identifier of the data document, if separate from the metadata. (mandatory)
* `url`: the canonical URL for retrieval of the document data. (It is better practice if `@id` is itself a URL and not a URN, then the url is not needed.)
* `timegate`: The memento timegate, if available, for the document data. Can also be retrieved in the document's timegate HTTP Link.
* `dc:last_modified`: The timestamp of the document's last modification, to be used with its timegate.
* `dc:mimetype`: the mimetype of the document (mandatory)
* `@type`: RDF type of the document
* `derived_from`: In the case where the document was the fork of another document (as in Github, Federated Wiki...), the URL of the original document.
* `processors`: a description of document processors (including ViewDefs) for this document, known to the document server (others may exist.) We should have links to generic sets of processors for a given document type. TBD: Description should build on [Web service description language](https://www.w3.org/TR/wsdl20-rdf/)?

The metadata may contain [provenance information](https://www.w3.org/TR/prov-o/#description), notably `prov:wasRevisionOf` links to the Memento snapshots, as described [here](http://ceur-ws.org/Vol-1035/iswc2013_poster_10.pdf))

We can also connect the document to the last Activity that generated it with a `provenance` HTTP link. (ActivityStreams or Provenance? Not connected, sigh. Linking to last activity or stream as a whole? A bundle in provenance terms, no stream equivalent. An OrderedContainer, I guess.)

### Composite documents

At its simplest, a composite document will be a (JSON-LD?) document fragment consisting of a list of pairs of document fragment identifiers, timestamp, and default viewSpec (processor and parameters).

A more complex composite document would add layout information. What matters is that creating the composite views (HTML, PDF) is work for a document processor that would know to ask the component's document processors to obtain presentation forms of the fragments that can be stitched together.

layers of viewspecs?
Separate document fragment from viewspec?
sideload annotations vs apply at once. Issue of overlap.

### Transformation processors

Many viewspecs will add layers of information on existing data; for example entity identification in text. They may work on the original document or on its HTML representation. In the case of HTML representation, many overlays can be defined using the [W3C annotation model](https://www.w3.org/TR/annotation-model/). This is also easy to do when the original document is XML-based; but in that case it is important that we can translate fragment selectors between the original data and the representation. (If fragment identifiers are preserved by the viewspec, some xpointer-based fragment selectors may be reused as-is. But that is a special case.) In most cases, it is easier to apply those processors to the HTML representation.

### Document server

We should reuse existing components that are aware of versioning, such as [Marmotta](http://marmotta.apache.org/platform/versioning-module.html).

### Edition

This addresses access, and not edition; edition is an application concern. However, we should explore ways to embed application-specific editors in a web applications. The important bootstrapting step is to create a minimal editor for composite documents.

From here, please consider it work in progress
## Use cases

### Story

1.  I want to write about issue I. Start doing "normal" research.
    (LA+google, wikipedia) Add some references to external data sources
    with MG connectors. (either autocompletion or LA search)
2.  Note: maybe send your draft to an analysis system that will try to
    extract concepts from your text and globally find graphs of cognate
    concepts elsewhere on the Web.
3.  Define a position P1, _mark the text as a position (UI of
    types)_ and send to Hub to find if it is known. Assume not,
    but hub proposes a list and I identify a similar cognate concept
    one, so P5 and I mark it as similar.
4.  Ask hub for concept neighbourhood of P5, which may serve as
    evidence and counter-arguments to P1, hopefully to strengthen it. I
    get a graph back (both as data and prob. as representation). Those
    also point to origin documents (backlinks) or conversations.
    _There are markers about argument popularity._
5.  A counter-argument A1 looks popular. Find a community debating it,
    because that realm is one of the provenances of the arguments.
    (IL)
6.  Heated debate, you're not sure about counter-argument so you read
    up. The holders of that counter-argument prefer another solution
    P2.
7.  You have doubts about the other solution, so you begin drafting a
    counter-argument A2 to that. The (TQ) AI informs you that it looks
    like an existing, already-refuted counter-argument A3. You do not
    post your counter-argument A2.
8.  The refutation helps you see a new perspective. You realize your
    proposal is flawed, but you see yet another flaw in the competing
    proposal. You write up the criticism A4 of the other proposal, and
    imagine a new solution P3 that combines your original solution with
    the competing solution.
9.  You revise parts of your document where you were talking about P1
    and replace it by P3; you do this through a filtered viewspec of the
    document that only shows sections that mention P1, and immediately
    related. You detect another inconsistency thanks to that focused
    view.
10. You take the elements from the public conversation, and create a
    concept map (MG) of your original proposal P1, the competing
    proposal P2, your improved proposal P3, and respective
    arguments.
11. You weave (vs write) this concept map into a coherent text, and publish it. (LA)
    _The publication contains the MG map._
12. _work the other way round, you have created narrative and see it in a HyperMap_
13. _This pings back the conversation, and the community gets notified of new arguments coming into the conversation._
14. Some community members read your proposal, accept that your
    arguments and proposals are cogent, and enough of them vote
    for their inclusion in the community mind map. (IL)
    (simpler alternative: just include you in the community.)
    Even if they did not, they would hear about it the next time
    they queried the TQ hub.
15. The author of the original competing proposal P2 thinks the
    original objection A1 also applies to your improved proposal, and
    links it as such.
16. _You get informed of this, but argue (A5) why the argument A1 is
    inapplicable to P3. Unless that argument is itself refuted, the
    objection is seen as already-refuted on your local map, in the hub
    and potentially so on the community map (because by now you've been
    accepted in that community). P3 is now the only non-refuted
    proposal._

### Key goals

We agree on the following goals to show added value of ecosystem:


1.  We want to highlight connection to community: Social Knowledge
    Network
2.  Peer to Peer collaboration of Personal Knowledge Work that can
    interoperate with Organisational knowledge and give rise to
    spontaneous order (cascade from personal to community to hub -\>
    with social merge dynamics)
3.  We want to highlight argumentation (multiple points of view,
    arguments pro and against, and by whom and through what evidence
    they are held)-\> view of consensus and dissensus
4.  Connect text regions to concepts, and links to associations.
    (articulation)
5.  We also want to show how the same concept can be referred to from
    elsewhere in the document, and in other documents. We want viewspecs
    focused on concepts and concept neighbourhoods. -\> easier to get
    multiple perspectives
6.  Map (Visualization and prioritization)
7.  Progressive formalization of knowledge (optional)

## Hyperknowledge data representation

Hyperknowledge state data representation will be based on [JSON-LD](https://json-ld.org/). Let us recapitulate a few relevant principles of [RDF](https://www.w3.org/TR/rdf11-primer/) and JSON-LD.

### Identity

Each concept has multiple identities which we can use to refer to it: we need to distinguish them. 

Resource identity is complicated. When we refer to a resource, do we refer to the resource as it exists now, or as it existed last time we looked it up? As we understand it or as some other source of data I used understands it (or used to)?
We propose making some of those references more precise, using concepts from the memento project, in the context of linked data.
All this only makes sense for mutable data; immutable data (in particular events) need none of this and can always use a single `@id`.
A few considerations:

1. Data exists in realms (repositories, databases, servers, RDF graphs, namespaces...) Two realms can hold resources that are meant to refer to "the same" entity, but may hold different (or even divergent) information about that entity. In the RDF world, it is common practice to use the shared name as a handle to the data, but it is misleading in some ways.
2. Event sourcing: some servers will express the latest version of a resource in terms of the latest known event ID instead of the time of change.
3. Distributed web: some URLs are content-addressable references (using DAT, IPFS etc.) rather than server-based, so some Memento assumptions about HTTP protocol headers need not hold.


So a resource is uniquely defined by a combination of some of those elements:

1. A resource's `lid` is always a dereferenceable URL that will give me the current state, *L*ocal to a specific realm. (We will sometimes use the term `flid` for a foreign local ID, i.e. a reference in realm A to a `lid` local to realm B.)
2. A resource's `tid` is a dereferenceable URL of a specific (local) version of a resource at a given time. (This may be a stored snapshot or reconstituted from the event stream.) A given server may not offer a timegate; a timegate proxy service must be offered.
3. A resource's alias is a timeless URI other than the canonical `lid` that can be used to refer to the resource. In particular, a `gid` is a global URI alias that may be known to many data sources. It is not bound to a specific server.
4. A resource's `ts` is the timestamp of the last modification to that resource
5. A resource's `eid` is the URL of the last (local) event that modified that resource (or more generally a Lamport clock URI specific to the data source?). It can replace the `ts` for some servers.
6. A resource belongs to a realm, which has an `rid` (`@id`), ideally a dereferenceable URL that gives information about the realm and how to access its timegate. (Some realms will not have a net presence; in that case we need some way to ensure URN uniqueness, such as a DID, and some way to obtain realm information, including an external timegate, from a realm ID and a timeless identity.)

The linked data `@id` usually refers to the `gid`, but I propose we tie it formally to the `lid`. This makes it clear that we refer to what is known (and believed) about the resource in a given data repository. Some mechanisms (IPLD) always use `tid` in references. This has the advantage of immutable references, but I believe it makes it impossible to include backlinks in resource representations (or tid change would propagate to the whole network endlessly.) It is possible to selectively present core data using `tid` forward links, and backlinks using `lid`; but I also believe it will create a number of useless updates, and I think it is easier to generally use `lid`, adjoining a realm-aware timegate service which can give us the `tid` at need, when given a combination of `rid + (lid | gid) + (ts | gid)`.

Of course, when desired, a resource can refer to another through a `tid`; it should ideally be easy to distinguish, within a certain realm, whether a give URL is a `tid` or `lid`.

The representation of a resource (obtained through a `lid` or `tid`) must always contain the `@id`, `tid`, previous `tid` at least one of the `ts` or `eid`, and should also contain the `gid` and other aliases if any.

When we refer to a resource from another realm, RDF claims we keep it as an external reference (akin to transclusion), and it is sometimes the case; but in practice, most systems will import the data into a local representation, to improve both access time and reliability. Thus, the foreign `rid+flid` becomes a local `lid`. However, we should keep a record of the importation event that mentions the full reference of the imported data, `(rid + flid + (ts|eid)`.), and it should be part of the resource representation. (If that foreign record was itself imported from a third source, we might keep the chain of importation events.) Moreover, there should be mention of whether that local copy of the foreign data entity was locally modified since that importation. (There could be an exception for changes to back-references only.)

If I keep a local copy of a foreign data resource, I may want to update it; but updates should be under my control. If I have locally modified the resource, updates result in a merge. (We could also use a rebase, with a new event chain replacing the previous one; git semantics apply.)

However, if I keep a local copy of many resources from the same foreign data realm, it would be a misrepresentation to keep different resources in different states. So updates must be coordinated. If a new resource is brought in later, either it must be imported in its state as of the last known timestamp/event, or I must update all resources from that data realm while adding the new resource. (OTH, if a process handles many data realms, each may have different representations of the same foreign realm at different times.)

It should be easy to subscribe to events from a foreign realm that concern resources that were imported. When a realm imports a resource, it should signal this importation back to the originating realm, so bidirectional subscription is possible. (Another event should allow unsubscription when an imported resource falls out of scope.)

Adding `gid`s and `ts` to resources is not terribly difficult; what is more difficult is creating the timegate. To make that easier, we should create a timegate-in-a-box, to which we can submit `(rid, lid, gid, (ts|tid), tid)` tuples periodically, possibly with the content snapshot, and which will keep a running index for us. This is more efficient than walking the `tid` chain. This timegate could also serve as a subscription proxy.

#### Human readable meaningful Ids

About two years ago I (GL) experimented with the idea of linking resources to combination of relevant keywords (wordgramms in Jack’s world). Recently I am gravitating to the notion of Human Readable Meaningful Ids: i.e. a particular sequence of keywords that highlight the intended import of a node and use that as an Id. Every time a node is created with a set of keywords a search is carried out in the graph available to the user to see if a node with thata Id exist, and refer to that if appropriate or create a new one with the addition of a distinguishing keyword that make the Human readable meaningful id unique.

In case of accessing further contexts or in the case of merging of graphs the same id is used then create a merge node with that ID and link to the variant. At this point each variant within its original context would link to the merged node.

MAP: Those meaningful ids (`mid`?) would be local aliases, in most cases, not `gid`s. I mostly worry about their persistence, as the paragraph content might be edited so much thta the original words might become totally appropriate. Do we update the meaningful ID, losing persistence? or do we keep it stable, losing meaningfulness? It does introduce the requirement that aliases be time-bound, which is a bit painful.

(everything that follows in the identity section is potentially obsolete)

#### Timeless vs snapshot

Each entity has a live identifier, i.e. an immutable identifier to a mutable resource (ideally the URL where the live resource can be retrieved); but also it is possible to refer to a specific (immutable) snapshot of the entity. Each such snapshot would have a different identifier as a snapshot, but also contain a reference to the identifier of the "live" resource.

Q: Should links to other resources point primarily to the live resource identifier, to the current snapshot identifier, or both?

Proposal (obsolete): The base links would at least have live identifiers with timestamps, or maybe a Lamport clock and base knowledge space (see next section.)

They MAY also include an identifier for the specific version, when such a version makes sense. Personal knowledge graphs that publish a coherent collection (aka a nexus) would snapshot concepts globally at the moment of publication, and include version-specific identifiers. In this case, it would be good practice if the version identifier were content-addressable, as in IPFS. Unlike [IPLD](https://github.com/ipld/specs/tree/master/ipld), this would only be a complementary mechanism.

Other services (esp. federation servers, or conversation servers) work with continuous change on a graph, modeled with event sourcing; point-in-time versions have to be reconstructed from the event stream, since any snapshot identifier would propagate to the whole graph. It means that, in terms of the [Memento](http://mementoweb.org/guide/quick-intro/) protocol, such servers would have a TimeGate but no TimeMap for concepts. (Documents may still have a TimeMap.) and memento identifiers would be constructed from the live identifier+timestamp.

#### Names and locators

[Linked Data](http://linkeddata.org/) states that we should prefer URLs as resource identifiers. However, multiple services may contribute to knowledge around a given resource, and the identifying URL may not be the only or even best source of knowledge about that resource. It is necessary to express the multiplicity of data sources about that resource, hence a multiplicity of URLs. Moreover, URNs are also legitimate URIs, and some ecosystem members may choose to use a URN (UUID, etc.) as a resource's main identifier.

It is specifically the work of federation servers to unify identities; a primary service of the federation server should then be to list URL data sources that offer information about a given identity, whether a URN, a foreign URL or a URL local to the federation server.

#### Knowledge realms and divergent versions

Each concept (incl. association) can belong to one or many knowledge spaces. (OR: knowledge spaces are subsets of concepts and associations.)

See the appropriate section for knowledge space use cases; but here, we will focus on distinguishing one person’s beliefs from commonly held beliefs (or sub-community's beliefs.)

Some knowledge spaces are also knowledge realms. Again, concepts always exist within a knowledge realm. They are defined by a flux of datachange events that live within a knowledge realm, so each KD is a source of events. When a knowledge realm refers to a concept present from another (source) knowledge realm, it may do so with a transclusion node; or use the node itself with its original ID. In both cases, it will include some of the events from the source KD within its own event flux, through the use of merge events. Merge events work exactly like a branch merge node in github, and have more than one ancestor event.

A knowledge realm can refer to events coming from multiple other KD, and how and when those events get merged is part of the definition of the domain. There is no constraint on the dataflow structure, which may be cyclical. There is a relationship between knowledge realms and their source domains. Within a domain, concepts should have a single representation. When people edit a concept, they will often do so in a domain that they control. The changes are then merged into domains “listening” to the original (source) domain, according to the merge procedure specific to the (target) domain. These merging procedure can be automated, but will generally involve humans.

There are use cases where this forms a dependency tree: For example, I may edit a concept in a domain I fully control on a service; the change event will be merged in my personal information space automatically as long as there is no conflict (using an algorithm like that of github), then that will be merged in my task team’s domain after we achieve full consensus; from there it may be merged in my company’s domain requiring my manager’s approval; from there it may go into consortium regulations following a vote; up to the ecosystem’s federated knowledge map where all knowledge is included with concepts that include all diverging values from all subdomains (automatic inclusive merge.) Note that the federation, unlike other domains, has no aim to propose a coherent view, but is indexing all possible views and showing where divergence happens.

So for any knowledge realm, there should always be a single representation of the concept, and explicit merging of local changes from multiple sources into a domain may or may not be accepted back by the source domains. Or it may chose to re-do the merge using different rules.

Ideally, many domains should have simple automated merge procedures; for example, if I control a domain and subdomains correspond to different tools I use for edition, the merging should assume fusion and last-edit-wins, and the concept will behave as a CRDT.

This describes the case of merging of divergent concepts; concepts that were created different and are found to be identical a posteriori may use the same merging mechanisms, although the merging of distinct concepts, unlike the merging of versions of a concept, should not merge concept identities, so that it can be reversed. See next section.

In some cases, when the technosocial mechanisms for resolution fail, the best course will be to agree to disagree, and create new concepts representing each faction’s viewpoint, maybe referring to a NPOV base concept. We can expect conflicts about which faction will retain the original live identifier (as it is referred to by other concepts), and we must enforce a policy of reserving it to something closer to a NPOV. We must also make it very easy to see the set of variants around a contentious concept. (This is true by design in the federation server, but should be a more general policy.) This last characteristic is fundamental to avoid recreating echo chambers.

This means that knowledge realms are a first class object in any tool that allows collaborative edition, and in the federation servers; setting the merging policy, and taking human actions according to that policy (vote, consensus, argumentation…) are fundamental knowledge work activity and should be explicit.

##### Data model for event sourcing

Technically, what we are doing is distributed event sourcing. Let’s dive into a possible structure for this.

Let's use @id for (local) live id of a concept or event, and @tid for time-bound snapshot id.
There is a way (like IPNS) to get from @id to @tid in at least some cases.
The root of the description is the domain.
The domain's description (probably won't change much) has the following attributes:
hk:eventStream The @id of the event stream
hk:lastSnapshot The @id of the last snapshot.
hk:eventStreamEndpoint an endpoint for subscribing to events (optional, in server mode.)
Dereferencing the eventStream gives us the latest event @tid.
Each event gives us an operation, and has hk:ancestors, a list of @tids of previous events.
There can be multiple ancestors in the case of merge events, mostly; otherwise the events are a linked list.
It is possible to walk up the event chain to obtain (slowly) a list of events and reconstruct the state thus; but in many cases, we'll hit a snapshotEvent fairly soon, which is a special kind of event.
A snapshot event (equivalent of publishing) will generally contain a map: @id->@tid for every concept in the domain (or for a self-contained subset).
The @id of the lastSnapshot will also map to the last (global) snapshotEvent.
So in general, a client will navigate the data thus to get a recent-enough snapshot:
domain @id -> domain @tid -> domain description data: lastSnapshot @tid -> lastSnapshot @id -> lastSnapshot: concept @tids (for relevant @ids) -> concept data.
But a client interested in staying up-to-date may subscribe to the eventStreamEndpoint, and will certainly traverse from the @tid of the last event on the eventStream to its ancestors until it encounters the know lastSnapshot, and can reconstruct from there.
Note: It is likely that this grand event mesh can use IPLD principles, as it is made of invariant events, even if the concepts are mutable.
Note 2: Some knowledge realms, especially external to the ecosystem, will not have an event stream, and will only have a latest snapshot. Some important functions of the ecosystem rely on semantic events, however, and ecosystem components should avoid going that route.

Q: Now how self-contained should the concept data be? Maybe it makes sense to use GL’s rich representation there, where associations and feature values, though independent entities, are folded into each node’s representation. My tendency (MAP) would be towards more atomic objects, avoiding redundancy; but it multiply fetches.
In a client-server space, it may also be possible to create ad-hoc subdomains, to subscribe to a fraction of events that pertain to a given subset of nodes; or even to request a limited snapshot for a similar subset. (This is related to the section on live queries.)



(the following text is an earlier version, still relevant but needs to be merged back.)
The interesting case is when a merge conflict happens, because different people give different values for the same (single-value) feature, assert different associations, etc.: it is a service concern whether trivial divergence are merged automatically; in some cases, humans will be alerted to the divergence. They may then create a merged node. This is again consistent with the github model. The merge may or may not be accepted by other people in the ecosystem, and then a concept has forked.

So, when asking a federation server about the latest snapshot for a given concept, we may obtain multiple merge heads, belonging to different knowledge spaces, corresponding to a bona fide divergence of opinion. In general, we will aim to "agree to disagree", i.e. propose a mutually agreeable common abstraction under the old live identifier, and assign new identifiers to the variants. (with a dedicated binary association to the original, such as hk:splitFrom.)

However, failure to converge is possible, and is not a bug but a feature: knowing how many different merge heads exist for a given base concept, and how many and which servers have included a given merge head in their context, conveys real and valuable information about actual community splits. In some cases, it would make sense, in a given domain, to transclude another version of the "same" concept obtained from another domain.

#### Merging

A key function of federation servers will be constructing merged concepts, often with concepts from multiple sources.  (These are above and beyond variant merges discussed earlier, but may use the same mechanics.) Merges will often be debated, so any merge decision should be an event with provenance and knowledge space. The features of the merged concept will be a function of the features of the merged concepts and a merging algorithm, which should also be part of the merged feature provenance.

Placeholder: Compare explicitly GL’s transclusion merges and merges.

##### Mutual merges

When two federation servers merge one another's concepts, we have a circular merge dependency. This case needs to be either avoided (by establishing a hierarchy) or handled well.

One failure scenario is that provenance information in server A is likely to refer to snapshots in server B, but creates a new version in server A which will generate a new version in server B, and so on. A related failure scenario is that the merge algorithms in each server fail to converge, leading to a livelock. Known solutions exist for single-process systems but do not extend well to distributed systems.

It would be better if the merge nodes of both servers used the same identity, and synchronized values through “merge operation” actions, again like a merge node in github. A merge initiated on one server being adopted by the other server cuts the chain.

#### Local and remote

Often, a federation server will have its own local copy of a concept defined by another service; the local copy will thus have a local identity (a URL on the federation server) distinct from the concept's original URI on the originating service. That URL for the local copy is distinct from the merged copy (below.) Data originating on the federation server URL MAY use the local copy URL as the primary identifier, but MUST then mention the original URI with the `hk:localCopy` relation. If the federation server uses the original URI, it SHOULD then advertise its local URL access point for the copy with the `hk:copyDataSource` relation. (Q: maybe we should only allow the latter?)

### Features and associations

Concept features (including even type assertions) can be problematized and as such made into concepts. Such concepts will have their own identity as well as being a feature of the original resource. In many cases, the feature will have a single value; in other cases, multiple differing values in different knowledge spaces. (This is distinct from a single value which happens to be a collection of values, or a multilingual string value...) One advantage of developing the features into separate concept is that each feature block can have its own [provenance](https://www.w3.org/TR/prov-o/) information. On the other hand, a client that just wants to know the value of the feature has to understand scoping. We must have a very simple way to designate the local knowledge space. (See knowledge space definition in next section.)


The problem with all this is that there are a multiplicity of ways to represent the data neighbourhood of a resource: The resource itself is little more than its identity, knowledge spaces and endpoints, but any reasonably useful representation must include at the very least features valid in the default knowledge space and references to associations valid in the default knowledge space. It is likely that a useful base representation would actually contain the default-knowledge space associations themselves.

Some services in the ecosystem, working on a single knowledge space, will use simpler (single-value, unambiguous) feature and association descriptions, probably something closer to `owl:ObjectProperty` or `owl:DatatypeProperty`. This is why we have to define the equivalences between such simple properties and multiple properties.

### Ontology

1. The ontology specification language must be rich enough to express [conceptual graphs](https://en.wikipedia.org/wiki/Conceptual_graph): it must be easy to refer to a graph subset.
2. The ontology specification language must be rich enough to express [topic maps](https://en.wikipedia.org/wiki/Topic_map): N-ary associations with roles.
3. When objects are merged, we can refer back to the original merged objects. (i.e. merge is non-destructive.) The merged object has its own identity, and values of attributes can be complex combinations of attributes of original objects, holding provenance information.
4. The ontology specification language must allow definition of classes, properties, association classes, and association roles. The definition may have a human-readable aspect. There will be inheritance between classes, etc., which will be used to navigate the graph across class equivalences.
5. The ontology specification language must be rich enough to express itself.
6. The ontology should have a natural mapping to RDF machinery, as it will be expressed in JSON-LD.
7. Though we want to be able to express higher-order constructs than RDF, sometimes it is convenient to express them as RDF. For example, a property with provenance is a full-fledged resource, but could be expressed as a property literal. The correspondence between those should be implicit.
8. The ontology should be dynamic, as changing the ontology is sometimes the best way to add capabilities (applicable service descriptions, qv) to a resource.
9. This dynamic ontology should rest on a stable core, that will form a basis for interoperability.


So the key concepts of RDFS and OWL are still relevant: There will be a mapping between `owl:Class` and `hk:Class`, `rdf:Property` and `hk:Property`. However, most `owl:ObjectProperty` will be modelled with a `hk:Relation` entity (a subclass of `hk:Class`), connected to other entities through `hk:Role` objects (which will be subclasses of `owl:ObjectProperty`.)

When communicating with other ontologies, there will be an inferred correspondence between binary `hk:Relation` and `owl:ObjectProperty`, as the RDF rule machinery allows.

#### There is a concern with porting practices from RDF namespaces. It is assumed that the number of ontologies is comparatively stable and low; this allows to uniquely namespace ontologies in CURIEs, and even to flatten those names in JSON-LD. However, if metadata is more dynamic, we will have multiple ontologies, and perhaps even multiple versions of ontologies in different knowledge realms. This makes sense for data, but could be problematic with metadata, and makes namespacing more difficult. Without such namespaces, we may not be able to use CURIEs, especially not in normalized JSON (we should normalize before putting in ipdf, for meaningful identity.)

#### Ontology terms

Note: It is possible that some properties are structural, and as such cannot be reified into a Feature. candidates include inKnowledge space, creator, dataSource... They definitely do not include `hk:Type`, `hk:isMerged`

Note 2: all term names under the hk namespace are negotiable.

`hk:Concept`
:   The root class for concepts. (~`owl:Thing`)

`hk:Class`
:   a metaclass of concepts (aka topics). The main metaclass. (<`owl:Class`)

`hk:Relation`
:   The metaclass of concepts which are associations (<`hk:Class`)

`hk:Role`
:   The metaclass of association roles (<`owl:ObjectProperty`)

`hk:BinaryRelation`
:   The metaclass of directed binary associations (<`hk:Relation`)

`rdf:type`
:   The relationship between a class and an instance

`rdfs:subClassOf`
:   The relationhsip between Concept or Relation Classes.

`hk:associationProperty`
:   Associates a `hk:BinaryRelation` with a corresponding `owl:ObjectProperty`, both when interoperating with owl data, and as a shorcut. TODO: maybe also define some associated properties for role paths in N-ary associations.

`hk:SubClass`
:   The `hk:BinaryRelation` that corresponds to `rdfs:subClassOf` (through `hk:associationProperty`)

`hk:Type`
:   The `hk:BinaryRelation` that corresponds to `rdf:type` (through `hk:associationProperty`)

`hk:Feature`
:   The metaclass for a concept property, realized as an entity with provenance. (<`owl:ObjectProperty`) (Q: We could make it <`rdf:Statement`, and reuse `rdf:subject`. We could also infer the `rdf:property` from the `hk:featureProperty` of the `hk:Feature`)

`hk:hasFeature`
:   associates a concept instance with a feature instance. (<`rdf:subject`?)

`hk:featureProperty`
: Associates a `hk:Feature` class with a corresponding `owl:DatatypeProperty`, for interoperability and as a shortcut.

`foaf:primaryTopic`
:   States that the document illustrates a concept.

`hk:localRepresentation`
:   States that the document is the main representation of the concept, and can stand in for it in linear representations of the concept graph. (<`foaf:primaryTopic`) Locality refers to the service hosting the concept. (or the current knowledge space? There should be a N-ary association for this.)

`hk:localCopy`
:   Associates the identifier (a URL) of the local copy of a concept on a federation server with the original identifier(s) of the concept on its origin services. (<`owl:sameAs`)

`hk:dataSource`
:   Advertises a URL where information about a concept may be retrieved. (Note: maybe add scoping information?)

`hk:copyDataSource`
:   <`hk:dataSource` where the information is explicitly derived from other sources (link to those sources?)

`hk:inKnowledge space`
:   a concept (association, feature...) is defined in a given knowledge space. (TODO: work out the association with data sources)

`prov:wasAttributedTo`
:   a concept (association, feature...) was defined by a `prov:Agent`.

`prov:wasGeneratedBy`
:   a concept (association, feature...) was defined through a `prov:Activity`. Especially relevant to merged concepts, features and associations, so we can retrace how the merge was constructed from original merged entities.

`hk:MergeEvent`
:   A decision was taken to merge concepts. (<`prov:Activity`,`hk:Relation`, Q: should we distinguish event and association?)

`hk:isMergedIn`
:   The `hk:Role` of the merged concept in the `hk:MergeEvent`

`hk:isMergeOf`
:   The `hk:Role` of the merging concept in the `hk:MergeEvent`

`dc:title`
:   the document's title

### Events

(See the previous section on the event model first, what follows is an older version.) 

All resource changes should be described as change events, as per [ActivityStreams](https://www.w3.org/TR/activitystreams-core/). We will at least use `as:Create`, `as:Delete`, `as:Update` as low-level events. (Actually: Use [Automerge](https://github.com/automerge/automerge)) It would be desirable to use higher-order event types defined in the [ActivityStreams Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-update), and maybe define a few more.

Every resource should have a reference to an activity stream, whether on a local or remote server, or on a peer-to-peer stream such as a [DAT hypercore](https://github.com/mafintosh/hypercore) or [ipfs-log](https://github.com/orbitdb/ipfs-log). Also relevant: [DAT-messages](https://github.com/datproject/discussions/issues/62). The stream may be uniquely specefic to this resource or more generic.

This activity stream should allow both GETting (paged) and POSTing new changes. (Q: should we have separate inbox and outbox as in [ActivityPub](https://activitypub.rocks/)? ActivityPub is social-centric, but can be repurposed to be resource-centric.) Posted Activities are requests, and may have an identity distinct from the "accepted" Activity when it is read back. In which case, the Accepted activity should contain the identity for the Activity request, and optionally an endpoint to retrieve it. POSTing activity requests is preferred to direct REST-ful operations on the resource endpoints, following [CQRS](https://martinfowler.com/bliki/CQRS.html).

Note on event sourcing: We should avoid storing personal information in an immutable event log, to allow GDPR “right to be forgotten”. So events should be associated with a pseudonymized identity, and the correlation of those identities with user data should happen in a separate, non-immutable data structure. (Or an immutable data structure with a per-user disposable encryption key.)

### Service description

Services enact data transformations on both concepts and documents. Machine-readable service description may be out of knowledge space, or may use the (rather heavyweight) [Web service description language](https://www.w3.org/TR/wsdl20-rdf/). What we do need to know is which services are available for a given document or concept type. This should be part of the type definition.

The services may offer a simple RPC interface, or may allow for subscriptions to change events in an underlying dataset, or reactive computations based on such changes. In some cases, a service would act as a service factory, allowing subscriptions to reactive computations to a provided data stream. (See Live Queries below.)

### Associated documents

Concepts and documents are closely associated, and are supposed to be interchangeable as follows.

A composite document is made of sub-documents. Those sub-documents may be document fragments or another composite document, recursively. Document fragments may be of any mime type, including HTML. Composite documents will be a new mimetype (based on JSON, comprising references to the composing documents and viewSpec information.).

Most documents have two forms: raw (any mimetype) and augmented HTML fragment. The latter uses HTML and RDFaLite to convert and/or wrap other documents in a HTML fragment containing its own metadata. (media objects are mostly wrapped, textual formats are converted.) In the case of a composite document, the components are resolved to their augmented HTML fragment recursively.

(Note: almost everything above could be rewritten with conversion to augmented PDF fragments instead of augmented HTML fragments, but the composition would likely be less straightforward.)

The augmented HTML fragment also has a structure that allows applying viewSpecs explicitly, with useful CSS names, and external WebAnnotations are resolved to spans if appropriate. However,  it is the result of a conversion operation; internal storage would be raw HTML or other formats, and Eedition operations, on the other hand, are generallywould also be done on raw documents.


Manipulation of augmented HTML (edition, rearrangement, altering viewspecs…) involves a family of Javascript operators, according to enclosed document types; those are naturally applied on the complete augmented HTML document, a final wrapping step for a root augmented HTML (and another distinct mime type.)

Each document fragment is a RDF resource, and is treated as a concept for document composition purposes. Some document fragments may also be said to illustrate a particular concept (`foaf:primaryTopic`), in some cases canonically (`hk:localRepresentation`). Thus, it is possible to create a linear document representation of a knowledge graph from a tree traversal specification of said graph, using canonical representations. Conversely, a linear document (as represented by a root composite document) implicitly contains at least a concept graph consisting of the document-fragments-as-concepts, and potentially another one referring to the primary topics of the concepts.

In some cases, the tree traversal can be obtained through a traversal strategy specification, even if the underlying data is a graph. This specification may be executed locally or on a distinct service.

It is possible to include a reference to a raw concept directly in a composite document; its augmented HTML fragment representation would then either use the canonical illustrative document if any, or alternatively wrap the JSON-LD with some textual or graphical representation of the local knowledge graph. (The choice is a viewSpec concern.)

Conversion and wrapping from basic document types (at least HTML, composite document, media types, and raw concepts) to augmented HTML fragment (and augmented HTML document) will be made through a standardized library (Probably JS at the outset.) This conversion and wrapping should be made available as a webservice. HTTP services in the ecosystem should make those conversion automatically through content negotiation.

Here is a simplistic example of how some aspects of an augmented HTML fragment would look like:

```
<div class="composite_document" id="#md_doc_f879">
<script type="application/ld+json">
{
    "@type": "CompositeDocument",
    "@id": "#md_doc_f879"
    // other metadata
    @content = [
        {
            "uri": "#md_content_56789",
            "css": "..."
        },
        ...
    ]
}</script>
<div anchor>

    <!-- fragment resolution -->
    <div class="document_fragment" id="#md_content_56789">
    <script type="application/ld+json">
    {
        "@type" = "DocumentHtmlFragment",
        "@id": "#md_content_56789",
        // other metadata
    }</script>

    <div class="content" id="#d_content_56789">
    some html
    </div>


</div>
</div>
```

Note that the composite was resolved into the sub-fragment; each level has is own metadata. The class and id attributes are mostly there as hooks for optional CSS.

#### ViewSpecs

The definition of viewspecs requires a lot of refinement. We mentioned wrapping components and sections in divs with standardized IDs based on the document fragment IDs (maybe hashed); this would allow many viewspecs to be implemented with CSS. Also, it must be easy to overlay a new viewSpec on an existing one, including the viewSpec elements embedded in the document structure. No doubt traversal specifications will be crucial here.

#### References to concepts

It is possible to include a reference to a raw concept directly in a composite document; its augmented HTML fragment representation would then either use the canonical illustrative document if any, or alternatively wrap the JSON-LD with some textual or graphical representation of the local knowledge graph. (The choice is a viewSpec concern.)

It is also possible for an external process to create a WebAnnotation that references the appropriate text target in the document structure (there may be some reference wrangling if the reference is computed in the augmentedHTML, as the annotation should ideally use the rawContent as a point of reference) and the annotation as a whole would refer to he concept.

#### Hyperglossary

A word on hyperglossary: it simply refers to a certain usage pattern of the above mechanisms, where references to concepts in the text (or added from without as WebAnnotations) can be the anchors for ViewSpec-directed affordances that allow to explore the global knowledge graph from that concept occurence. It should also be possible to design text editors that can ask federation servers for likely concept identities given a run of text, and can create those concept references in the document data.

### Constraints

* We absolutely need subclasses to navigate the graph. (MVP)
* Domain and range constraints are helpful to edit the graph.
* Some cardinality constraints, including functional nature, minimal cardinality, etc.
* All constraints apply within a given knowledge space by default, some may apply to the global union knowledge space.
* We would like to express that certain role-association paths should form a strict tree (vs graph). (useful for document structure!)
* Also, impose reachability (i.e. find orphan: MVP?)
* Constraints expressed as most general forbidden graph (Mineau)

### Global link model

About internal links vs links coming from the outside through WebAnnotations (To write)

## Hyperknowledge protocols

Different components of the ecosystem will share data. This will be done by a mix of RESTful APIs (mostly for reading, since that’s cacheable), following [Linked Data Platform](https://www.w3.org/TR/ldp/) principles with [Memento](http://www.mementoweb.org/guide/quick-intro/); Event streams for each resource, following [ActivityPub](https://activitypub.rocks/), for updating; and some event streams could use peer-to-peer serverless architectures, such as [IPFS](https://ipfs.io/) or [DAT](https://datproject.org/).

### Once the use of distributed file system is in scope we have an opportunity to capture “Thought vectors in concept space” globally in a distributed way and retrieve relevant things without “searching”.

The scheme described below  is an optional scheme but much recommended.
In effect it gives a “semantic identity layer” to Distributed HyperKnowledge Federation.

Consider that for every node we create we designate two or three words (wordgrams)  in a specific sequence that characterize and kind of identifies through associations the intended meaning of the content, information contained, a kind of meaningful coordinates.
We can then use an IPFS  hashing algorithm to assign corresponding ids.
Then have a global key value store or anything else to get the actual node data.
Also have a wordgram graph built so that we have a rich auto associative context that emerges through use.

### Federated time machine

(GL) Consider using content addressability ala IPFs and references to snapshots in Augmented HTML documents (hence graph transclusion) as a built in (to the federation protocol/server) mechnanisms for “version control”, publish events, and or time machine for the unfolding graph.

MAP: I believe references should minimally have timeless identifier + timestamp (or lamport clock) as described here. They may optionally include a timestamp identity; in the latter case, IPFS reference is a good way to have such an identity.

### Content negotiation

Besides the layer of content negotiation implicit in the LDP, there should be content negotiation at the underlying data level.

#### Concepts

Though resources will have a "natural" representation, comprising mostly local facets and associations, each of those is really an entity in its own right. There can be a huge amount of such facets and associations, especially in federation servers. We have to balance between the needs of efficient queries and result size.

Possible options: (maybe complementary)

1. Use GraphQL to tailor query results (pro: tooling exists. con: the tooling is heavy.)
2. Use HTTP Prefer as in [LDP](https://www.w3.org/TR/ldp/#prefer-parameters)
3. Use paging for large-scale data
4. Use subscriptions

##### Documents

Some document viewSpecs could be defined at the HTTP negotiation level; in particular augmented HTML (fragment or self-contained) should have a mimetype, and the conversion from base HTML to augmented HTML, and the wrapping of other document types in augmented HTML fragments, should be done through content negotiation.

### Graph traversals

We should be able to describe complex graph traversals of the hyperknowledge structure in a manner that can be interpreted on either clients or servers. The easiest descriptions are procedural (à la [Tinkerpop Gremlin](http://tinkerpop.apache.org/docs/3.3.2/tutorials/gremlin-language-variants/)) but maybe we should consider a declarative language akin to [Cypher](https://www.opencypher.org/).) Either of those needs to learn about knowledge spaces, roles and multiple feature values.

## Previous draft (2)


### Conceptual model

We propose a base model for knowledge representation. The primary aim
is to describe and represent dynamic aspects of mental models, how they
can diverge and be made to converge again. We will also develop a
representation of that model suitable as a communication layer between
knowledge applications. We are using aspects taken from TopicMaps
[TMRM07], Conceptual graphs [Sow08], RDF, Wikidata, AtomSpace
[GPG14], Transitional Modeling [Rön18] and other models of
knowledge.

#### Topics, statements, and perspectives

The primary units we are working with are topics, statements and
perspectives.

A topic is anything that can be thought or spoken about. Statements
relate topics to one another, or sometimes to literal values. Note that
all statements are topics, though the reverse is not true. (But a topic
will often be introduced in an initial statement, and we may sometimes
identify the topic with that statement.)



A statement is made by an agent at a certain time, from a certain
perspective. The statement also applies to a certain time, which is
distinct from the time of articulation. The notion of perspective is not
unlike that of mental space \[Fau94\]; an agent can hold multiple
perspectives, for example subjunctive worlds or mental image of
another's agent's perspective. We will focus on perspectives as
communicational artefacts; a perspective will be defined by a sequence
of statements that are said to hold in that perspective, about a certain
number of topics relevant to that perspective. The notion of perspective
can also be compared to subgraphs in conceptual
graphs\[Sow08\].



\[Note: I will use Sans-serif for the conceptual model, and serif fonts
for the protocol data model.\]



Concretely, those entities are modeled in a given knowledge application
which will be known as a Realm; an abstract topic, which is an entity in
the mind, has a unique local identifier in a realm, which acts as a
topic namespace. Perspectives and statements are also local to a Realm,
but a statement can be a localization of a foreign statement (i.e. from
another realm), and among those statements, a local topic can be said to
correspond to a specific foreign topic.



There is an underlying intent that the topic is \"the same topic\"
across time and perspective, just as we hope to be referring to
comparable concepts (brain states) in different people when using the
same word. Sometimes we use the term topic to mean what is identified
across realms (*abstract topic*), sometimes what is identified by a
topic identifier within a realm (*local topic*), sometimes the more
precise *situated topic* within a perspective, sometimes the *topic
state* fully determined by a universal topic locator (q.v.) that
includes a point-in-time position in the series of statements of a given
perspective.



An elementary statement is like a Wikidata statement (more accurately
the main snak), or a named RDF triple; it ties a topic, predicate and
value. The value can be a literal or a reference to another topic. In
particular, unlike in Wikidata, the value can be another statement.
\[Question: should I still have explicit statement qualifiers, or can
all statement qualifiers be treated as statements of their own? Some
aspects of the statement, such as timestamp and agent, will be modeled
using qualifiers; but I'm thinking about wikidata-style qualifiers such
as numeric precision, time period, etc.\] \[Also: When does a literal
become a topic?\]



A statement can also override a set of other statements. Active
statements in a perspective is the union of all asserted statements
minus the set union of all overridden statements from all asserted
statements. (Overriding cannot be negated, you have to reassert a new
statement.)



A composite statement refers to a set of elementary statements that
forms a coherent unit (using some application-dependent notion of
coherency.) \[Question: does composition need to be recursive? Not
convinced for once. Question 2: how necessary is it for elementary
statements that are part of a composite to be referenceable? I'll go
with yes for now.\]



We said that statements are made sequentially in a Perspective, and
have a sequential ID. (and/or timestamp) More formally, a Perspective
owns a queue of events, most of which are statement events. Events have
a sequential ID in the perspective. (The lamport clock could be
realm-global?) The statement remembers the perspective and sequential
event ID where it was introduced. A perspective may inherit statements
from other perspectives in the same realm; in this case it inherits all
statements relevant to its own topics up to a certain sequential
id.



Most elementary events will introduce elementary statements.
High-level, application-dependent composite events will unify related
events. Events will thus be in a containment hierarchy. We will refer to
the top of an event hierarchy as the top event. Composite statements
will be contained within a high-level event boundary. (Though a
high-level event may contain more than one composite statement.)
Statements will have the sequential ID of the (top?) event where they
were introduced, and we will interchangeably speak of the sequential ID
of the statement or its introducing event.

#### Remote perspectives and local references

A local topic T~1~ in realm R~1~ may be a local adaptation of set of
foreign topics from other realms.

In that case, R~1~ will hold mirror perspectives P~1~, \... P~N~ to
reflect the statement sequence from perspectives of those foreign realms
containing that topic. (In theory, we could have foreign event
references, but this is a recipe for a realm becoming unusable because
it depends on unreachable events when a realm becomes
unavailable.)



The perspective will keep track of topic equivalences: In perspective
P~1~ of realm R~1~, Topic T~1~ corresponds to topic T~2~ of realm R~2~
in perspective P~2~. This is a statement, thus local to a perspective.
The same topic T~1~ in R~1~ could also have been made equivalent to
Topic T~3~ of P~2~ in a different perspectives P~3~ of R~1~. But in
general, a perspective will either be a partial mirror of a foreign
perspective or rely on mirror perspectives. Direct cross-realm
references from a non-mirror perspective should be rare (because they
are inherently more fragile.)



The set of events that apply to a topic (and hence its state, and
perhaps its identity) is thus given by a *universal topic locator*,
(UTL) composed of the following components:



1. Realm identifier

2. Topic identifier within the realm

3. Perspective identifier

4. Position in the event stream (given as an event identifier or a
timestamp.)



(This should take a URL form.)



Internally, topic reference in the statement will often be encoded
simply with the pair (topic, perspective), i.e. refer to a situated
topic.

We're trying to balance the fact that the statement refers to a topic
as it is at the moment the statement is made, and further updates to the
target topics may invalidate the statement retrospectively; vs the
prohibitive labour of updating every statement about a topic every time
the topic changes. The proposed compromise is as follows: When a
statement refers to a situated topic in the same perspective, it is
implicitly to be checked for consistency when the topic is updated. The
perspective is in charge of keeping its local statements up-to-date. But
the reference to a topic external to the perspective is frozen in time
at the moment of the latest event inherited from that perspective.
Statements should be re-checked when new events from that perspective
are merged. So the point-in-time reference of the topic is implicit in
most cases. (We would still allow for more precise point-in-time
references, ie. full-form UTLs, but those would be more
rare.)

#### Perspective forks and merges

We said a perspective owned an event queue. A perspective can be based
upon any number of other perspectives, and assume that all events in
those perspectives, relevant to its target topics, up to a given
position, are part of its own event chain. This is itself expressed with
a merge event; A merge event states that perspective P~1~ takes on
events up to sequence Id K from perspective P~2~. A new perspective P~3~
can thus easily be forked from P~1~ by starting its event queue with a
merge event based on the current position of P~1~. In that case, the
merge event should also explicitly include all other local perspectives,
with the position in their event queues, already merged in P~1~, such as
P~2~. (Making it implicit would make finding the positions in the past
absurdly difficult.)

At any point, if we want to update perspective P~3~ with the new events
in P~1~, this is a new merge event. If P~1~ already merged new events
from P~2~, we also have to update P~3~ to take those events into account
in the same merge event.

Note that a merge event may invalidate any or all of the statements
contained in events it imports; but it has to do so explicitly. The
position of the perspective pointers in a given perspective can be seen
as indicating that one perspective is at least aware of the other
perspective up to that point, whether it agrees or not.

Also note that, if a new topic is added in the scope of a perspective,
many new statements from many new events from many old foreign event
queues will suddenly fall into the scope of the perspective.

This is especially complex in the case of a mirror perspective, which
may not have the relevant events, and will have to query the remote
realm for all past events related to this new topic (and its
closure.)

#### Topic closure and snapshots

The set of statements that involves a topic is a natural unit, but not
necessarily the most natural unit. Certain statements are more tightly
bound to their topics than others, and we will speak of the closure of a
set of topics to include all statements bound to those topics through
certain types of links and roles, topics bound to those statements, and
recursively. (Note: specifying which bindings will be traversed in a
closure operation has to be directional...) Among high-binding roles, we
have those involved in typing relationships, topic merge relationships,
topic basis relationships, and topic naming (to be defined later). When
importing topics from one realm to another, there is a minimal set of
linked topics that have to be imported with it for it to make sense,
such as, for example, its class hierarchy. So any cross-realm topic
request will not only give events for the topics asked for, but for
their closure. This need not absolutely apply to cross-situated topic
inclusion, since the closure topics are readily available. However, we
have to be able to calculate the state of the closure topic, as
implicated by all statements in all referenced perspectives... so
probably worth applying the same logic.



Either an elementary or composite statement has an outlying signature
of attached topics; so any statement subset can be transformed into a
closed statement subset by adding composition and core relations.
Coherency verifications should apply to closed statement
subsets.



Also, for performance reasons, we would want to create snapshots of a
topic's state, so that reconstructing an event's state does not always
have to go from the beginning of the statement chain; we could look from
the snapshot onwards. The snapshot will not always include the entire
connectome of the topic (i.e. the set of all the relations it's part of)
but a subset of it; and a subset of related topics that are central to
the topic's state. This is also a sort of closure.



The definition of closure should be partly application-dependent, but
we have to both detect closures that include too many nodes and have a
base closure set for essential relations.

#### Base relations

Among base relations and roles that have to be part of the protocol's
definition, we have:

1.  Typing relation (instance, subclass) for topics

    a.  Keys for literal attributes or casting are types. There is argument about whether to use inheritance there. (I think we have to, to be compatible with RDF)

2.  Topic merge relationship (not to be confused with perspective merge... maybe call this topic fusion instead? I know topic merge is established, but so is fork/merge.)

3.  Topic basis relationship: This topic is based on another topic in another realm

4.  Perspective inheritance: This perspective uses events up to id N from that other perspective

5.  Naming: We can say that a global name, such as a RDF URI or a wordgram, corresponds to a topic. There should be only one topic corresponding to a given global name in a given perspective.

#### Reification

One difficulty with statement structure is that there are many models
of relations. Some systems, like RDF, build all relations from binary
relations; N-ary relations have to be made into subjects; and using a
binary relation as a subject requires reification. In other systems,
like Wikidata, those binary relations are statements, and can be subject
of other triples, but those level-2 triples cannot be subjects. Because
of the variety of modes, we have to allow reification as an event.
Basically, the most elementary parts of a statement, such as a casting,
may be given a temporary, descriptive identity (which could be a
composite of subject, relation type and role for example) but whenever a
foreign realm tries to refer to that casting as a subject in a relation,
the base realm would implicitly reify the casting to a full fledged
topic, and signal the change of identity in an event. This event might
require the reformulation of dependent events, though ideally the new
topic would retain the original composite name as an alternate name and
that name could still be used.



One key intuition of PowerLoom[?] is that the transport system must at
some internal level identify some statements whose representation
varies, such as inverse relations, or reified or unreified statements.
What this identification means beyond aliasing still requires thought.
The interaction with topic merging also bears investigation.

#### Comparing statement models

We are trying to unify many models of statements.

RDF statements are unnamed triples. They can be reified, but the
relationship between triples and their reification is
undefined.

In wikidata, a statement is essentially a RDF triple, based on a topic,
qualified by other triples that have the statement as a subject. (Thus
the statement is referenceable.) The namespace of those triple's
subjects and predicates, and the statements themselves, are kept apart,
so we cannot naturally make a statement on a statement, only alter the
qualifiers.

From the topic mapping viewpoint, a statement could be said to be
either a valuation (i.e. a literal triple) or a relation (i.e. a
key-value dictionary where keys are role types and values are topics.)
Other statements (merge/unmerge) can be thought of as relations.
Relations can have a scope.

From the AtomSpace viewpoint, atoms have a key-literal dictionary, and
links are atoms which also have an atom tuple. If you want literals in
the tuple, you would wrap the literal in a value-bearing atom. The
statement model that would emerge would be either tuple assertion, or
key-literal pair, but the latter is then unreferenceable unless
reified.

Marid, Triaclick and ZigZag use tuples; all literals are reified in
Triaclick, so tuples are universal; I think it's the same in ZigZag;
Marid tuples have a payload.



The issue with tuples is that content-addressability does not play well
with identity of mutable objects. In any relation, one can argue there
are essential identifying roles, and secondary ones. But there can be
hesitation about the value of the casting of even essential roles,
though we have reasons to say that those different tuples refer to the
same relation. Also, identity-definition should be application
dependent, (notion of legend in Topic Maps) which clashes with tuple
definition.

It is tempting to add secondary roles as extra links, but a
too-restrictive signature bans multiple conflicting versions; a
too-extensive signature makes searching difficult. All in all, the basic
solution is the same as always: reify on demand, making contentious
relations into entities. So any search for a pattern will actually look
for two patterns. (Ah! we could have multiple reifications for a base
tuple... really needs thinking about in detail. It has to be a storage
artefact.)



The most generic data model would allow a dictionary of role to sets
and/or lists of literals or topics; this was originally implemented in
HK (with sets). But it's so general that implementing it in more
restricted models is demanding. (Note that each element's inclusion in
the set was a separate statement/topic. This allowed greater
granularity, but sometimes it's useful to refer to a coherent set as a
whole. This was done using event composition in HK.)



The reverse approach is to have a minimalist data model in the
protocol, and to build more generic data models above it.

Having natural mappings to important data models (esp. Wikidata, maybe
RDF) is an important secondary goal.

HA! Event composition leads to statement composition. We have topic
composition in the sheaves paper, we can have statement composition.
This makes micro-addressability and macro-addressability work
together.

##### Talking about statements

GL: On possible idea to handle talking about statements



In wikidata parlance introduce and addition entity class along with Q
and P, let's call it R

By protocol it will have three built in properties

statement subject

statement predicate

and

statement object



Now we have available a node in the HyperKnowledge Graph that can have
access to what is being talked about and a place to add any statements
we care to think about



The very same machinery that we have, whether it is RDF store, or
protocol event model or in memory json we can play the same game what we
start with by supporting WikData model

We can sell this to say, hey we have just overcome one of the
limitations of the current model with minimal cost



Sorting out roles will lead further away



But the approach in my current thinking is to offer a level of higher
expressive power that can turn dumb linked data to be worked on as if it
was say topic model or even something more powerful



Discussion MAP+GL: Do we need inheritance at the protocol level? It
seems wikidata does not use sub-properties. To be discussed. Of course
at the query language level we'll have to support recursive
classification.

#### Comparison with topic maps



We are taking as a basis the [Topic Maps Reference
Model](http://www.isotopicmaps.org/TMRM/TMRM-7.0/tmrm7.pdf),
where everything that can be talked about is a *Topic*. That includes
notably relations between topics, resources (digital or not, as in RDF),
etc. (As in RDF, the topics may be metadata for external resources, but
not necessarily.)



Topics are typed, and types have multiple inheritance. To interoperate
with other models such as RDF, we will allow topics to have multiple
types, though this is not part of TMRM.



Every topic has attributes; the target of the attribute, a literal, may
also be a topic. (Q: Is it always a topic? Do we allow
\"bare\" literals?)

Note that the fact that a topic **T** has an attribute of type **A**
with value **V** is itself a topic; we call those key-value pairs
*attribute bindings*, and their types are the *Attribute*s
themselves.



*Relations* between topics are (typed) topics; each topic designated by
the relation plays a *role* (also a type) in this relation (and we speak
of the *players* in the relation. Each binding of an player in the
relation is a *casting*, whose type is the Role.



In traditional topic mapping, the set of attributes that uniquely
determine a topic\'s identity is application dependent, and constitutes
a *legend*. So in a given application, we may discover that two
different topics are equivalent, and merge them. The merged
topic can be merged in turn, etc. When dealing with any topic, we will
often want to deal with its *merge head* instead, i.e. the result of
transitively following the merge relation. Topics can also be
unmerged.





Each topic has a biography: what attributes and relations were asserted
by whom. This biography will take the form of an event
stream.



Note: to simplify the model and prevent infinite regress, some simple
binary relations, especially type, will be modeled with
role bindings being treated as part of the relation rather than as
distinct objects with their own identity, relations and
biography.

#### Events, state, identity and versioning



First, we will allow for the fact that any representation of a topic in
a given information system has an identity in the database (or
equivalent structure) of that system. This gives us a unique identifier
for the topic relative to that system. (We call such systems that define
local identifiers *Realms*)



We will describe the state of the topic map as a whole as a succession
of immutable events. Some events will be *elementary topic events* and
describe changes in the topics; others event types (out of scope here)
will describe changes in designated resources (according to resource
type); yet other event types will be *ad hoc* high-level *composite
events*, which translate to elementary topic or resource
events.

#### Realms and perspectives



Each event exists in an event stream, and every event stream exists
within a realm. A realm is basically a "naming authority" for events and
topics, and every topic has a unique identifier within a realm.
Different event streams may make different statements about the \"same\"
topic (i.e. having the same realm-local identifier.), representing
different *perspectives* about a set of topics. Such
different statements may change identifying information about the topic
(in terms of the legend), which may vary as new events are added, or
between different event streams.



[Note that event streams will often build on one another, in the way of
git branches (if you think of each changeset as an event.) This is how
someone can build a perspective on (an)other perspective(s). So each
perspective will name other perspective, and a position in that
perspective's event stream, whose events are considered part of this
perspective as a starting point. (This is developed
[here](#whats-in-a-name-link-knowledge-graph).)





Topics may also have *global names*, such as URIs (esp. coming from
RDF) or [wordgrams](#on-wordgrams); many distinct local
topics may be designated by a given global name, as different realms or
even perspectives may want to split topics differently.



Note: It is possible to use wordgrams as local identifiers, but we have
to be especially aware that the local identifier must remain unique when
a topic is split, whereas the wordgram should ideally still refer to
both topics after the split.


##### Human-readable

The base syntax has to be line-oriented, to reflect the event
structure.

Other than that, I propose to use a modified turtle syntax for
roles.

So we can write

%realm: realm url

%perspective: perspective\_id

\<event\_id\> \<timestamp\> \<user\_id\> \<event\_type\>
....

for each line. The base event type is *stmt* (statement)

(Other base event types: *declare* topicId, *rename* sourceTopicId
destTopicId)

Each statement would follow the syntax of a single turtle clause ending
in a period, but could contain many commas and semicolons.

Predicates with roles will be written thus:

\<subject\_id\>
**\<**role\_name1**:**predicate\_name**:**role\_name2**\>**
\<object\_id\>

Though, as in turtle, the special case where role\_name\_1 is subject
and role\_name\_2 is object can be shortened to
**\<**predicate\_name**\>** (and maybe also have a \<-predicate\> syntax
for reverses?)

Actually, \<role\_name: and :role\_name\> are syntaxic units that can
be used in path queries, corresponding to incoming and outgoing links
respectively.

For example, given atoms X, Y, Z, W

where X plays role1 in relation Y

and Y plays role2 in relation Z

and W plays role3 in relation Z,

It is possible to navigate from X to W thus

    subject \<role1: \[predicate1 constraint\] \<role2: \[predicate2 constraint\] :role3\>

TODO : create a syntax for recurring path elements and query
variables.

From this, it is possible to write strange-looking turtle such
as

    subject \<role1:predicate1:role2\> object1 , object2 ; :role3\> object3 ; predicate2:role4\> object4 ; \<role5:predicate5:role6\> object5.



(Note: maybe worth breaking up into separate events, if only to give
them distinct identity. But that should be optional as long as they have
identity otherwise. OR we could enumerate statements within an event and
use the pair \<event\_id, stmt\_num\> as an identifier.
Sigh.)



This would be a shortcut to the following statement family:

    subject \<role1:predicate1:role2\> object1.
    
    subject \<role1:predicate1:role2\> object2.
    
    subject \<role1:predicate1:role3\> object3.
    
    subject \<role1:predicate2:role4\> object4.
    
    subject \<role5:predicate5:role6\> object5.

##### Json event format

This is a json-encoded event sequence:

```json
[
  {
    "@id": "e10",
    "@type": "stmtEvt",
    "when": "20190101T01010100000Z",
    "who": "u1",
    "stmt": {
      "@id": "stmt10",  // use event Id if undefined
      "@type": "someRelation",  // data on signature
      "role1": "t1", // role1 may be "subject"
      "role2": "t2", // role2 may be "object"
      "role3": "t3", // may be stored in a qualifier in wikidata
      "role4": {
          "@value": "some literal",
          "@type": "...", // rdf literal types or mime types?
          "precision": "...", // a la wikidata: unit, precision...
          "@language": "....",
      },
      "vs": ["stmt2", "stmt3"]
    }    
  }
]
```

##### JSON snapshot format

We want to keep statement identity, so basically just an indexed
statement sequence:

```json
{
    "subject_id": {
        "role1": [
            {
      "eid": "event_id",
      "@id": "statement_id", // if different from eid
              // rest of statement content as above
},
...
        ],
        "role2": {
            "@type": "RestEndpoint",  // there may be other endpoint types for DWeb
            "@value": "give an endpoint for a very long statement search"
        },
        ...
    }
}
```

##### Search syntax

Now that we have a path syntax, it might be as simple as

    \<identifier\> \<path\_re\> \<identifier\>

where identifier is a topic\_id or a variable or a full topicRef
(todo)

path\_re is made of path elements: any (\*), role names, identifiers,
search subconstraints in brackets like in xpath (including type
constraints), or sequences {path\_re}\<quantifier\> with the usual
regexp quantifiers ?,+,\*, \[1..6\] etc.

So eg the immediate neighbourhood of a node is always \<node-id\>
\<\*:\*:\*\>

#### ontology considerations

Roles have an inherent "stickiness": when getting the snapshot of a
node, we want to get all statements connected through a sticky role.
(Otherwise give a query endpoint.) We could have mechanisms whereby the
stickiness extends to another actor in the sticky relation. (Or do we
always do this with search specs in the legend?)



Another factor: suppose we have this

    S1: John <giver:P1[gives]:object> book
    S2: John <giver:P1[gives]:to> Jane



it would be a mistake to encode as those two triples:

    John giver_gives_object book
    John giver_gives_to Jane

because those may be two giving moments.

We can use the core named statement

    S1: John giver_gives_object book
    S2 : S1 :+to Jane

The `+` here distinguishes a role that extends S1 vs a role that applies
to S1 as a subject. (We may have to create extension variants of roles
in a named-triple storage.)


### Earlier glossary MAP:

Topic: everything's a topic (i.e. can be referenced, has an identifier)
(close to RDF:Resource, wider than Graph:Node...)

Statement: A topic that makes an assertion about another topic (think
\~wikidata)

The "same" topic can be described differently by different people. We
want to distinguish "Topic names", URNs that describe the topic
"abstractly" and are meant to be shared by different servers; vs "topic
locators", that designate the specific set of assumptions about a
concrete topic, held by one principal (or group) in one data center
according to one perspective.









There are many ways to distinguish (and group) various such views about
a topic:

1.  Server (aka process): Data center in charge of building views from
    > events

2.  Naming authority (aka Realm): creates local names for topics in the
    > context of one data center.

3.  Perspective: a chain of statement events about a set of
    > topics. compare to a git branch. Under one's
    > principal's responsibility. (naming scheme of branches tbd, but
    > it's ultimately a naming authority + local name.)

4.  Snapshot: How was a given topic considered in one perspective at a
    > given moment. (can be designated either by timestamp or opaque
    > event\_id. the union will called a time\_id)

Note that creating a new branch does not automatically change the local
name of a resource, but will change the full locator of the
topic.

So the full locator of a topic must allow to identify Naming authority,
Local name, and optionally Perspective identifier and
time\_id.

It must be possible to go from a naming authority to the
Process.

Encompass: A topic statements to cover what is covered by another
topic, whether owned or a global name, such as a RDF
identity.

ImmutableResource: an immutable document (may be media, json, etc.) Has
a signature.

(Note on meaningful vs opaque names: any meaningful names becomes a
resource, and generates conflict. For that reason, I suggest realm names
should be opaque, such as uuid.)

### Serverless Operations:

in process of renaming.

Diagram:



https://gitlab.com/hyperknowledge/hyperknowledge/raw/master/main\_diagram2.png

https://gitlab.com/hyperknowledge/hyperknowledge/raw/master/main\_diagram2.svg

media/image6.png

(Some of those operations can be made serverless with distributed hash
tables.)

#### unpackLocator(Topic locator) -\> (realm, local name, perspective?, time\_id?)

I can take a global URI of an owned topic and somehow
obtain:

1.  its realm

2.  a server that can answer questions about it

    a.  Event queue

    b.  ...

3.  its canonical local name within that realm

it could be in structure of URI:

eg hk://\<realm urn\>/\<topic local name\>/\<perspective local
name\>/\<time\_id\>

BUT global URI could be a URN or IPNS name, did, etc. and I'd have to
ask the dweb to get me parts above.

#### findServer(Realm)-\>Server

There must be a (distributed?) service as part of the protocol to
obtain servers for a given realm.

#### localize(Topic name, Realm)=\>local name

find a local name in a realm

#### findConcreteTopics(Topic global identifier) -\> (Topic locator)\*

I can take any URI that refer to any topic and access distributed data
(without a realm or server) that will give me a possibly non-exhaustive
list of concrete topics that statement to encompass that abstract topic.
(i.e. you get a starting point.)

#### getCoreView(realm, local name, perspective, time\_id?)-\>base information (immutable base)

When I ask the protocol about info about a topic, I will get some
of:

a.  immutable snapshots of jsonld statement bundles (esp. about core statements)
    i.  known global names, or topic locators in other realms (especially of any perspective from which events were merged)
    ii. Whether this topic is further encompassed in another (local or foreign) proxy
b.  The endpoint to get more events (and subscribe)
c.  an endpoint to get some non-core statements (may be a slow call, with callbacks...)


### Server-based Operations:

#### perspectivesOf(realm, local name)-\>perspectives

in which perspectives is this topic mentioned?

#### subscribe(realm, perspective, search\_description?)-\>event subscription

Subscribe to the events of a given perspective. We can have a query
that restricts the events that will reach us. The simplest query will be
a list of topic local names and a "radius." (W/o search description, it
can probably be done serverless)

#### getView(realm, local name, perspective, info\_spec, time\_id?)-\>current statement view

When I ask a process about info about a topic, I will get some
of:

d.  immutable snapshots of jsonld statement bundles (esp. about core statements)
e.  Some more (non-core) statements
f.  Some events
g.  The endpoint to get more events (and subscribe)
h.  an endpoint to get some non-core statements (may be a slow call, with callbacks...)

Info spec is like graphql query specification: How much information do
you want?

Other servers may convert statement views to other views (eg
html)

#### submitPerspective(realm, source\_perspective, dest\_perspective, subset\_spec?)=\>Pull request

Create a pull request so events from the source perspective are
integrated into the dest perspective. May fail due to consistency rules
of the dest perspective, or permissions on the dest
perspective.

Note that we may want to merge only a subset of events regarding a
subset of topics.

On success, returns a merge event.

#### findTopicsInRealm(Realm, Topic global identifier) -\> (Topic locator)\*

I can take any URI that refer to an abstract topic and access an
exhaustive list of concrete topics that statement to encompass that
abstract topic within that realm.

#### findTopicsInPerspective(Perspective, Topic global identifier) -\> (Topic locator)\*

I can take any URI that refer to an abstract topic and access an
exhaustive list of concrete topics that statement to encompass that
abstract topic within that perspective. In a well-formed perspective,
there should be only one, but that may not be enforced at system
level.


### Graph Event definition:

Base graph events add or remove statements about a topic. If you want
to reassert a statement, make a new one with a different ID; that way
you have a CRDT structure. Events use local topic names. Events will
refer to previous events and form a lattice structure. Non-base events
get translated to base events. (question: previous events or dependent
events? both?)

Events list a set of topics they are relevant to, for subscription
purposes. That way, if we subscribe to a topic, we will also get events
related to a change in its description document (which will also be a
topic), the relations it gets in, etc.

Because merge can be partial, a merge will need to explicitly name (or
duplicate?) events from the source branch. (This is especially true if
the event comes from another realm, thus topic references need to be
changed.)

Question: How to do rebase with immutable events? Basically a rebase is
a new branch.

Branch name may be as simple as an IPNS reference to an event ID. BUT
we want to subject this operation to some access control. Otherwise, a
single event gives you the whole knowledge graph! (Not with encrypted
events, so we could still go that route.)

Hierarchical event transactionality to allow consistency checking after
top-level event.

We have to make sure that a merge operation, which merges only a subset
of events from a PR (defined by neighbourhoods) will also yield a
consistent state.

### View definition

When we query a view, we want at least

1.  core attributes and relations of the topic
    a.  Local identifier in this realm
        b.  direct types
        c.  Current perspective identifier
        d.  Underlying literal value if a small resource
        e.  identifier of last event to affect the core
        f.  Pointer to last core snapshot (content-addressable hash?)
            g.  All merged topics
        h.  next merging topic
        i.  topics that were split off
        j.  Known global names
        k.  topic locator of merged topics
        l.  Origin topic locator of this topic (if transcluded from other realm)
            m.  Any attribute used in a legend (or only perspective-local legends?)
        2.  In-perspective relations to topic (content or references+endpoint?)
    > but out
    3.  In-perspective non-core attributes (content)
    4.  id of last event to affect this topic (incl. non-core changes so outside of
    5.  top merging topic (outside of core because recursive query)
    6.  endpoints for queries / optional info
    n.  The event stream of the current perspective
        o.  All known realms hosting refs to this topic (may be slow)
            p.  All referring attributes relations (incl. out-of-realm)
            q.  All including perspectives in this Realm (core?) (all including or only
            r.  Underlying (large) resource, if topic is metadata for a resource

### Statement importation

(By statement, I mean all attribute and role bindings on a given topic;
they statement that this attribute holds, or that the topic participates
in that relation. This is inspired by wikipedia, where everything is
stored as quads made of a statement id and a RDF triple.)

When I (realm S) get a statement from some realm R, saying \<global
name gA\> owl:sameAs \<global name gB\>\...

I have many options:

I may localize that statement to its original targets locR(gA) sameAs
locR(gB)

OR to my localization of these local targets: locS(locR(gA)) sameAs
locS(locR(gB))

OR to my localization of the abstract concept: locS(gA) sameAs
locS(gB)



If the original statement was \<local name lA\> sameAs \<localName
lB\>

I may keep that statement to its original targets lA sameAs
lB

OR to my localization of these local targets: locS(lA)) sameAs
locS(lB)

OR to my localization of the abstract concept: locS(lA) sameAs
locS(lB)



(I'm using sameAs as an example where the predicate is global and
uncontroversial, but in some cases the same logic applies to the
predicate.)



Any of those translations can just be something my realm is aware of vs
has accepted into the definitions of lA, lB. Statements have
a provenance, and an acceptance value within the current
realm:

1.  to be processed
2.  known
3.  accepted
4.  rejected
5.  superseded (by another named statement)

Is that true of events? (probably)

(Note every topic states its provenance, esp. any realm/perspective it
got events from)

#### Discoverability

Following a discussion with Gyuri, he was proposing that global names
(and identity-defining relations) form a hashable signature, to which we
could attach snapshots. I'm afraid of conflicts around this, but we
could have a shared

MerkleMap\<global\_signature, Map\<RealmId,
IPNS\<Snapshot\>\>

(The map might be implemented as a AppendList\<Pair\<RealmId,
IPNS\<Snapshot\>\>\> ; that way both the merkle map and the AppendList
are CRDT, and nobody would fight over the proper
definition.)


### Security and protocols

We want different forms of access control between peers.



1.  event queue access: global access control for a queue

2.  column access: you may want to give a partial views of all objects.
    > If defined in advance, you can have encrypted sections in your
    > object descriptions; or encrypted events.

3.  row access for security purpose. Again, we can encrypt different
    > topics against different keys, but for that we need to pre-define
    > our security perimeter, which is a drag.



Related but distinct consideration: a small personal server can get
overwhelmed with all events from a large federation server. We need to
be able to subscribe to events on a subset of topics to avoid that. This
cannot be done with a gossip protocol like scuttlebutt. That means we
can use scuttlebutt as a low-level protocol between peers, but not
necessarily between all server pairs.





We need a good UI to make this understandable. We should be able to
give a default security level to different fields/relations of topics. A
specific topic could have a separate set of security levels for fields
or the topic as a whole.

One non-obvious consequence: if we say one topic encompasses an
external topic with its own ACL, that topic may not be
accessible.

#### First Draft

Ok, here's a first draft technical proposal:

1.  Distinguish public (like scuttlebutt) and private channels.
    > Messages on public channels must be encrypted (unless
    > public)

2.  Distinguish physical from virtual channels. Eg. many branches'
    > events may travel on one physical channel. (In scuttlebutt, we may
    > also tie multiple physical channels to an identity)

3.  When I need to give a partial view of data for a client, try to
    > decompose into units (eg a group of topics, or a field type). Put
    > each unit on a virtual channel, with its own encryption key. Give
    > those keys to the client. Copy events from the main channel to
    > those virtual channels, encrypted against that virtual channel's
    > key. The next client may need different units; but with
    > well-chosen units, it should be possible to reuse virtual
    > channels, so the number of virtual channels should grow much less
    > than the number of access combinations. (which should also be
    > fairly limited.) Any given datum should only have to be copied on
    > one or two virtual channels. So there will be duplication, but
    > hopefully handleable.



## Glossary

Assertion
:   Assertions are pieces of data in the hyperknowledge graph: stating that a concept exists, is involved in a given association, has a certain feature with a certain value… Most assertions will have a provenance, and exist in a certain knowledge space.

Relation
:   Relations are concepts that link other concepts. They can be accounts of relationships (causality, containment, typing…) or events. Each concept involved in the association has a Role in the association. The roles are determined by the association’s type(s). Relations are N-ary in general, but many are directed binary relations.

Concept
:   Almost everything in the hyper-knowledge graph is a concept. Synonymous in intent with `owl:Thing` or topic (Topic maps) (though those concepts are not strictly synonymous with one another.) All concepts are also `rdf:Resources`, but the reverse is not true. A concept has features (aka properties) and is involved in associations. It lives in multiple knowledge spaces.

Document
:   Resources which correspond to a document, of any mimetype. Ideally, there should be a way to retrieve the document on a given endpoint (including P2P content addressing), but some documents are physical. The document’s metadata has a representation as a concept, distinct from the document data. The combined document and metadata can be obtained as Augmented HTML. The document may be a representation of a concept, which is distinct.

Graph
:   

Hyperknowledge
:   

Hyperknowledge Graph
:   

Identity
:   

Knowledge
:   

Knowledge Graph
:   

Knowledge Service
:   A service able to communicate with other knowledge services using the hyperknowledge formats, using one of the many (client-server or P2P) protocols. Some services are originators of concepts (Knowledge sources), other are transformers (and may add information to existing concepts), others still handle merge operations (federation servers.)

Merge
:   

Nexus
:   

Provenance
:   

Raw Concept
:   

Viewspec
:


