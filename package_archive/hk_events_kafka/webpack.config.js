var path = require('path');
const FlowWebpackPlugin = require('flow-webpack-plugin')
const nodeExternals = require('webpack-node-externals');
const babel = require("@babel/core")
const MinifyPlugin = require("babel-minify-webpack-plugin");

const env = process.env.NODE_ENV

module.exports = {
  entry: [
    './src/kafkanode.js',
  ],
  output: {
    filename: 'index.js',
    libraryTarget: 'umd',
    library: 'hyperknowledgeEventsKafka',
    publicPath: '/',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',
  plugins: [
    new FlowWebpackPlugin(),
    // new MinifyPlugin({ removeUndefined: false, mangle: false }, {babel: babel, sourceMap: false}),
  ],
  target: 'node',
  mode: env || 'development',
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      }
    ]
  }
}
