// @flow
import {level, type LevelT, type levelValue, type BatchT} from "level"

import type {
    localId, eventId, kName, URLType, kRealmId, eventLocator, PresenceType
} from "./types";
import { presenceTypes } from "./types";
import {
    Event, IRealm, IPerspective, IQueue, ITopicEventQueue, TopicRef, StoredEvent, FrozenQueueRef
} from "./base";

const maxDate = new Date(8640000000000000);
const minDate = new Date(-8640000000000000);


function levelPromise<K, V>(name, options): Promise<LevelT<K, V>> {
  return new Promise<LevelT<K, V>>((resolve, error) => {
    level(name, options, (err, db: LevelT<K, V>) => {
      if (err) {
        error(err)
      } else {
        resolve(db)
      }
    })
  })
}


export class LevelRealm implements IRealm {

    url: URLType;
    eventsDb: ?LevelT<[localId, Date], string>;  // (queue_id, date) -> event
    PTIndex: ?LevelT<[localId, localId, Date], [?Date, PresenceType]>;  // (queue_id, topic_id, date)-> (previous date, presenceType)
    TPIndex: ?LevelT<[localId, localId, Date], [PresenceType, boolean]>;  // (topic_id, queue_id, date)-> (presenceType, incl/excl)
    mergeIndex: ?LevelT<[localId, Date], [localId, Date]>;  // (target_queue_id, date) -> (merged_queue_id, until_date)
    nameIndex: ?LevelT<[localId, string, Date], [localId, Date]>;  // (queue_id, name, date) -> (local_id, prev date)
    localIndex: ?LevelT<Array<any>, [localId, Date]>
    globalIndex: ?LevelT<Array<any>, [localId, Date]>
    lastId: number = 0

    constructor(url: URLType) {
        this.url = url;
    }

    createLocalId(): localId {
        this.lastId++;
        return this.lastId.toString()
    }

    async init(): Promise<void> {
        return new Promise((resolve, err) => {
            // create indices
            
            return Promise.all([
                levelPromise<[localId, Date], string>("var/events", {valueEncoding: 'binary'}),
                levelPromise<[localId, localId, Date], [?Date, PresenceType]>("var/TPidx"),
                levelPromise<[localId, localId, Date], [PresenceType, boolean]>("var/PTidx"),
                levelPromise<[localId, Date], [localId, Date]>("var/mergesIdx"),
                levelPromise<[localId, string, Date], [localId, Date]>("var/namesIdx"),
                levelPromise<Array<any>, [localId, Date]>("var/localIndex"),
                levelPromise<Array<any>, [localId, Date]>("var/globalIndex")
            ]).then(([eventsDb, PTIndex, TPIndex, mergeIndex, nameIndex, localIndex, globalIndex])=>{
                this.eventsDb = eventsDb;
                this.PTIndex = PTIndex;
                this.TPIndex = TPIndex;
                this.mergeIndex = mergeIndex;
                this.nameIndex = nameIndex;
                this.localIndex = localIndex;
                this.globalIndex = globalIndex;
                // Find highest topic identifier used, and initialize lastId
                this.globalIndex.createKeyStream({lt: [StatementEvent.typeName+"\0x00"], reverse: true, limit: 1}).on('data', (key: [localId, localId, Date])=>{
                    this.lastId = Number.parseInt(key[0]) + 1
                    resolve()
                }).on('error', (error)=>{
                    err(error)
                }).on('close', ()=>{
                    resolve()
                }).on('end', ()=>{
                    resolve()
                });
            });
        });
    }

    findGlobalName(name: URLType, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void> {
        throw new Error("Not implemented");
        // look in nameIndex
    }

    textSearch(text: string, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void> {
        throw new Error("Not implemented");
        // Create a full text search index?
    }

    findPerspectivesWithTopic(id: localId, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void> {
        throw new Error("Not implemented");
        // probably TPIndex
    }

    getPerspective(id: URLType | localId): Promise<?IPerspective> {
        throw new Error("Not implemented");
        // check if in PTIndex, and create LevelPerspective
    }

    getAllPerspectives(): AsyncGenerator<IPerspective, void, void> {
        throw new Error("Not implemented");
        // not sure. Go through PTIndex, jump through dates?
        // Look for Perspective type in PTIndex?
    }

    createPerspective(spec: string): Promise<IPerspective> {
        throw new Error("Not implemented");
        // have a perspective creation event, add it to topic index
    }


    async lastTopicTime(queue_id: localId, topic_id: localId): Promise<?Date> {
        const ptindex = this.PTIndex;
        if (!ptindex) throw new Error("not initialized")
        return new Promise((resolve, error)=>{
            ptindex.createKeyStream({
                lt: [queue_id, topic_id+"_", minDate], reverse: true, limit: 1
            }).on('data', (key: [localId, localId, Date])=>{
                if (key[0] == queue_id && key[1] == topic_id) {
                    resolve(key[2])
                } else {
                    resolve(null)
                }
            }).on('error', (err)=>{
                error(err)
            }).on('close', ()=>{
                resolve(null)
            }).on('end', ()=>{
                resolve(null)
            })
        })
    }

    async queue_append(queue_id: localId, eventPayload: Event): Promise<StoredEvent> {
        const eventsDb = this.eventsDb
        const PTIndex = this.PTIndex
        const TPIndex = this.TPIndex
        if (!(eventsDb && PTIndex && TPIndex))
            throw new Error("not initialized")
        const now = new Date()
        const event = new StoredEvent(eventPayload, now)
        const promises: Array<Promise<void>> = []
        // append the event to events
        var promise: ?Promise<void> = eventsDb.put([queue_id, now], JSON.stringify(event));
        if (promise)
            promises.push(promise)
        // get topics from events
        const topics = eventPayload.topics
        // for each topic, update PTIndex/TPIndex
        if (topics.length) {
            const lastTimes = topics.map((topic_id)=>this.lastTopicTime(queue_id, topic_id))
            const PTbatch = ((PTIndex.batch(): any): BatchT<[localId, localId, Date], [?Date, PresenceType]>)
            const TPbatch = ((TPIndex.batch(): any): BatchT<[localId, localId, Date], [PresenceType, boolean]>)
            Promise.all(lastTimes).then((times: Array<?Date>)=>{
                for (var i = 0; i < topics.length; i++) {
                    const topic_id = topics[i]
                    const lastTime = times[i]
                    // TODO: presence type
                    PTbatch.put([queue_id, topic_id, now], [lastTime, presenceTypes.modified])
                    // TODO: exclusion
                    TPbatch.put([topic_id, queue_id, now], [presenceTypes.modified, true])
                }
                promise = PTbatch.write()
                if (promise)
                    promises.push(promise)
                promise = TPbatch.write()
                if (promise)
                    promises.push(promise)
            })
        }
        // TODO:
        // if a naming event, update namingIndex
        // if a merge event, update mergeIndex
        return new Promise((r, e)=>{
            Promise.all(promises).then((x)=>{
                r(event);
            }).catch((err) => {
                e(err)
            })
        })
    }

    async queue_event(queue_id: localId, id: eventLocator): Promise<?StoredEvent> {
        const eventsDb = this.eventsDb
        if (!eventsDb)
            throw new Error("not initialized")
        // note: what if the event is from a merged queue?
        const eventRep = await eventsDb.get([queue_id, id])
        if (eventRep && eventRep.length) {
            const event = await StoredEvent.fromJSON(eventRep)
            return event
        }
    }

    async *queue_events(queue_id: localId, since: ?eventLocator, until: ?eventLocator): AsyncGenerator<StoredEvent, void, void> {
        throw new Error("Not implemented");
        // look in eventsDb
    }

    async *queue_eventsForTopic(queue_id: localId,
        topic: localId,
        until: ?eventLocator,
        since: ?eventLocator,
        chronological: boolean
    ): AsyncGenerator<StoredEvent, void, void> {
        throw new Error("Not implemented");
        // look in PTIndex
    }

    async *queue_includedQueues(queue_id: localId, until: ?eventLocator): AsyncGenerator<FrozenQueueRef, void, void> {
        throw new Error("Not implemented");
        // look in mergeIndex
    }
    async *knownRealms(): AsyncGenerator<IRealm, void, void> {
        throw new Error("Not implemented");
        // when a realm forks a perspective from another realm, it should ping back and this should be recorded
    }
}


export class LevelQueue implements ITopicEventQueue {
    realm: LevelRealm;
    id: localId;
    
    constructor(realm: LevelRealm, id: localId) {
        this.realm = realm;
        this.id = id;
    }
    
    async append(eventPayload: Object): Promise<StoredEvent> {
        return this.realm.queue_append(this.id, eventPayload);
    }

    async event(id: eventLocator): Promise<?StoredEvent> {
        return this.realm.queue_event(this.id, id);
    }

    async *events(until: ?eventLocator, since: ?eventLocator): AsyncGenerator<StoredEvent, void, void> {
        yield *this.realm.queue_events(this.id, until, since)
    }

    async *eventsForTopic(topic: localId,
        until: ?eventLocator,
        since: ?eventLocator,
        chronological: boolean
    ): AsyncGenerator<StoredEvent, void, void> {
        yield *this.realm.queue_eventsForTopic(this.id, topic, until, since, chronological)
    }

    async *eventsForTopics(topics: localId[],
        until: ?eventLocator,
        since: ?eventLocator, 
        exceptTopics: ?localId[],
        chronological: boolean
    ): AsyncGenerator<StoredEvent, void, void> {
        // merge multiple generators with queue_eventsForTopic
        const values = {}
        const iters = {}
        var done = false
        var lastVal = {}
        for (const topic of topics) {
          const iter = this.eventsForTopic(topic, until, since, chronological)
          iters[topic] = iter
          values[topic] = await iter.next()
          // check exceptTopics, keep iterating
        }
        while (!done) {
          var extrKey = undefined
          var extrTs = undefined
          done = true
          for (const topic of topics) {
            const val = values[topic]
            if (!val.done) {
              const ts = val.value.obj.ts
              if (extrTs == undefined || ((chronological) ? (extrTs < ts) : (extrTs > ts))) {
                extrTs = ts
                extrKey = topic
              }
            }
          }
          if (extrKey) {
            const val = values[extrKey].value
            if (val.cid != lastVal.cid) {
              yield values[extrKey].value
              lastVal = val
            }
            values[extrKey] = await iters[extrKey].next()
            // check exceptTopics, keep iterating
            done = false
          }
        }
    }
    async *includedQueues(until: ?eventLocator): AsyncGenerator<FrozenQueueRef, void, void> {
        yield *this.realm.queue_includedQueues(this.id, until)
    }
}
