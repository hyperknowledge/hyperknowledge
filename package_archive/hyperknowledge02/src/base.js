// @flow
import type { localId, eventId, kName, URLType, kRealmId, eventLocator, PresenceType } from "./types";


export class TopicRef {
    invalid: boolean
    topicId: localId
    perspectiveId: URLType
    _perspective: ?IPerspective

    constructor(perspective: IPerspective | URLType, id: localId) {
        if (typeof perspective == "string") {
            this.perspectiveId = perspective
        } else {
            this._perspective = ((perspective: any): IPerspective)
            this.perspectiveId = this._perspective.url
        }
    }

    async perspective() : Promise<IPerspective> {
        if (this.invalid)
            throw Error(this.perspectiveId)
        if (this._perspective) {
            return this._perspective
        }
        // ask the registry
        const perspective = await registry.getPerspective(this.perspectiveId)
        if (perspective) {
            this._perspective = perspective
            return perspective
        } else {
            this.invalid = true
            throw Error(this.perspectiveId)
        }
    }

    async realm() : Promise<IRealm> {
        const perspective = await this.perspective()
        return perspective.realm
    }

    get url(): URLType {
        return this.perspectiveId + "/" + this.topicId
    }
}

export class FrozenTopicRef extends TopicRef {
    _eventId: ?eventId
    _time: ?Date
    constructor(perspective: IPerspective | URLType, id: localId, when: eventLocator) {
        super(perspective, id)
        if (when instanceof Date) {
            this._time = when
        } else {
            this._eventId = when
        }
    }

    async eventId(): Promise<eventId> {
        if (this.invalid)
            throw new Error()
        const eid = this._eventId
        if (eid) {
            return eid
        }
        const time = this._time
        if (time) {
            const perspective: IPerspective = await this.perspective()
            const event: ?StoredEvent = await perspective.event(time)
            if (event) {
                this._eventId = event.id
                return event.id
            }
        }
        this.invalid = true
        throw new Error()
    }
    get url(): URLType {
        if (this.invalid)
            throw new Error()
        const eid = this._eventId
        if (eid)
            return super.url + "/e" + eid
        const time = this._time
        if (time)
            return super.url + "/" + time.toISOString()
        this.invalid = true
        throw new Error()
    }
}


//type IRealmFactoryFn = (url: URLType) => Promise<?Realm>;


class Registry {
    /* Transform URLs and names into objects
    */
    perspectiveFactories: IPerspectiveFactoryFn[];
    knownPerspectives: Map<URLType, IPerspective>;
    constructor() {
        this.perspectiveFactories = [];
        this.knownPerspectives = new Map();
    }
    perspectiveFactories: IPerspectiveFactoryFn[] = [];
    async getPerspective(url: URLType): Promise<?IPerspective> {
        const perspective = this.knownPerspectives.get(url)
        if (perspective)
            return perspective
        for (const fn of this.perspectiveFactories) {
            const perspective: ?IPerspective = await fn(url)
            if (perspective) {
                this.knownPerspectives.set(url, perspective)
                return perspective;
            }
        }
    }
    registerFactory(fn: IPerspectiveFactoryFn) {
        this.perspectiveFactories.push(fn)
    }
    static isWord(s: string): boolean {
        const match = s.match(/\w+/)
        return match !== null && match[0].length == s.length
    }
    getTopicRef(url: URLType): ?TopicRef {
        // 3 cases: url/id , url/id/id, url/id/date
        // hard to distinguish first two
        const fragments: string[] = url.split('/')
        if (fragments.length < 2)
            return null
        const last = fragments[fragments.length - 1]
        const partial1 = fragments.slice(0, fragments.length - 1).join('/')
        const perspective1 = this.knownPerspectives.get(partial1)
        if (fragments.length > 3) {
            const last2 = fragments[fragments.length - 2]
            const partial2 = fragments.slice(0, fragments.length - 2).join('/')
            const perspective2 = this.knownPerspectives.get(partial2)
            if (!Number.isNaN(Number(last))) {
                const asDate = new Date(last)
                if (asDate.getTime() !== undefined) {
                    return new FrozenTopicRef(perspective2 || partial2, last2, asDate)
                }
            }
            if (!Registry.isWord(last)) return null
            if (perspective2 && Registry.isWord(last2)) {
                return new FrozenTopicRef(perspective2, last2, last)
            } else if (perspective1) {
                return new TopicRef(perspective1, last)
            } else {
                // the hard case... unknown perspective, guess if many Ids.
                // heuristic based on number of elements
                if (fragments[1].length == 0 && fragments.length > 3 && Registry.isWord(last2)) {
                    return new FrozenTopicRef(partial2, last2, last)
                } else {
                    return new TopicRef(partial1, last)
                }
            }
        } else if (Registry.isWord(last)) {
            return new TopicRef(perspective1 || partial1, last)
        }
    }

    async getValidatedTopicRef(url: URLType): Promise<?TopicRef> {
        const topicRef = this.getTopicRef(url)
        if (topicRef) {
            const perspective = await topicRef.perspective()
            if (perspective && !topicRef.invalid)
                return topicRef
        }
    }
}

export const registry = new Registry();

export interface IRealm {
    url: URLType;

    // search should limit for a presence type. Also include or not shadow perspectives?
    findGlobalName(name: URLType, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void>;
    textSearch(text: string, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void>;
    findPerspectivesWithTopic(id: localId, presence: ?PresenceType): AsyncGenerator<TopicRef, void, void>;

    knownRealms(): AsyncGenerator<IRealm, void, void>;

    getPerspective(id: URLType | localId): Promise<?IPerspective>;  // look by global name???
    getAllPerspectives(): AsyncGenerator<IPerspective, void, void>;
    createPerspective(spec: string): Promise<IPerspective>;
    createLocalId(): localId;

    // TODO: Realm ownership and permissions
}

export class EventIndexInfo {
    type: localId;
    global: ?boolean;
    data: localId[];
    constructor(type: localId, data: localId[], global: ?boolean) {
        this.type = type;
        this.global = global;
        this.data = data;
    }
}


export class Event { // aka payload
    static types: Map<string, Class<Event>> = new Map();
    static typeName: string = "abstract event";
    who: TopicRef;
    constructor(who: TopicRef) {
        this.who = who;
    }
    get indexing(): EventIndexInfo[] {
        // return a list of lists of index values.
        // Each index is a list, and must start with queue id (or null if global index), then sub-index name, then values
        return []
    }
    static fromJSON(evS: Object) {
        throw new Error("Abstract")
    }
    static registerEventType(evCl: Class<Event>) {
        const typeS: string = evCl.typeName
        if (!typeS)
            throw new Error("registering an event class w/o typeName")
        Event.types.set(typeS, evCl)
    }
}
// subclasses: NewPerspective, Merge, StatementEvent

export class MergeEvent extends Event {
    targetPerspective: TopicRef;
    until: eventLocator;
    static typeName: string = "merge";
    constructor(who: TopicRef, targetPerspective: TopicRef, until: eventLocator;) {
        super(who)
        this.targetPerspective = targetPerspective;
        this.until = until;
    }
    get indexing(): EventIndexInfo[] {
        return [
            new EventIndexInfo(MergeEvent.typeName, [this.targetPerspective.url])
        ]
    }
}

Event.registerEventType(MergeEvent);

export class StoredEvent {
    ev: Event;
    when: Date;
    constructor(ev: Event, when: ?Date) {
        this.ev = ev
        this.when = when || new Date()
    }
    get id(): eventId {
        // default implementation
        return this.when.toISOString();
    }
    static fromJSON(dataS: string): ?StoredEvent {
        const data = JSON.parse(dataS)
        const whenS = data.when
        if (!whenS) return null
        const when = new Date(whenS)
        const evS = data.ev
        if (!evS) return null
        const typeS = evS['@type']
        if (!typeS) return null
        const type = Event.types.get(typeS)
        if (!type) return null
        const ev = type.fromJSON(evS)
        if (!ev) return null
        return new StoredEvent(ev, when)
    }
    // maybe an accessor that extracts the creator from the payload?
    // maybe a ref to the perspective? or at least queue? optional or not?
    // Note: queue endpoint does not give me perspective, because I also need realm endpoints.
    // this is really an event storage wrapper, not an event... all in the payload.
}



export interface IQueue {
    append(payload: Event): Promise<StoredEvent>;
    event(id: eventLocator): Promise<?StoredEvent>;
    events(
        until: ?eventLocator,
        since: ?eventLocator,
        chronological: boolean): AsyncGenerator<StoredEvent, void, void>;
}

// what's important is the possibility to define a Queue slice. Topics are one slice axis.
// Event types are another.
// A smart queue can optimize some slice algorithms, such as topics.
// So SliceableQueue?
// Do we allow arbitrary slice functions?
// Do we allow the slicing generator to change the slice definition midway? (We decided against this)
// Another qn. is mergeability above or below sliceability? It seems they're traits at the same level.
// I can do slicing badly on merge, or merge less badly on slice.
// Ok. 1st: slices have to be unambiguous and immutable. So I cannot say "events up to time T on `my topics`" without
// fixing the topic set at the same time. If the topic set changes, it has to be part of the merge operation.
// Time and topic set has to increase monotonically, I think. I cannot "forget" a topic. (Is that so?
// Basically it would allow foreign events to come in and out of scope. We could do that explicitly, I guess.
// Maybe make the notion of fixed point explicit? It is explicit in other good systems.
// A fixed point is a more useful concept than a transaction in a CRDT context!
// And it allows tentative endless computations: Can you reach a fixpoint in X amount of gas?



//
//note on indices: the index name is part of index structure, i guess.
//
export interface ITopicEventQueue extends IQueue {
    eventsForTopic(topic: localId,
        until: ?eventLocator,
        since: ?eventLocator,
        chronological: boolean
    ): AsyncGenerator<StoredEvent, void, void>;

    eventsForTopics(topics: localId[],
        until: ?eventLocator,
        since: ?eventLocator, 
        exceptTopics: ?localId[],
        chronological: boolean
    ): AsyncGenerator<StoredEvent, void, void>;
}

class SliceDefinition {
    filter(ev: Event): boolean {
        // abstract
        return false;
    }


}

class StatementSliceDefinition {
    // based on topics index
}

class MergeSliceDefinition {
    // based on merge index
}

class NamingSliceDefinition {
    // based on topics index
}

class ExtensionSliceDefinition {
    base: SliceDefinition;
    except: SliceDefinition;
    filter(ev: Event): boolean {
        return this.base.filter(ev) && !this.except.filter(ev);
    }
}

// class mixin factory
function makeQueueMergeAware(queueClass: Class<ITopicEventQueue>): ITopicEventQueue {
    return class MergeAwareQueue extends queueClass {
        includedQueues(until: ?eventLocator): AsyncGenerator<FrozenQueueRef, void, void> {
            // TODO
        }
        eventsForTopic(topic: localId,
            until: ?eventLocator,
            since: ?eventLocator,
            chronological: boolean,
            includeMerges: boolean
        ): AsyncGenerator<StoredEvent, void, void> {
            if (!includeMerges) {
                yield* super.eventsForTopic(until, since, chronological)
            } else {
                // make sure to include merges.. how
            }
        }
    }    
}

export class FrozenQueueRef {
    queue: ITopicEventQueue;
    when: Date;
}


// next layer

export interface IPerspective extends ITopicEventQueue {
    // or owns a queue? note that the merge algo would belong here.
    // is Perspective URL under the Realm URL? Maybe...
    // Does it lead to a perspective description? What goes there?
    
    realm: IRealm;
    url: URLType;
    
    statements(ref: TopicRef) : AsyncGenerator<Statement, void, void>;
    refutedStatements(ref: TopicRef) : AsyncGenerator<Statement, void, void>;
    activeStatements(ref: TopicRef) : AsyncGenerator<Statement, void, void>;
    freeze(ref: TopicRef, eID: eventId): FrozenTopicRef;
    situate(ref: TopicRef): TopicRef;
    findGlobalName(name: URLType, until: ?Date): AsyncGenerator<TopicRef, void, void>;
    myTopics(until: ?Date): AsyncGenerator<TopicRef, void, void>;
    includedPerspectives(until: ?Date): AsyncGenerator<FrozenPerspectiveRef, void, void>;
    knownForeignShadows(): AsyncGenerator<IPerspective, void, void>;


    // TODO: Realm ownership and permissions

/*
  StoredEvent[] eventQueue
  rID[] topics
  Map<perspective_id, eID> includedPerspectives
  TopicSnapshot makeSnapshot(FrozenTopicRef)
  TopicSnapshot latestSnapshot(FrozenTopicRef)
  TopicSnapshot makeCache(TopicRef)
  eID lastEventId
  TopicRef[] neighbourhood(TopicRef, SearchSpec)
  Map<gId, rID[]> aliases
*/
}

class FrozenPerspectiveRef {
    perspective: IPerspective
    when: Date
}


export interface Statement {
    
}

export type IPerspectiveFactoryFn = (url: URLType) => Promise<?IPerspective>;
