// @flow
import express from "express";
import bodyParser from "body-parser";
import {LINK_HEADER_REL} from "jsonld/lib/constants"
import {init as initKafkaStream} from "hyperknowledge-events-kafka"


// import {jsonld, realms, baseTopics, commands, events, processServer, utils } from "hyperknowledge"

import {
  type kId,
  type kGlobalId,
  type kName,
  type URLType,
  type kRealmId,
  type kAccessSpec
} from "hyperknowledge/src/types";
import {
  type IProcess,
  type IProcessDispatcher,
  type IEventStreamSpec
} from "hyperknowledge/src/baseTopics";

import {JLDContext, LocalRealm, urlFromRestSpec, remoteProcessFactory,
  Process as LocalProcess,
  RealmRegistry,
  BaseEvent,
  Realm,
  TopicXRef,
  CreateTopicDCommand,
  AddRelationDCommand,
  CommandProcessor,
  AddAttributeCommand,
  extractTargetId, Continuation, Continuations } from "hyperknowledge";

const host = process.env.host || "localhost";
const port = process.env.port || 3000;
const portString = Boolean(process.env.proxied)? '' : `:${port}`;
const protocol = Boolean(process.env.secured)?"https":"http";
const base = `${protocol}://${host}${portString}/`;
const testData = Boolean(process.env.testdata);
const kafkaBroker = process.env.kafka

if (kafkaBroker) {
  initKafkaStream(RealmRegistry)
}

const jsonld_mime = 'application/ld+json'

class Server {
  express: Object;
  continuations: Map<kId, Continuations>;
  processRegistry: Map<kAccessSpec, IProcess>;

  constructor() {
    this.express = express();
    this.continuations = new Map();
    this.processRegistry = new Map();
    this.express.use(bodyParser.json());
    this.express.get("/", (req, res, next) => res.json(this.realmsList(true)));
    this.express.get("/remote", async (req, res, next) => {
      const idA = req.query.id;
      const id = Array.isArray(idA)?idA[0]:idA;
      if (id) {
        const realm = await RealmRegistry.getRealm(id, true);
        if (realm) {
          return res.json(realm);
        }
        return res.status(404).send("No such realm");
      }
      return res.json(this.realmsList(false, true));
    });
    this.express.get("/evs/?", async (req, res: express$Response, next) => {
      res.json([... RealmRegistry.streamRegistry.keys()].map((x)=>`${base}evs/${x}`))
    })
    this.express.head("/evs/:streamId/?", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(404).send("No such stream")
      const lastId: kId = await eventStream.nextEventId()
      console.log(lastId)
      res.links({nextId: lastId})
      res.status(200).send()
    })
    this.express.get("/evs/:streamId/?", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(404).send("No such stream")
      const eventIdA = req.query.id;
      if (eventIdA) {
        const eventId = Array.isArray(eventIdA)?eventIdA[0]:eventIdA;
        const event = await eventStream.eventById(eventId);
        if (event) {
          res.links({LINK_HEADER_REL: `${base}evs/${streamId}/context`})
          res.json(event)
        }
        else res.status(404).send("No such event: " + eventId);
      } else {
        const events = []
        const fromEventA = req.query.from
        let fromEvent = Array.isArray(fromEventA)?fromEventA[0]:fromEventA;
        if (fromEvent !== undefined && typeof(fromEvent)!='string')
          fromEvent == null
        if (fromEvent && fromEvent.startsWith('ev:'))
          fromEvent = `${base}evs/${streamId}/${fromEvent.substring(3)}`
        for await (const ev of eventStream.eventsStartingFrom(fromEvent)) {
          events.push(ev)
        }
        res.links({LINK_HEADER_REL: `${base}evs/${streamId}/context`})
        res.json(events)
      }
    })
    this.express.get("/evs/:streamId/context", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(404).send("No such stream")
      res.json({"ev":{"@id":`${base}evs/${streamId}/`, "@prefix": true}})
    })
    this.express.get("/evs/:streamId/:eventId", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventId = req.params.eventId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(404).send("No such stream")
      const event = await eventStream.eventById(eventStream.spec.url + eventId);
      if (event) {
        res.links({LINK_HEADER_REL: `${base}evs/${streamId}/context`})
        res.json(event)
      }
      else res.status(404).send("No such event: " + eventId);
    })
    this.express.put("/evs/:streamId/?", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventId = req.params.streamId
      let spec = req.body
      if (typeof(spec) != 'object') {
        console.warn("spec not an object:"+spec)
        spec = {}
      }
      if (!spec.name) spec.name = streamId
      if (!spec.url) spec.url = `${base}evs/${streamId}/`
      if (!spec.type) spec.type = (kafkaBroker) ? 'kafka' : 'mem'
      if (spec.type == 'kafka') {
        if (!kafkaBroker)
          return res.status(400).send("no kafka broker")
        if (!spec.data)
          spec.data = `kafka://${kafkaBroker}/${streamId}/server`
      }
      try {
        const eventStream = await RealmRegistry.getEventStream(spec)
        if (!eventStream)
          return res.status(500).send("Could not create stream")
        else res.status(201).send("Created");
      } catch (error) {
        return res.status(500).send(error)
      }
    })
    this.express.delete("/evs/:streamId/?", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventId = req.params.streamId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(404).send("No such stream")
      try {
        await eventStream.resetEvents()
      } catch (error) {
        return res.status(500).send("Could not delete stream")
      }
      res.status(200).send("Deleted");
    })
    this.express.post("/evs/:streamId/?", async (req, res, next) => {
      const streamId = req.params.streamId
      const eventId = req.params.streamId
      const eventStream = RealmRegistry.streamRegistry.get(streamId)
      if (!eventStream)
        return res.status(500).send("No such stream")
      const kid = await eventStream.appendEvent(req.body)
      return res.status(201).location(kid).send()
    })
    this.express.get("/:realmId/?", async (req, res, next) => {
      const realm = await this.getRealm(req.params.realmId);
      if (!realm) return res.status(404).send("No such realm");
      const json = realm.toJSON();
      json.access = {
        events: ["rest:"+realm.logStreamSpec.url],
        inbox: "rest:"+realm.inboxStreamSpec.url,
        topics: ["rest:topic"],
        process: ["rest:process"]
      };
      this.addJsonLd(res, realm, json, true, true) // explicit context in realm
    });
    this.express.get(
      "/:realmId/context",
      async (req, res, next) => {
      const realm = await this.getRealm(req.params.realmId);
      if (!realm) return res.status(404).send("No such realm");
      res.type(jsonld_mime).json({'@context': realm.context.asContext()})
    });
    this.express.get(
      "/:realmId/process",
      async (req, res, next) => await this.handleGetProcesses(req, res, next)
    );
    this.express.post(
      "/:realmId/process",
      async (req, res, next) => await this.handlePostProcess(req, res, next)
    );
    this.express.get(
      "/:realmId/process/:processName",
      async (req, res, next) => await this.handleGetProcess(req, res, next)
    );
    this.express.post(
      "/:realmId/process/:processName",
      async (req, res, next) => await this.handleStartProcess(req, res, next)
    );
    this.express.post(
      "/:realmId/process/:processName/:token",
      async (req, res, next) => await this.handleProcessNext(req, res, next)
    );

    this.express.put("/:realmId/?", async (req, res, next) => {
      const id = req.params.realmId;
      let realm = await this.getRealm(id);
      if (realm) return res.status(400).send("Realm exists");
      realm = await this.addRealm(id, req.body.logStreamSpec, req.body.inboxStreamSpec);
      if (!realm) return res.status(500).send("Could not create realm");
      const json = realm.toJSON();
      json.access = {
        events: ["rest:"+realm.logStreamSpec.url],
        inbox: "inbox:"+realm.inboxStreamSpec.url,
        topics: ["rest:topic"],
        process: ["rest:process"]
      };
      this.addJsonLd(res.status(201), realm, json, true, true)
    });
    this.express.get("/:realmId/topic/@all", async (req, res, next) => {
      const realm = await this.getRealm(req.params.realmId);
      if (!realm) return res.status(404).send("No such realm");
      const json = await realm.topicJSON()
      // json['@context'] = realm.contextUrl
      this.addJsonLd(res, realm, json)
    });
    this.express.get(
      "/:realmId/topic/?",
      async (req, res, next) => await this.handleTopics(req, res, next)
    );
    this.express.get(
      "/:realmId/topic/:topicId?",
      async (req, res, next) => await this.handleTopic(req, res, next)
    );
    this.express.get(
      "/:realmId/ev/?",
      async (req, res, next) => await this.handleEvents(req, res, next)
    );
    this.express.get(
      "/:realmId/ev/:eventId?",
      async (req, res, next) => await this.handleEvent(req, res, next)
    );
    this.express.post(
      "/:realmId/ev",
      async (req, res, next) => await this.handlePostCommand(req, res, next)
    );
  }
  addJsonLd(res: express$Response, realm: Realm, json: any, ctx_in_json?: boolean, nowrap?: boolean) {
    res.type(jsonld_mime)
    if (realm.contextUrl && !ctx_in_json) {
      const links = {}
      links[LINK_HEADER_REL] = realm.contextUrl
      res.links(links)
    }
    if (!nowrap)
      json = realm.wrapJSONLD(json, ctx_in_json)
    return res.json(json)
  }

  realmsList(local: ?boolean, remote: ?boolean) {
    return [...RealmRegistry.realms.keys()].filter(id => {
      const isLocal = id.startsWith(base);
      return (local && isLocal) || (remote && !isLocal);
    });
  }
  async getRealm(realmId: kRealmId): Promise<?Realm> {
    return await RealmRegistry.getRealm(base + realmId + '/', false);
  }
  async addRealm(name: kName, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec): Promise<Realm> {
    logStreamSpec = logStreamSpec || {
      name: name,
      type: "mem",
      url: `${base}evs/${name}/`
    }
    inboxStreamSpec = inboxStreamSpec || {
      name: name+"_inbox",
      type: "mem",
      url: `${base}evs/${name}_inbox/`
    }
    const localRealm = new LocalRealm(base + name + '/', logStreamSpec, inboxStreamSpec);
    await localRealm.init()
    RealmRegistry.registerRealm(localRealm);
    return localRealm;
  }
  async ensureProcessSpecs(realm: IProcessDispatcher): Promise<void> {
    for (const process: IProcess of realm.getProcesses()) {
      let spec = process.spec;
      if (!spec) {
        // new process without a spec, create one.
        // TODO: Multiple access specs...
        spec = "rest:process/p" + this.processRegistry.size;
        process.spec = spec;
      }
      if (!this.processRegistry.has(spec)) {
        this.processRegistry.set(spec, process);
      }
    }
  }
  async handleGetProcesses(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    await this.ensureProcessSpecs(realm);
    const idA = req.query.id;
    if (idA) {
      const id = Array.isArray(idA)?idA[0]:idA;
      const process = RealmRegistry.getProcess(id);
      if (!process) return res.status(404).send("No such process");
      return res.json(process);
    }
    const json = {};
    for (const process: IProcess of realm.getProcesses()) {
      if (process.spec) json[process.id] = process.spec;
    }
    this.addJsonLd(res, realm, json)
  }
  async getLocalProcess(realm: Realm, processName: string): Promise<?LocalProcess> {
    await this.ensureProcessSpecs(realm);
    const targetUrl = realm.id + "/process/" + processName;
    for (const process: IProcess of realm.getProcesses()) {
      if (process instanceof LocalProcess) {
        const spec = process.spec;
        if (!spec) continue;
        try {
          const url = urlFromRestSpec(spec, realm.id + "/");
          if (url == targetUrl) return process;
        } catch (error) {}
      }
    }
  }
  async handleGetProcess(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    const process = await this.getLocalProcess(realm, req.params.processName);
    if (!process) return res.status(404).send("No such process");
    this.addJsonLd(res, realm, process.toJSON(null, realm.context))
  }
  async handlePostProcess(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    const body = (req.body: Object);
    const spec = body.spec;
    if (!spec) return res.status(400).send("Missing realm spec");
    const id = body["@id"];
    const process = await remoteProcessFactory(id, realm, spec);
    if (!process) return res.status(404).send("Could not find process");
    if (id && id != process.id)
      return res.status(404).send("Process spec does not match ID");
    RealmRegistry.registerProcess(process);
    res.status(201)
    this.addJsonLd(res, realm, process.toJSON(null, realm.context))
  }
  async handleEvents(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    // TODO: Make this give data progressively. Or page?
    const fromEventA = req.query.from;
    let fromEvent = Array.isArray(fromEventA)?fromEventA[0]:fromEventA;
    if (fromEvent !== undefined && typeof(fromEvent)!='string')
      fromEvent == null
    if (fromEvent && fromEvent.startsWith('ev:'))
      fromEvent = `${realm.logStreamSpec.url}${fromEvent.substring(3)}`
    if (fromEvent && fromEvent.startsWith('inbox:'))
      fromEvent = `${realm.inboxStreamSpec.url}${fromEvent.substring(3)}`
    const events = await realm.eventsJSON(fromEvent);
    this.addJsonLd(res, realm, events)
  }
  async handleEvent(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    // const topicNum = req.params.topicId;
    const eventId = base.substr(0, base.length-1) + req.url
    const event = await realm.eventById(eventId);
    if (event) {
      this.addJsonLd(res, realm, event.toJSON(null, realm.context))
    }
    else res.status(404).send("No such event: " + eventId);
  }

  async handleTopics(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    const topicIdA = req.query.id;
    if (topicIdA) {
      const topicId = Array.isArray(topicIdA)?topicIdA[0]:topicIdA;
      const topic = await realm.byId(topicId);
      if (topic) {
        this.addJsonLd(res, realm, topic.toJSON(null, realm.context))
      }
      else res.status(404).send("No such topic: " + topicId);
    } else {
      this.addJsonLd(res, realm, [...realm.concepts.keys()])
    }
  }

  async handleTopic(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm: ?Realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    // const topicNum = req.params.topicId;
    const topicId = base.substr(0, base.length-1) + req.url
    const topic = await realm.byId(topicId);
    if (topic) {
      this.addJsonLd(res, realm, topic.toJSON(null, realm.context))
    }
    else res.status(404).send("No such topic: " + topicId);
  }

  async handlePostCommand(req: express$Request, res: express$Response, next: express$NextFunction) {
    try {
      const realm = await this.getRealm(req.params.realmId);
      if (!realm) return res.status(404).send("No such realm");
      if (!req.body) return res.status(400).send("Body not well formatted");
      const command = await realm.revive(req.body);
      if (!(command instanceof BaseEvent)) {
        console.warn(command);
        return res.status(400).send("Not an event");
      }
      const result = await realm.applyCommand(command);
      if (!result) res.status(400);
      const events = await command.events() || []
      const json = events.map(e => e.toJSON(null, realm.context))
      this.addJsonLd(res, realm, json);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  }
  async handleStartProcess(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    const process: ?LocalProcess = await this.getLocalProcess(
      realm,
      req.params.processName
    );
    if (!process) return res.status(404).send("No such process");
    let continuations: ?Continuations = this.continuations.get(process.id);
    if (!continuations) {
      continuations = new Continuations(process);
      this.continuations.set(process.id, continuations);
    }
    const topics = await realm.revive(req.body);
    if (topics && topics[0] instanceof BaseEvent) {
      const command = topics[0]
      if (!process.canExpand().has(command.inherentTypeNameSafe)) {
        console.error("can't expand")
        return res.status(400).send("This process cannot handle this command");
      }
      const continuation = new Continuation(process.expandCommand(command));
      const token = continuations.add(continuation);
      return res
        .status(201)
        .location(
          `${base}${req.params.realmId}/process/${
            req.params.processName
          }/${token}`
        )
        .send();
    }
  }

  async handleProcessNext(req: express$Request, res: express$Response, next: express$NextFunction) {
    const realm = await this.getRealm(req.params.realmId);
    if (!realm) return res.status(404).send("No such realm");
    const process: ?IProcess = await this.getLocalProcess(
      realm,
      req.params.processName
    );
    if (!process) return res.status(404).send("No such process");
    let continuations: ?Continuations = this.continuations.get(process.id);
    if (!continuations)
      return res.status(500).send("No continuations for process");
    const continuation = continuations.get(req.params.token);
    if (!continuation) return res.status(404).send("No such continuation");
    const body = (req.body: Object);
    if (body.stop) {
      continuations.stop(continuation);
      return res.status(202).send("Stopped");
    }
    if (continuation.done) return res.status(202).send("Done");
    let events: BaseEvent[] = [];
    if (req.body) {
      let x = await realm.revive(req.body);
      if (x !== null && Array.isArray(x)) events = x;
    }
    const result: IteratorResult<BaseEvent, void> = await continuation.gen.next(
      events
    );
    continuation.done = result.done;
    if (result.value) {
      const command: BaseEvent = result.value;
      res.append("X-Done", continuation.done.toString())
      return this.addJsonLd(res, realm, command.toJSON(null, realm.context));
    } else if (result.done) {
      return res.append("X-Done", continuation.done.toString()).json(null)
    }
    return res.status(500);
  }

  start() {
    this.express.listen(port, error => {
      if (error) {
        console.error(error);
      } else {
        console.log(`ready on ${base}`);
      }
    });
  }
}

export const server: Server = new Server();
async function createTestData() {
  const logStreamSpec = {
    name: 'first',
    type: (kafkaBroker)?'kafka':'mem',
    data: (kafkaBroker)?`kafka://${kafkaBroker}/first/server`:'',
    url: `${base}evs/first/`
  }
  const inboxStreamSpec = {
    name: 'first_inbox',
    type: 'mem',
    url: `${base}evs/first_inbox/`
  }
  await server.addRealm("first", logStreamSpec, inboxStreamSpec)
  const localRealm: ?Realm = await RealmRegistry.getRealm(base + "first/", false)
  const processor = new CommandProcessor()
  await processor.ensureTypeCache()
  RealmRegistry.registerProcess(processor);
  if (localRealm) {
    const lastId = await localRealm.nextEventId()
    if (lastId == undefined || lastId.endsWith('/0')) {
      let command = new CreateTopicDCommand(localRealm);
      await localRealm.applyCommand(command);
      const conceptAId = await extractTargetId(command);
      command = new AddAttributeCommand(
        localRealm,
        conceptAId,
        new TopicXRef(
          RealmRegistry.vocabulary.title,
          RealmRegistry.vocabulary.rootRealm),
        null,
        {"@lang":"en", "@value":"here is a value"})
      await localRealm.applyCommand(command);
      command = new CreateTopicDCommand(localRealm, "Topic");
      await localRealm.applyCommand(command);
      const conceptBId = await extractTargetId(command);
      if (conceptAId && conceptBId) {
        command = new AddRelationDCommand(localRealm, null, [
          { roleName: "source", actor: localRealm.compactIri(conceptAId) },
          { roleName: "target", actor: localRealm.compactIri(conceptBId) }
        ]);
        await localRealm.applyCommand(command);
      }
    }
  }
}

 RealmRegistry.initBaseTypes().then(async () => {
   if (testData) {
     await createTestData();
   }
   server.start();
// }).catch(error => {
//   console.error("server initBaseType")
});
