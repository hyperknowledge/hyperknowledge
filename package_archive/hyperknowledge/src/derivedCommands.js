// @flow
import {
  type kId,
  type kGlobalId,
  type kName,
  type URLType,
  type kRealmId
} from "./types";
import {
  TopicXRef,
  BaseEvent,
  Realm,
  RealmRegistry,
  BaseTopic,
  Process,
  Attribute,
  RoleBinding,
  Concept,
  Relation,
  SimpleRelation,
  MetaConcept,
  ErrorEvent,
  hasError,
  SubEvent,
  type IRelation,
  type topicRefJson
} from "./baseTopics";
import { ConcreteRealm } from "./realms";
import {
  CreateTopicEvent,
  AddRoleTargetEvent,
  AddAttributeEvent,
  ImportTopicEvent,
  DeleteTopicEvent,
  AddToCollectionEvent,
  RemoveFromCollectionEvent,
  RejectEvent,
  extractTargetId,
  extractTargetRealm,
  simple_event_types,
  CreateTopicCommand,
  DeleteTopicCommand,
  ImportTopicCommand,
  AddAttributeCommand,
  AddRoleTargetCommand,
  AddToCollectionCommand,
  RemoveFromCollectionCommand,
  SplitConceptCommand,
  MergeConceptsCommand,
  UnmergeConceptCommand
} from "./baseEvents";
import { JLDContext } from "./jsonld"


export class CreateTopicDCommand extends BaseEvent {
  static inherentTypeName = "dc_create_concept";
  baseType: ?kName;
  baseAttributes: ?{ [key: kName]: any };

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    baseType: ?kName,
    baseAttributes: ?{ [key: kName]: any }
  ) {
    super(realmOrFromCommand);
    this.baseType = baseType;
    this.baseAttributes = baseAttributes;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.baseType = this.baseType;
    result.baseAttributes = this.baseAttributes;
    return result;
  }

  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<CreateTopicDCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    const result = new this(
      fromCommand || realm,
      json.baseType,
      json.baseAttributes
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export class AddRelationDCommand extends BaseEvent {
  static inherentTypeName = "dc_add_relation";
  baseType: ?kName;
  baseRoleBindings: ?({
    roleName: kName,
    actor: topicRefJson
  }[]);
  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    baseType: ?kName,
    baseRoleBindings: { roleName: kName, actor: topicRefJson }[]
  ) {
    super(realmOrFromCommand);
    this.baseType = baseType;
    this.baseRoleBindings = baseRoleBindings;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.baseType = this.baseType;
    result.baseRoleBindings = this.baseRoleBindings;
    return result;
  }

  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<AddRelationDCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    const result = new this(
      fromCommand || realm,
      json.baseType,
      json.baseRoleBindings
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export class ImportTopicDCommand extends BaseEvent {
  static inherentTypeName = "dc_import_topic";
  originalTopic: TopicXRef;
  keepRemoteBindings: ?(kId[]); // bindings to keep remote (TODO)
  skipBindings: ?(kId[]);
  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    originalTopic: TopicXRef,
    keepRemoteBindings: ?(kId[]),
    skipBindings: ?(kId[])
  ) {
    super(realmOrFromCommand);
    this.originalTopic = originalTopic;
    this.keepRemoteBindings = keepRemoteBindings;
    this.skipBindings = skipBindings;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.originalTopic = this.originalTopic.toJSON(null, ctx);
    result.keepRemoteBindings = this.keepRemoteBindings;
    result.skipBindings = this.skipBindings;
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<ImportTopicDCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    const originalTopic = await TopicXRef.reviver(json.originalTopic, realm, ctx)
    const result = new this(
      fromCommand || realm,
      originalTopic,
      json.keepRemoteBindings,
      json.skipBindings
    );
    return await result.reviverPhase2(json, ctx);
  }
}

export class SyncRealmDCommand extends BaseEvent {
  static inherentTypeName = "dc_sync_realm";
  sourceRealm: kRealmId;
  untilEvent: ?kId;
  // ideally, there should be a filter function...
  importConcepts: boolean;
  importRelations: boolean;
  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    sourceRealm: kRealmId,
    importConcepts: boolean,
    importRelations: boolean,
    untilEvent: ?kId
  ) {
    super(realmOrFromCommand);
    this.sourceRealm = sourceRealm;
    this.untilEvent = untilEvent;
    this.importConcepts = importConcepts;
    this.importRelations = importRelations;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.sourceRealm = this.sourceRealm;
    result.untilEvent = this.untilEvent;
    result.importConcepts = this.importConcepts;
    result.importRelations = this.importRelations;
    return result;
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<SyncRealmDCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    const result = new this(
      fromCommand || realm,
      json.sourceRealm,
      json.importConcepts,
      json.importRelations,
      json.untilEvent
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export const derived_event_types: Map<kName, typeof BaseEvent> = new Map()

for (const evtype of [
  CreateTopicDCommand,
  AddRelationDCommand,
  ImportTopicDCommand,
  SyncRealmDCommand,
  ]) {
    derived_event_types.set(evtype.inherentTypeNameSafe, evtype)
    RealmRegistry.registerReviver(evtype)
}
/**
A processor that knows some complex commands
*/
export class CommandProcessor extends Process {
  canExpandCommands: Set<kName>;
  constructor() {
    super("baseProcessor", RealmRegistry.rootRealm);
  }
  canExpand(): Set<kName> {
    if (!this.canExpandCommands) {
      this.canExpandCommands = new Set();
      this.canExpandCommands.add(CreateTopicDCommand.inherentTypeNameSafe);
      this.canExpandCommands.add(AddRelationDCommand.inherentTypeNameSafe);
      this.canExpandCommands.add(ImportTopicDCommand.inherentTypeNameSafe);
      this.canExpandCommands.add(SyncRealmDCommand.inherentTypeNameSafe);
    }
    return this.canExpandCommands;
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    let gen: AsyncGenerator<BaseEvent, void, BaseEvent[]>;
    switch (command.inherentTypeNameSafe) {
      case CreateTopicDCommand.inherentTypeName:
        gen = this.expandCreateTopic(((command: any): CreateTopicDCommand));
        break;
      case AddRelationDCommand.inherentTypeName:
        gen = this.expandAddRelation(((command: any): AddRelationDCommand));
        break;
      case ImportTopicDCommand.inherentTypeName:
        gen = this.expandImportTopic(((command: any): ImportTopicDCommand));
        break;
      case SyncRealmDCommand.inherentTypeName:
        gen = this.expandSyncRealm(((command: any): SyncRealmDCommand));
        break;
      default:
        throw new Error("Unknown command:"+command.inherentTypeNameSafe);
    }
    let events: BaseEvent[] = [];
    let notDone = true;
    while (notDone) {
      let r = await gen.next(events);
      if (r.value) {
        events = yield r.value;
      }
      notDone = !r.done;
    }
  }

  async *expandCreateTopic(
    command: CreateTopicDCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const realm = command.realm;
    const baseType = command.baseType
    const baseTypeRef = baseType? new TopicXRef(baseType, RealmRegistry.rootRealm) : null
    let subcommand: BaseEvent = new CreateTopicCommand(command, baseTypeRef);
    let events: BaseEvent[] = yield subcommand;
    if (events && hasError(events.values())) return;
    const conceptId: ?kId = await extractTargetId(subcommand);
    if (!conceptId) {
      yield new ErrorEvent(subcommand, "could not create concept")
      return;
    }
    if (command.baseAttributes) {
      for (const [key, value] of Object.entries(command.baseAttributes)) {
        const type = await realm.byName(key);
        if (type) {
          subcommand = new AddAttributeCommand(
            command,
            conceptId,
            type.asTopicXRef(),
            null,
            value
          );
          events = yield subcommand;
          if (events && hasError(events.values())) return;
        } else {
          command.addErrorEvent("Could not interpret type");
        }
      }
    }
  }

  async *expandAddRelation(
    command: AddRelationDCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const realm = command.realm;
    const baseType: ?kName = command.baseType
    const typeT: ?BaseTopic = baseType ? await realm.byName(baseType) : undefined
    const type: MetaConcept = (typeT && typeT instanceof MetaConcept) ? typeT : RealmRegistry.glob.Relation
    let subcommand: BaseEvent = new CreateTopicCommand(command, type.asTopicXRef());
    let events: BaseEvent[] = yield subcommand;
    if (hasError(events.values())) return;
    const conceptId: ?kId = await extractTargetId(subcommand);
    if (!conceptId) {
      events.push(new ErrorEvent(subcommand, "could not create relation"));
      return;
    }
    if (command.baseRoleBindings) {
      for (const val: {
        roleName: kName,
        actor: topicRefJson
      } of command.baseRoleBindings) {
        const type: ?BaseTopic = await realm.byName(val.roleName);
        if (type) {
          const actorRef = await TopicXRef.reviver(val.actor, command.realm)
          subcommand = new AddRoleTargetCommand(
            command,
            conceptId,
            type.asTopicXRef(),
            actorRef
          );
          events = yield subcommand;
          if (hasError(events.values())) return;
        } else {
          command.addErrorEvent("Could not interpret type");
        }
      }
    }
  }

  async *expandImportTopic(
    command: ImportTopicDCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const realm = command.realm;
    const remoteConcept: ?BaseTopic = await command.originalTopic.resolve()
    if (!remoteConcept) {
      throw new Error("Missing Realm");
    }
    if (remoteConcept instanceof BaseEvent) {
      throw new Error("Cannot import events");
    } else if (remoteConcept instanceof Realm) {
      throw new Error("Cannot import realms");
    }

    let subcommand: BaseEvent = new ImportTopicCommand(
      command,
      command.originalTopic
    );
    let events: BaseEvent[] = yield subcommand;
    if (hasError(events.values())) return;
    const importToRealm: ?Realm = await extractTargetRealm(subcommand);
    const conceptId: ?kId = await extractTargetId(subcommand);
    if (!(conceptId && importToRealm)) {
      events.push(new ErrorEvent(subcommand, "could not import"));
      return;
    }
    let newConcept: ?BaseTopic = await importToRealm.byId(conceptId);
    if (!newConcept) {
      throw new Error("internal error");
    }
    const keepRemoteBindings: Set<kId> = new Set(
      command.keepRemoteBindings || []
    );
    const skipBindings: Set<kId> = new Set(command.skipBindings || []);
    const remoteBindings: Set<kId> = new Set();
    const localizedBindings: Map<kId, kId> = new Map();
    let simpleRelationType: ?MetaConcept = null
    if (newConcept instanceof Relation)
      simpleRelationType = newConcept.canBeSimpleRelation()
    if (remoteConcept instanceof Concept && newConcept instanceof Concept) {
      for (const attributes: Attribute[] of remoteConcept.attributes.values()) {
        for (let remoteAttribute: Attribute of attributes) {
          if (skipBindings.has(remoteAttribute.id)) continue;
          // no keepRemote for Attribute bindings
          // subcommand = new AddAttributeCommand(command, conceptId,
          //         remoteAttribute.type.id, remoteAttribute.type.realm.id,
          //         remoteAttribute.value);
          subcommand = new ImportTopicCommand(
            command,
            remoteAttribute.asTopicXRef()
          );
          let events: BaseEvent[] = yield subcommand;
          if (hasError(events.values())) return;
          let attributeId: ?kId = await extractTargetId(subcommand);
          if (!attributeId) {
            events.push(
              new ErrorEvent(subcommand, "could not import attribute")
            );
            return;
          }
          const localAttribute = await importToRealm.byId(attributeId);
          if (!localAttribute || !(localAttribute instanceof Attribute)) {
            events.push(
              new ErrorEvent(subcommand, "could not import attribute")
            );
            return;
          }
          let intoAttributes: ?(Attribute[]) = newConcept.attributes.get(
            remoteAttribute.type
          );
          if (!intoAttributes) {
            intoAttributes = [];
            newConcept.attributes.set(remoteAttribute.type, intoAttributes);
          }
          intoAttributes.push(localAttribute);
        }
      }
      if (newConcept instanceof Relation && remoteConcept instanceof Relation) {
        for (const bindings: RoleBinding[] of remoteConcept._bindings.values()) {
          for (let remoteBinding: RoleBinding of bindings) {
            if (skipBindings.has(remoteBinding.id)) continue;
            // TODO: keep remote binding. May have to add new event...
            // subcommand = new AddRoleTargetCommand(
            //     command, conceptId,
            //     remoteBinding.type.id, remoteBinding.type.realm.id,
            //     remoteBinding.actorId, remoteBinding.actorRealm.id);
            subcommand = new ImportTopicCommand(
              command,
              remoteBinding.asTopicXRef()
            );
            let events: BaseEvent[] = yield subcommand;
            if (hasError(events.values())) return;
            let bindingId: ?kId = await extractTargetId(subcommand);
            if (!bindingId) {
              events.push(
                new ErrorEvent(subcommand, "could not import binding")
              );
              return;
            }
            const localBinding = await importToRealm.byId(bindingId);
            if (!localBinding || !(localBinding instanceof RoleBinding)) {
              events.push(
                new ErrorEvent(subcommand, "could not import binding")
              );
              return;
            }
            let intoBindings: ?(RoleBinding[]) = newConcept._bindings.get(
              localBinding.type
            );
            if (!intoBindings) {
              intoBindings = [];
              newConcept._bindings.set(localBinding.type, intoBindings);
            }
            intoBindings.push(localBinding);
          }
        }
      }
    } else if (
      remoteConcept instanceof SimpleRelation &&
        (newConcept instanceof SimpleRelation ||
          (newConcept instanceof Relation && simpleRelationType)))
    {
      if (newConcept instanceof Relation) {
        // this line to make flow happy, we know simpleRelationType is not empty
        simpleRelationType = simpleRelationType || RealmRegistry.glob.Relation
        const recast = new SimpleRelation(newConcept.id, newConcept.realm, simpleRelationType)
        await recast.recastPhase2(newConcept)
        newConcept = recast
      }
      let remoteBinding = remoteConcept.source;
      if (remoteBinding && !skipBindings.has(remoteBinding.id)) {
        // TODO: keep remote binding. May have to add new event...
        subcommand = new ImportTopicCommand(
          command,
          remoteBinding.asTopicXRef()
        );
        let events: BaseEvent[] = yield subcommand;
        if (hasError(events.values())) return;
        let bindingId: ?kId = await extractTargetId(subcommand);
        if (!bindingId) {
          events.push(new ErrorEvent(subcommand, "could not import binding"));
          return;
        }
        let localBinding = await importToRealm.byId(bindingId);
        if (!localBinding || !(localBinding instanceof RoleBinding)) {
          events.push(new ErrorEvent(subcommand, "could not import binding"));
          return;
        }
        newConcept.source = localBinding;
      }
      remoteBinding = remoteConcept.target;
      if (remoteBinding && !skipBindings.has(remoteBinding.id)) {
        // TODO: keep remote binding. May have to add new event...
        subcommand = new ImportTopicCommand(
          command,
          remoteBinding.asTopicXRef()
        );
        let events: BaseEvent[] = yield subcommand;
        if (hasError(events.values())) return;
        let bindingId: ?kId = await extractTargetId(subcommand);
        if (!bindingId) {
          events.push(new ErrorEvent(subcommand, "could not import binding"));
          return;
        }
        const localBinding = await importToRealm.byId(bindingId);
        if (!localBinding || !(localBinding instanceof RoleBinding)) {
          events.push(new ErrorEvent(subcommand, "could not import binding"));
          return;
        }
        newConcept.target = localBinding;
      }
    }
  }

  async *expandSyncRealm(
    command: SyncRealmDCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const realm = command.realm;
    const targetRealm: ?Realm = await RealmRegistry.getRealm(
      command.sourceRealm
    );
    if (!targetRealm) throw new Error("Cannot find targetRealm");
    const lastKnownEvent: ?kId = realm.lastProcessedEvents.get(targetRealm.id);
    const deleted: Set<kId> = new Set();
    let alreadyImportedMap: ?Map<kId, kId> = realm.remoteToLocalIdCache.get(
      targetRealm.id
    );
    if (!alreadyImportedMap) {
      alreadyImportedMap = new Map();
      realm.remoteToLocalIdCache.set(targetRealm.id, alreadyImportedMap);
    }
    function localizeRef(ref: TopicXRef): ?TopicXRef {
      if (ref.realmId == realm.id) return ref
      if (!alreadyImportedMap) return
      if (ref.realmId == targetRealm.id) {
        const localId = alreadyImportedMap.get(ref.id)
        if (localId)
          return new TopicXRef(localId, realm)
      } else {
        const map = realm.remoteToLocalIdCache.get(ref.realmId)
        if (map) {
          const localId = map.get(ref.id)
          if (localId)
            return new TopicXRef(localId, realm)
        }
      }
    }
    let events: ?(BaseEvent[]) = null;
    for await (const sourceEvent: BaseEvent of targetRealm.eventsStartingFrom(
      lastKnownEvent
    )) {
      if (!simple_event_types.has(sourceEvent.inherentTypeNameSafe))
        continue
      let destCommand: ?BaseEvent = null;
      // console.log(`sync: ${sourceEvent.id} ${sourceEvent.inherentTypeNameSafe}`)
      switch (sourceEvent.inherentTypeNameSafe) {
        case CreateTopicEvent.inherentTypeName:
        case ImportTopicEvent.inherentTypeName:
          if (sourceEvent instanceof CreateTopicEvent) {
            const targetId = sourceEvent.targetId;
            const target = await targetRealm.byId(targetId);
            if (!target) {
              deleted.add(targetId);
            } else if (alreadyImportedMap.get(target.id)) {
              // do nothing
            } else {
              let doImport = false;
              if (
                command.importRelations &&
                (target instanceof Relation || target instanceof SimpleRelation)
              ) {
                doImport = true;
              }
              if (command.importConcepts && target instanceof Concept) {
                doImport = true;
              }
              if (doImport) {
                destCommand = new ImportTopicDCommand(
                  command,
                  new TopicXRef(target)
                );
              }
            }
          }
          break;
        case DeleteTopicEvent.inherentTypeName:
          if (sourceEvent instanceof DeleteTopicEvent) {
            const targetId = sourceEvent.topicId;
            deleted.add(targetId);
            const localId = alreadyImportedMap.get(targetId);
            if (localId) {
              destCommand = new DeleteTopicCommand(
                command,
                localId
              );
            }
          }
          break;
        case AddRoleTargetEvent.inherentTypeName:
          if (sourceEvent instanceof AddRoleTargetEvent) {
            const sourceCommand = sourceEvent.fromCommand;
            const bindingId: kId = sourceEvent.topicId;
            const targetId: kId = sourceCommand.topicId;
            const localRelationId: ?kId = alreadyImportedMap.get(targetId);
            if (!localRelationId) break;
            const localRelationT = await realm.byId(localRelationId);
            if (
              !localRelationT ||
              !(
                localRelationT instanceof SimpleRelation ||
                localRelationT instanceof Relation
              )
            )
              break;
            const localRelation: IRelation = ((localRelationT: any): IRelation);
            const actorRef = sourceCommand.actorRef
            const localActorRef = localizeRef(actorRef)
            const roleRef = localizeRef(sourceCommand.roleRef) || sourceCommand.roleRef
            const role = await roleRef.resolve()
            if (!role || !(role instanceof MetaConcept)) break;
            // see if it was already imported as part of concept import
            const bindings = localRelation.bindingsMap().get(role);
            if (bindings) {
              let found = false;
              for (const binding of bindings) {
                if (
                  (binding.actorRef.id == actorRef.id &&
                    binding.actorRef.realmId == actorRef.realmId) ||
                  (localActorRef &&
                    binding.actorRef.realmId == localActorRef.realmId &&
                    binding.actorRef.id == localActorRef.id)
                ) {
                  found = true;
                  break;
                }
              }
              if (found) break;
            }
            // what was that about?
            // if (
            //   localRelation instanceof SimpleRelation ||
            //   bindingId == RealmRegistry.vocabulary.inherent
            // ) {
            //   const beforeActorRef = (sourceCommand.beforeActorRef
            //     ) ? localizeRef(sourceCommand.beforeActorRef) : undefined
            //   destCommand = new AddRoleTargetCommand(
            //     command,
            //     localRelationId,
            //     roleRef,
            //     localActorRef || actorRef,
            //     beforeActorRef
            //   );
            // } else {
              destCommand = new ImportTopicDCommand(
                command,
                new TopicXRef(bindingId, targetRealm)
              );
            // }
          }
          break;
        case AddAttributeEvent.inherentTypeName:
          if (sourceEvent instanceof AddAttributeEvent) {
            const attributeId: kId = sourceEvent.topicId;
            const targetId: kId = sourceEvent.fromCommand.topicId;
            const localConceptId: ?kId = alreadyImportedMap.get(targetId);
            if (!localConceptId) break;
            const localConceptT = await realm.byId(localConceptId);
            if (!localConceptT || !(localConceptT instanceof Concept)) break;
            const localConcept: Concept = ((localConceptT: any): Concept);
            const sourceCommand = sourceEvent.fromCommand
            const attributeType = await sourceCommand.attributeTypeRef.resolve()
            if (!attributeType || !(attributeType instanceof MetaConcept))
              break;
            // see if it was already imported as part of concept import
            const attributes = localConcept.attributes.get(attributeType);
            if (attributes) {
              let found = false;
              for (const attribute of attributes) {
                if (attribute.value == sourceEvent.fromCommand.value) {
                  found = true;
                  break;
                }
              }
              if (found) break;
            }
            destCommand = new ImportTopicDCommand(
              command,
              new TopicXRef(attributeId, targetRealm)
            );
          }
          break
        case AddToCollectionEvent.inherentTypeName:
          throw new Error("TODO");
        case RemoveFromCollectionEvent.inherentTypeName:
          throw new Error("TODO");
      }
      if (!(sourceEvent instanceof RejectEvent)) {
        if (!destCommand) {
          // TODO: key would be localized key of original event.
          yield new RejectEvent(command, null, sourceEvent.id, targetRealm.id);
        } else {
          destCommand.derivedFromRef = sourceEvent.asTopicXRef();
          events = yield destCommand;
          if (destCommand instanceof ImportTopicDCommand) {
            let found = false
            for (const received_event: BaseEvent of events) {
              for await (const event of received_event.rec_events()) {
                if (event instanceof ImportTopicEvent) {
                  alreadyImportedMap.set(
                    destCommand.originalTopic.id,
                    event.topicId
                  );
                  found = true
                  break
                }
              }
              if (found) break
            }
          }
        }
      }
      realm.lastProcessedEvents.set(targetRealm.id, sourceEvent.id);
      if (sourceEvent.id == command.untilEvent) break;
    }
  }
}

// RealmRegistry.registerProcess(new CommandProcessor());
