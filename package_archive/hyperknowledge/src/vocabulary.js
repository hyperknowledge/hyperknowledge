// @flow
import { type kGlobalId, type kName, type URLType } from "./types";
import { in_nodejs } from "./utils"

export var base_jsonld = null

if (in_nodejs) {
  const fs = require("fs")
  base_jsonld = JSON.parse(fs.readFileSync('hyperknowledge.jsonld', 'utf8'));
}

// contents given in hyperknowledge.jsonld
// I would like to use "http://purl.org/hyperknowledge/context"
// but https://github.com/digitalbazaar/jsonld.js/issues/269
export const base_jsonld_url = "https://hyperknowledge.org/ns/hyperknowledge.jsonld"
// TODO: This knowledge should be encapsulated in a topic.owl.ttl document.
export const baseRelations: { [key: kName]: { [key: kName]: kName } } = {
  instanceOf: {
    Concept: "Class",
    Class: "Concept",  // close to owl:Class, but topic-friendly. Circular.
    Topic: "Class",
    Relation: "Class",  // close to rdf:Property, but topic-friendly
    Collection: "Class",  // Subset of concepts
    VirtualProxy: "Class",  // for topic merging
    Process: "Class",
    Realm: "Class",
    rootRealm: "Realm",
    RoleBinding: "RoleMetaClass",
    AttributeBinding: "AttributeMetaClass",
    Relation: "RelationMetaClass",
  },
  subClassOf: {
    source: "RoleBinding",
    target: "RoleBinding",
    instanceOf: "Relation",
    Concept: "Topic",
    // they're classes because they're instances of subclasses of topic:Class...
    // Relation: "Class",
    // AttributeBinding: "Class",
    // RoleBinding: "Class",
    inCollection: "RoleBinding",   // Collection inclusion relation
    mergedInto: "RoleBinding",   // VirtualProxy inclusion relation
    Collection: "Relation",
    VirtualProxy: "Relation",
    Process: "Concept",
    Realm: "Process",
    title: "AttributeBinding",
    description: "AttributeBinding",
    instanceOf: "Relation",
    subClassOf: "Relation",  // close to rdfs:subClassOf, but topic-friendly
    inCollection: "Relation",
    mergedInto: "Relation",
    RoleMetaClass: "Class",
    AttributeMetaClass: "Class",
    RelationMetaClass: "Class",
  }
};
