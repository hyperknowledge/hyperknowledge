// @flow
import {JLDContext} from "./jsonld"
// add "hyperknowledge-events-kafka": "file:../hk_events_kafka" to packages.json
// import {init as initKafkaStream} from "hyperknowledge-events-kafka"

import { LocalRealm } from "./realms";
import { urlFromRestSpec, remoteProcessFactory } from "./restfulSource"
export * from "./baseTopics";
export * from "./derivedCommands";
export * from "./baseEvents";
import { Continuation, Continuations } from "./processServer";

export {
  LocalRealm,
  urlFromRestSpec,
  remoteProcessFactory,
  Continuation,
  Continuations};
