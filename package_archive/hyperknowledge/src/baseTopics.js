// @flow
import {
  type kId,
  type kGlobalId,
  type kName,
  type URLType,
  type kRealmId,
  type kAccessSpec
} from "./types";
import { baseRelations, base_jsonld_url } from "./vocabulary";
import { chain, strMapToObj, objToStrMap, sleep } from "./utils";
import { JLDContext } from "./jsonld";

/**
Something that has an identity
*/
export class AbstractBase {
  static inherentTypeName: ?kName;
  static _inherentType: ?MetaConcept;
  id: kId;
  realm: Realm;

  constructor(id: ?kId, realm: Realm) {
    if (id) this.id = id;
    this.realm = realm;
  }

  static inherentTypeC(): ?MetaConcept {
    let cls: Class<AbstractBase> = this
    while (cls) {
      if (cls && cls.hasOwnProperty("_inherentType"))
        return ((cls._inherentType: any): MetaConcept)
      cls = ((Object.getPrototypeOf(cls): any): Class<AbstractBase>)
    }
  }

  get inherentType(): ?MetaConcept {
    return this.constructor.inherentTypeC()
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<AbstractBase> {
    throw new Error("Abstract");
  }
  async ensureTypeCache(reset: ?boolean) {
    if (this.constructor.hasOwnProperty("_inherentType") && !reset)
      return
    if (this.constructor.inherentTypeName &&
        this.constructor.hasOwnProperty('inherentTypeName')) {
      const realm: ?Realm = this.realm;
      if (realm) {
        const type: ?BaseTopic = await realm.byName(
          this.constructor.inherentTypeName
        );
        if (type && type instanceof MetaConcept) {
          this.constructor._inherentType = type;
          // console.log(`setting type ${type.name} on ${this.id}`)
          await type.ensureTypeCache(); // TODO: Do we want to propagate reset?
          return;
        }
      }
    }
  }
}

export type IEventStreamSpec = { name: kName, type: string, data?: string, url: URLType }
type IRealmFactoryFn = (id: kRealmId, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec) => Promise<?Realm>;
type IEventStreamFactoryFn = (spec: IEventStreamSpec, nocreate: ?boolean) => Promise<?IEventStream>;
export type IRawEventCallbackFn = (ev: Object) => Promise<void>
export type IEventCallbackFn = (ev: BaseEvent) => Promise<void>

interface IRealmRegistry {
  realms: Map<kId, Realm>;
  vocabulary: { [key: kName]: kGlobalId };
  glob: { [key: kName]: MetaConcept };
  rootRealm: RootRealm;
  revivers: Map<kName, Class<AbstractBase>>;

  registerRealm(realm: Realm): void;

  initBaseTypes(): Promise<void>;

  getRealm(id: kRealmId, allowCreation: ?boolean, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec): Promise<?Realm>;
  getEventStream(spec: IEventStreamSpec, nocreate: ?boolean): Promise<?IEventStream>;
  getGlobalType(id: kGlobalId): ?MetaConcept;
  registerReviver(cls: Class<AbstractBase>): void;
  registerRevivers(cls: Array<Class<AbstractBase>>): void;
  registerRealmFactory(f: IRealmFactoryFn): void;
  registerEventStreamFactory(type: string, f: IEventStreamFactoryFn): void;
  registerProcess(process: IProcess): void;
  getProcess(id: kId): ?IProcess;
  getProcesses(): Array<IProcess>;
}

declare var RealmRegistry: IRealmRegistry;

export type topicRefJson = kId | { realm: kRealmId, rid: kId }

export class TopicXRef {
  static inherentTypeName = "TopicXRef";
  reftime: ?Date
  realmId: kRealmId
  id: kId
  _target: ?BaseTopic
  _realm: ?Realm
  constructor(target: BaseTopic | string, realm?: Realm | string, reftime?: Date) {
    if (target instanceof BaseTopic) {
      this._target = target
      this.id = target.id
      this._realm = target.realm
      this.realmId = target.realm.id
    } else {
      this.id = target
      if (realm) {
        if (realm instanceof Realm) {
          this._realm = realm
          this.realmId = realm.id
        } else
          this.realmId = realm
      } else {
        if (RealmRegistry.rootRealm.byNameCached(target)) {
          this._realm = RealmRegistry.rootRealm
          this.realmId = RealmRegistry.rootRealm.id
        } else
          throw new Error("Missing realm information")
      }
    }
    // wait on reftime
  }

  toJSON(key: ?string, ctx: ?JLDContext): topicRefJson {
    if (!ctx && this._realm) {
      ctx = this._realm.context
    }
    const rid = ctx ? ctx.compactIri(this.id) : this.id
    if (rid.indexOf(':') == -1) {
      // global name
      return rid
    }
    if (rid.startsWith('ev:') && this._realm && ctx == this._realm.context) {
      return rid
    }
    const realm = ctx ? ctx.compactIri(this.realmId) : this.realmId
    return { realm, rid }
  }

  static async reviver(json: topicRefJson, realm: Realm, ctx?: JLDContext): Promise<TopicXRef> {
    if (typeof(json) == 'string') {
      if (ctx) {
        json = ctx.expandIri(json)
        const base = ctx.base
        if (base && base != realm.id) {
          const baseRealm = await RealmRegistry.getRealm(base, true)
          if (baseRealm)
            realm = baseRealm
          else
            throw new Error("Unknown foreign realm: " + base)
        }
      }
      if (RealmRegistry.rootRealm.byIdCached(json))
        return new TopicXRef(json, RealmRegistry.rootRealm)
      return new TopicXRef(json, realm)
    }
    if (!(json.realm && json.rid))
      throw new Error("Incomplete reference")
    ctx = ctx || realm.context
    const realmId = ctx.expandIri(json.realm)
    let myRealm = realm
    if (realmId != realm.id)
      myRealm = await RealmRegistry.getRealm(realmId);
    if (!myRealm)
      throw new Error("Cannot find realm")
    return new TopicXRef(json.rid, myRealm)
  }

  async realm(): Promise<Realm> {
    let realm: ?Realm = this._realm
    if (realm == null)
      this._realm = realm = await RealmRegistry.getRealm(this.realmId)
    if (realm == null)
      throw new Error("Cannot get realm "+this.realmId)
    return realm
  }

  async resolve(): Promise<?BaseTopic> {
    const realm = await this.realm()
    return await realm.byName(this.id)
  }

  async resolveEvent(): Promise<?BaseEvent> {
    const realm = await this.realm()
    const id = realm.expandIri(this.id)
    const ev = await realm.eventById(id)
    return ev
  }
  async resolveEither(): Promise<?BaseTopic> {
    try {
      return this.resolve()
    } catch (error) {}
    try {
      return this.resolveEvent()
    } catch (error) {}
  }

};

export type topicRef = TopicXRef | kId | kName;

/**
Something that has an identity, and can be pointed to by a Relation
*/
export class BaseTopic extends AbstractBase {
  /* If related to remote concepts, may participate in remote relations.
  Note: Could this also be in realm's relation cache?
  Does it apply to events?
  Should I only store the XRefs here?
  */
  inRemoteRelations: ?(IRelation[]);
  /*
      Concepts of this realm may be imported (forked) from another realm.
      This tells us the concept's original Id in another realm, by realm Id.
   */
  sourceRealmIds: ?Map<kRealmId, kId>;
  constructor(id: ?kId, realm: Realm) {
    super(id, realm);
  }

  baseTypes(): Set<MetaConcept> {
    // should be overridden most of the time
    const types = new Set()
    if (this.inherentType)
      types.add(this.inherentType)
    else {
      console.warn('missing inherent type')
    }
    return types
  }
  get shortName(): ?(kName | kId) {
    if (this.id)
      return this.realm.compactIri(this.id)
  }
  asTopicXRef(): TopicXRef {
    return new TopicXRef(this)
  }

  asTopicRef(realm: Realm): topicRef {
    if (realm == this.realm) {
      const shortName = this.shortName
      if (shortName)
        return shortName
    }
    return new TopicXRef(this)
    // const myShortName = this.shortName
    // const relShortName = referenceRealm.compactIri(this.id)
    // const result: Object = {
    //   realm: this.realm.shortName,
    //   id: this.shortName
    // }
    // if (myShortName != relShortName) {
    //   result['@context'] = this.realm.contextUrl
    // }
    // return result;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const _ctx: JLDContext = ctx || this.realm.context
    const compact = (id) => _ctx.compactIri(id)
    const localContext = _ctx == this.realm.context
    const result: Object = {}
    if (this.id)
      result['@id'] = (localContext)? this.shortName : this.id // we could expand in context...
    if (!localContext)
      result['realm'] = this.realm.id
    const inherentTypeName = this.constructor.inherentTypeName;
    // if (inherentTypeName) result.inherent = compact(inherentTypeName);
    const typeArray = []
    for (const baseType of this.baseTypes())
      typeArray.push(baseType.asTopicRef(this.realm))
    if (typeArray.length > 1)
      result['@type'] = typeArray
    else if (typeArray.length == 1)
      result['@type'] = typeArray[0]
    else {
      console.warn("empty typeArray for "+this.id)
      // this causes issues with sync
      // result['@type'] = "Concept"
    }
    const sourceRealmIds = this.sourceRealmIds;
    if (sourceRealmIds) {
      result.aka = []
      for (const [realm: kId, rid: kId] of sourceRealmIds.entries()) {
        // do not compact remote Ids for now.
        result.aka.push({rid, realm})
      }
    }
    const inRels = this.realm.relationsOfTopic(this.id);
    if (inRels) {
      result.inRels = [...inRels].map(r => r.asTopicRef(this.realm));
    }
    return result;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<BaseTopic> {
    ctx = ctx || realm.context
    const result = new this(ctx.expandIri(json["@id"]), realm);
    return await result.reviverPhase2(json, ctx);
  }

  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    ctx = ctx || this.realm.context
    const expand = (id) => ctx ? ctx.expandIri(id) : id
    if (json["@id"])
      this.id = expand(json["@id"]);
    if (json.inRels) {
      for (const xrel of json.inRels) {
        const relRef = await TopicXRef.reviver(xrel, this.realm, ctx)
        if (relRef) {
          const rel = await relRef.resolve()
          if (!rel)
            throw new Error("Missing relation")
          if (!this.inRemoteRelations) this.inRemoteRelations = [];
          if (rel instanceof Relation || rel instanceof SimpleRelation)
            this.inRemoteRelations.push(rel);
        }
      }
    }
    if (json.aka) {
      let sourceRealmIds: ?Map<kRealmId, kId> = this.sourceRealmIds;
      if (!sourceRealmIds) {
        sourceRealmIds = new Map();
        this.sourceRealmIds = sourceRealmIds;
      }
      for (const aka of json.aka) {
        const ref = await TopicXRef.reviver(aka.rid, aka.realm, ctx)
        sourceRealmIds.set(ref.realm.id, ref.id)
        this.realm.registerRemoteId(ref.realm.id, this.id, ref.id)
      }
    }
    return this;
  }

  hasType(type: MetaConcept): boolean {
    const inherentType = this.inherentType;
    return inherentType != null && type.isSubtypeOf(inherentType);
  }

  /**
  Convert the local Id to the remote Id relative to given realm
  */
  remoteId(realm: ?Realm): kId {
    if (realm && this.sourceRealmIds) {
      return this.sourceRealmIds.get(realm.id) || this.id;
    }
    return this.id;
  }

  /**
  The id of the topic by which this event will be indexed.
  Most topics will index themselves, except (attribute, role) bindings
  which will be indexed by their (Concept, Relation) respectively.
  */
  keyTopicId(realm: ?Realm): kId {
    if (realm && realm != this.realm) {
        const id = this.realm.localizeId(this.id, realm.id)
        if (id)
            return id
    }
    return this.id
  }

  async *inRelations(
    relationType: ?MetaConcept,
    roleType: ?MetaConcept,
    localOnly: ?boolean,
    clear_cache: ?boolean
  ): AsyncGenerator<IRelation, void, void> {
    // console.log(`inRelations: search rel ${relationType? (relationType.name || relationType.id):'none'}, role ${roleType?(roleType.name || roleType.id):'none'} on ${this.id}`)
    if (!this.realm) {
      return;
    }
    for (let rel: IRelation of chain(
      this.realm.relationsOfTopic(this.id),
      localOnly ? null : this.inRemoteRelations
    )) {
      // if (rel == this)
      //   continue
      if (rel != this) {
        if (rel.id == this.id)
          console.error('wtf')
        await rel.ensureTypeCache();
      }
      if (relationType && relationType != this)
        await relationType.ensureTypeCache()
      if (relationType != undefined && !rel.hasType(relationType)) continue;
      if (roleType) {
        let found = false;
        const bindingsMap = rel.bindingsMap();
        for (let role: MetaConcept of bindingsMap.keys()) {
          if (role) {
            if (clear_cache && role != this) {
              await role.ensureTypeCache();
            }
            if (role.isSubtypeOf(roleType)) {
              const bindings: ?(RoleBinding[]) = bindingsMap.get(role);
              if (!bindings) {
                continue;
              }
              for (let binding: RoleBinding of bindings) {
                // this role binding corresponds to this actor or a remote actor corresponding to this
                if (
                  (this.id == binding.actorRef.id &&
                    this.realm.id == binding.actorRef.realmId) ||
                  this.id == this.realm.localizeRef(binding.actorRef)
                ) {
                  found = true;
                  break;
                }
              }
              if (found) {
                break;
              }
            }
          }
        }
        if (!found) {
          continue;
        }
      }
      yield rel;
    }
  }
  /**
  follow the relation to a list of Concepts
  */
  async *relatedConcepts(
    relationType: ?MetaConcept,
    fromRoleType: ?MetaConcept,
    toRoleType: ?MetaConcept,
    localOnly: ?boolean,
    clear_cache: ?boolean
  ): AsyncGenerator<BaseTopic, void, void> {
    for await (let rel of this.inRelations(
      relationType,
      fromRoleType,
      localOnly,
      clear_cache
    )) {
      await rel.ensureTypeCache()
      if (toRoleType)
        await toRoleType.ensureTypeCache()
      for await (let c: BaseTopic of rel.actorsByRole(
        toRoleType,
        clear_cache
      )) {
        if (c != this)
          yield c;
      }
    }
  }
  /**
  follow the relation to a list of Concepts
  */
  async relationClosure(
    closure: Set<BaseTopic>,
    relationType: ?MetaConcept,
    fromRoleType: ?MetaConcept,
    toRoleType: ?MetaConcept,
    localOnly: ?boolean,
    clear_cache: ?boolean
  ): Promise<void> {
    closure.add(this)
    await BaseTopic.relationClosureS(closure, relationType, fromRoleType, toRoleType, localOnly, clear_cache)
  }

  static async relationClosureS(
    closure: Set<BaseTopic>,
    relationType: ?MetaConcept,
    fromRoleType: ?MetaConcept,
    toRoleType: ?MetaConcept,
    localOnly: ?boolean,
    clear_cache: ?boolean
  ): Promise<void> {
    // console.log(`-> relationClosure: ${this.id} on ${relationType?relationType.name:'*'}, ${fromRoleType?fromRoleType.name:'*'}->${toRoleType?toRoleType.name:'*'}`)
    let baseValues: Set<BaseTopic> = new Set(closure);
    while (baseValues.size > 0) {
      let additions: Set<BaseTopic> = new Set();
      for (const c: BaseTopic of baseValues.values()) {
        for await (let sub: BaseTopic of c.relatedConcepts(
          relationType,
          fromRoleType,
          toRoleType,
          localOnly,
          clear_cache
        )) {
          if (!baseValues.has(sub) && !closure.has(sub)) {
            additions.add(sub);
          }
        }
      }
      // set update...
      for (const c: BaseTopic of baseValues) {
        closure.add(c)
      }
      baseValues = additions
    }
    // console.log('<- relationClosure: '+[...closure].map(x=>x.id).join(', '))
  }

  async recast(newType: ?MetaConcept): Promise<BaseTopic> {
    const hasType: (aType: MetaConcept) => boolean = aType => {
      if (newType) return newType.isSubtypeOf(aType);
      else return this.hasType(aType);
    };
    let result: ?BaseTopic = null;
    const clRels = [];
    for await (const c of this.relatedConcepts(
      RealmRegistry.glob.instanceof,
      RealmRegistry.glob.target,
      RealmRegistry.glob.source
    ))
      clRels.push(c);
    const isProcess = hasType(RealmRegistry.glob.Process);
    const isMetaConcept =
      hasType(RealmRegistry.glob.Class) || clRels.length > 0;
    const isRelation = hasType(RealmRegistry.glob.Relation);
    if (Number(isProcess) + Number(isMetaConcept) + Number(isRelation) > 1) {
      throw new Error("Exclusive types");
    }
    if (isRelation) {
      // first settle the simple relation case
      if (this instanceof Relation) {
        const simpleRelType: ?MetaConcept = this.canBeSimpleRelation(newType);
        if (simpleRelType)
          result = new SimpleRelation(this.id, this.realm, simpleRelType);
      } else if (
        newType &&
        (!(this instanceof Concept) || this.attributes.size == 0)
      ) {
        result = new SimpleRelation(this.id, this.realm, newType);
      }
      if (result === null) {
        if (hasType(RealmRegistry.glob.VirtualProxy)) {
          result = new VirtualProxy(this.id, this.realm);
        } else if (hasType(RealmRegistry.glob.Collection)) {
          result = new Collection(this.id, this.realm);
        } else {
          result = new Relation(this.id, this.realm);
        }
      }
    } else if (isProcess) {
      if (hasType(RealmRegistry.glob.Realm))
        throw new Error("Cannot cast into realm");
      result = new Process(this.id, this.realm);
    } else if (isMetaConcept) {
      result = new MetaConcept(this.id, this.realm);
    } else {
      // TODO: Error if newClass is a binding class.
      result = new Concept(this.id, this.realm);
    }
    if (!result || result.constructor === this.constructor) {
      return this;
    }
    await result.recastPhase2(this, newType);
    return result;
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    this.sourceRealmIds = original.sourceRealmIds;
    this.inRemoteRelations = original.inRemoteRelations;
    // replace in remoteRelations? THINK HARD.
    original.realm.replace(this);
  }
}

/**
A topic with an inherent single type.
*/
export class SingleTypeTopic extends BaseTopic {
  type: MetaConcept;
  constructor(id: kId, realm: Realm, type: MetaConcept) {
    super(id, realm);
    this.type = type;
    const inherentType = this.inherentType;
    if (RealmRegistry && inherentType && !type.isSubtypeOf(inherentType)) {
      console.warn(type.id)
      console.warn([...type._superTypes.values()].map(x=>x.name||x.id))
      throw new Error(`type ${type.name || 'missing?'} should be a subtype of inherent type ${inherentType.name || 'missing?'}`);
    }
  }
  baseTypes(): Set<MetaConcept> {
    // cache the set?
    const types = new Set()
    types.add(this.type)
    return types
  }
  static async extractType(json: Object, realm: Realm): Promise<MetaConcept> {
    const expand = (id) => realm.context.expandIri(id)
    if (!json["@type"]) throw new Error("missing type information");
    let typeRealm = realm
    if (json.typeRealm) {
      typeRealm = await RealmRegistry.getRealm(expand(json.typeRealm));
      if (!typeRealm) throw new Error("Cannot find type realm");
    }
    const typeName = json["@type"];
    const type = await typeRealm.byName(typeName);
    if (!type || !(type instanceof MetaConcept)) {
      throw new Error("Cannot find type: " + typeName);
    }
    return type;
  }
  async ensureTypeCache(reset: ?boolean) {
    await super.ensureTypeCache(reset);
    await this.type.ensureTypeCache();
  }
  hasType(type: MetaConcept): boolean {
    return type.isSubtypeOf(this.type);
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    await super.recastPhase2(original, newType);
    if (newType) this.type = newType;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<SingleTypeTopic> {
    ctx = ctx || realm.context
    const result = new this(
      ctx.expandIri(json["@id"]),
      realm,
      await SingleTypeTopic.extractType(json, realm)
    );
    return await result.reviverPhase2(json, ctx);
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const _ctx: JLDContext = ctx || this.realm.context
    const compact = (id) => _ctx.compactIri(id)
    const result = super.toJSON(key, _ctx);
    // note the name is within the target context... TODO: change @context (ouch)
    // possibly the @type could be something like
    // "@type" : {"@id": "xyz", "@context": "...", "realm": "..."}
    result["@type"] = compact(this.type.id)
    result.typeRealm = compact(this.type.realm.id);
    return result;
  }
}
/**
Simple, replayable data event.
All Commands must translate to a replayable series of BaseEvents.
most Commands have a target topic and a keyTopic. The latter corresponds
to the keyTopicId of the target topic. The keyTopic must be local,
the target usually so.
Eventually, it must be possible to reconstitute the keyTopic from the
sequence of associated events.
*/
export class BaseEvent extends BaseTopic {
  idPromise: ?Promise<kId>
  _fromCommand: ?BaseEvent; // usually AtomicCommand
  keyId: ?kId;
  derivedFromRef: ?TopicXRef; // ref to event from an other realm, when imported from there.
  _events: ?(BaseEvent[])
  _eventRefs: ?(TopicXRef[])
  constructor(realmOrFromCommand: BaseEvent | Realm, keyId: ?kId) {
    const fromCommand: ?BaseEvent = (realmOrFromCommand instanceof BaseEvent) ? realmOrFromCommand : null;
    const realm: Realm = (fromCommand)? fromCommand.realm : ((realmOrFromCommand: any): Realm);
    super(null, realm)
    if (fromCommand) {
      this._fromCommand = fromCommand;
      fromCommand.addEvent(this);
    }
    this.keyId = keyId
  }
  // set id(id: kId) {
  //   if (!id.startsWith(this.realm.id)) {
  //     console.error(`wrong id: {id} not in {this.realm.id}`)
  //   }
  //   this.id = id
  // }
  get fromCommand(): ?BaseEvent {
    return this._fromCommand
  }

  get type(): MetaConcept {
    const inherentType = this.inherentType;
    if (!inherentType) {
      console.warn(this.toJSON())
      throw new Error("events must have an inherent type");
    }
    return inherentType;
  }
  baseTypes(): Set<MetaConcept> {
    const s = new Set()
    s.add(this.type)
    return s
  }

  hasEvents(): boolean {
    return (!!this._events) || (!!this._eventRefs)
  }

  async *eventsG(): AsyncGenerator<BaseEvent, void, void> {
    const eventRefs = this._eventRefs
    let events = this._events
    if (eventRefs && !events) {
      events = []
      for (const ref: TopicXRef of eventRefs) {
        const event: ?BaseEvent = await ref.resolveEvent()
        if (event) {
          events.push(event)
          yield event
        }
      }
      this._events = events
      this._eventRefs = undefined
    } else if (events) {
      for (const event of events) {
        yield event
      }
    }
  }

  async events(): Promise<BaseEvent[]> {
    if (this._events) {
      return this._events.slice()
    }
    const events: BaseEvent[] = []
    if (this._eventRefs) {
      for await (const ev of this.eventsG())
        events.push(ev)
    }
    return events  // now equivalent to this._events
  }

  async *rec_events(): AsyncGenerator<BaseEvent, void, void> {
    for await (const event of this.eventsG()) {
      yield event
      yield* event.rec_events()
    }
  }
  addEvent(ev: BaseEvent) {
    let events = this._events;
    if (!events) {
      events = [];
      this._events = events;
    }
    events.push(ev)
  }

  addErrorEvent(msg: string) {
    this.addEvent(new ErrorEvent(this, msg));
  }
  static get inherentTypeNameSafe(): kName {
    const inherentTypeName = this.inherentTypeName;
    if (!inherentTypeName) {
      throw new Error("events must have an inherent type");
    }
    return inherentTypeName;
  }

  get inherentTypeNameSafe(): kName {
    return this.constructor.inherentTypeNameSafe;
  }

  async recast(newType: ?MetaConcept): Promise<BaseTopic> {
    throw new Error("Cannot recast events");
  }
  async waitForId(): Promise<kId> {
    if (this.id)
      return this.id
    if (this.idPromise)
      return await this.idPromise
    while (!this.id) {
      await sleep(200)
    }
    return this.id
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const _ctx: JLDContext = ctx || this.realm.context
    const result = super.toJSON(key, _ctx);
    const events = this._events
    if (events && events.length) {
      // result.events = this._events.map(ev => ev.id?_ctx.compactIri(ev.id):null);
      result.events = events.filter((ev) => ev.id).map(
        ev => ev.asTopicXRef().toJSON(null, _ctx));
      // for (const ev of this.events) {
      //   if (!ev.id) {
      //     console.warn("missing id:"+JSON.stringify(ev))
      //   }
      // }
    }
    const fromCommand = this._fromCommand
    if (fromCommand) {
      result.fromCommand = fromCommand.asTopicXRef().toJSON(null, _ctx)
    }
    const derivedFrom = this.derivedFromRef
    if (derivedFrom) {
      result.derivedFrom = derivedFrom.toJSON(null, _ctx)
    }
    if (this.keyId)
      result.keyId = _ctx.compactIri(this.keyId);
    return result;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<BaseEvent> {
    let fromCommand: ?BaseEvent = null;
    if (json.fromCommand) {
      fromCommand = await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
    }
    ctx = ctx || realm.context
    const keyId = (json.keyId) ? ctx.expandIri(json.keyId) : undefined
    const result = new this(fromCommand || realm, keyId);
    return await result.reviverPhase2(json, ctx);
  }
  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    ctx = ctx || this.realm.context
    if (json.derivedFrom) {
      this.derivedFromRef = await TopicXRef.reviver(json.derivedFrom, this.realm, ctx);
    }
    if (json.events) {
      const refs: TopicXRef[] = []
      for (const ref of json.events) {
        refs.push(await TopicXRef.reviver(ref, this.realm, ctx))
      }
      // delay resolving the references, lazy eval, otherwise circularity
      this._eventRefs = refs
    }
    return this;
  }
}

/**
Simple, replayable data event.
All Commands must translate to a replayable series of BaseEvents.
most Commands have a target topic and a keyTopic. The latter corresponds
to the keyTopicId of the target topic. The keyTopic must be local,
the target usually so.
Eventually, it must be possible to reconstitute the keyTopic from the
sequence of associated events.
*/
export class SubEvent<SuperEventT: BaseEvent> extends BaseEvent {
  constructor(fromCommand: SuperEventT, keyId: ?kId) {
    super(fromCommand, keyId)
  }
  get fromCommand(): SuperEventT {
    return ((this._fromCommand: any): SuperEventT)
  }
  static async getBaseCommand<SuperEventT>(
    json: any,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<SuperEventT> {
    const fromCommandRef = await TopicXRef.reviver(json.fromCommand, realm, ctx)
    const fromCommand = fromCommandRef ? await fromCommandRef.resolveEvent() : null
    if (fromCommand && fromCommand instanceof BaseEvent) {
      // TODO: check if it's really a SuperEventT
      return ((fromCommand: any): SuperEventT);
    }
    throw new Error(`Cannot find base command: ${JSON.stringify(json.fromCommand)} in ${realm.id}`);
  }
}

export class ErrorEvent extends BaseEvent {
  static inherentTypeName = "eer";
  errorMessage: string;
  constructor(realmOrFromCommand: BaseEvent | Realm, errorMessage: string) {
    super(realmOrFromCommand);
    this.errorMessage = errorMessage;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const _ctx: JLDContext = ctx || this.realm.context
    const result = super.toJSON(key, _ctx);
    result.errorMessage = this.errorMessage;
    return result;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<ErrorEvent> {
    const fromCommand = await realm.eventById(json.fromCommand);
    if (!fromCommand)
      throw new Error("Cannot find base command");
    const result = new this(fromCommand, json.errorMessage);
    return await result.reviverPhase2(json, ctx);
  }
}

export function hasError(events: Iterator<BaseEvent>): ?ErrorEvent {
  for (const event: BaseEvent of events) {
    if (event instanceof ErrorEvent) return event;
  }
}

/**
An attribute binding for a Concept.
Holds the value for a given key attribute (given as a concept).
Questions: Should we hold scoping information?
Should we hold an ordered list of values?
*/
export class Attribute extends SingleTypeTopic {
  static inherentTypeName = "AttributeBinding";
  target: Concept;
  value: any;

  constructor(id: kId, target: Concept, key: MetaConcept, value: any) {
    super(id, target.realm, key);
    this.target = target;
    this.value = value;
  }
  async recast(newType: ?MetaConcept): Promise<BaseTopic> {
    throw new Error("Cannot recast attributes");
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.value = this.value;
    if (!key) {
      ctx = ctx || this.realm.context
      result.target = this.target.asTopicXRef().toJSON(null, ctx);
    } else if (this.realm == this.target.realm) {
      delete result.realm;
    }
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext,
    concept?: Concept
  ): Promise<Attribute> {
    const type = await SingleTypeTopic.extractType(json, realm);
    let target: ?BaseTopic;
    if (json.target) {
      const targetRef = await TopicXRef.reviver(json.target, realm, ctx)
      target = await targetRef.resolve()
    }
    target = target || concept
    if (!target || !(target instanceof Concept))
      throw new Error("Cannot find target");
    const result = new this(json["@id"], target, type, json.value);
    return await result.reviverPhase2(json, ctx);
  }
  /**
  The id of the topic by which this event will be indexed
  */
  keyTopicId(realm: ?Realm): kId {
    return this.target.keyTopicId(realm)
  }

}

/**
A role binding for a Relation. Says which actor (Concept) plays which role in which Relation.
The bindings are an ordered list for any given role type (see Relation._bindings)
*/
export class RoleBinding extends SingleTypeTopic {
  static inherentTypeName = "RoleBinding";
  inRelation: IRelation;
  actorRef: TopicXRef;

  constructor(
    id: kId,
    inRelation: IRelation,
    role: MetaConcept,
    actorRef: TopicXRef
  ) {
    super(id, inRelation.realm, role);
    this.inRelation = inRelation;
    this.actorRef = actorRef;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    // This is to check whether we're coming directly from the json '@graph'.
    // Not sure it handles full relations well. TODO: a better toJSON...
    ctx = ctx || this.realm.context
    if (!key || !Number.isNaN(Number.parseInt(key))) {
      result.target = this.inRelation.asTopicXRef().toJSON(null, ctx)
    } else if (this.realm == this.inRelation.realm) {
      delete result.realm;
    }
    result.actor = this.actorRef.toJSON(null, ctx);
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext,
    relation?: IRelation
  ): Promise<RoleBinding> {
    const type = await SingleTypeTopic.extractType(json, realm);
    let target: ?BaseTopic;
    if (json.target) {
      const targetRef = await TopicXRef.reviver(json.target, realm, ctx)
      target = await targetRef.resolve()
    }
    target = target || relation;
    if (
      !target ||
      !(target instanceof Relation || target instanceof SimpleRelation)
    ) {
      throw new Error("Cannot find target");
    }
    ctx = ctx || realm.context
    const actorRef = await TopicXRef.reviver(json.actor, realm, ctx)
    if (!actorRef)
      throw new Error(`Could not revive {JSON.stringify(json.actor)}`)
    const result = new this(
      json["@id"],
      target,
      type,
      actorRef
    );
    return await result.reviverPhase2(json, ctx);
  }

  async actor(): Promise<?BaseTopic> {
    return await this.actorRef.resolve()
  }
  // temporary synonym
  get role(): MetaConcept {
    return this.type;
  }

  async recast(newType: ?MetaConcept): Promise<BaseTopic> {
    throw new Error("Cannot recast role bindings");
  }
  /**
  The id of the topic by which this event will be indexed
  */
  keyTopicId(realm: ?Realm): kId {
    return this.inRelation.keyTopicId(realm)
  }

}

/**
The main class.
Concepts have attributes, play a role in a Relation, belong to many Collections, and live in a Realm.
They have multiple classes thanks to the instanceOf Relation.
*/
export class Concept extends BaseTopic {
  /**
   * Attributes, like RoleBindings, have a single MetaConcept as key.
   * May be local or remote.
   */
  attributes: Map<MetaConcept, Attribute[]>;
  // private cache variables
  /*
      a cache of all the base types of this concept.
   */
  _baseTypesCache: Set<MetaConcept>;
  static inherentTypeName = "Concept";

  constructor(id: kId, realm: Realm) {
    super(id, realm);
    this.attributes = new Map();
    this._baseTypesCache = new Set();
  }
  *allAttributes(): Generator<Attribute, void, void> {
    for (const attributes: Attribute[] of this.attributes.values()) {
      for (const attribute: Attribute of attributes) yield attribute;
    }
  }
  addAttribute(attr: Attribute) {
    let attributes = this.attributes.get(attr.type);
    if (!attributes) {
      attributes = [];
      this.attributes.set(attr.type, attributes);
    }
    attributes.push(attr);
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    if (this.attributes.size)
      result.attributes = [...this.allAttributes()].map(a =>
        a.toJSON("attributes", ctx)
      );
    return result;
  }
  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    if (json.attributes)
      for (const attrib: Object of json.attributes) {
        const attribute: Attribute = await Attribute.reviver(
          attrib,
          this.realm,
          ctx,
          this
        );
        this.addAttribute(attribute);
      }
    await this.ensureTypeCache();
    return this;
  }

  async attributesOfType(attributeType: MetaConcept): Promise<Attribute[]> {
    const result: Attribute[] = [];
    for (const [
      type: MetaConcept,
      attributes: Attribute[]
    ] of this.attributes.entries()) {
      if (type.isSubtypeOf(attributeType)) {
        result.push(...attributes);
      }
    }
    return result;
  }

  hasType(type: MetaConcept): boolean {
    for (const aType: MetaConcept of this.baseTypes()) {
      if (aType.isSubtypeOf(type)) {
        return true;
      }
    }
    return false;
  }

  async ensureTypeCache(reset: ?boolean) {
    if (this._baseTypesCache.size > 0 && !reset)
      return
    await super.ensureTypeCache(reset);
    const types: Set<BaseTopic> = new Set();
    const inherentType = this.inherentType;
    if (inherentType) {
      this._baseTypesCache.add(inherentType)
      types.add(inherentType)
    } else {
      console.warn("Missing inherent type: "+this.id)
    }
    for await (const type of this.relatedConcepts(
      RealmRegistry.glob.instanceOf,
      RealmRegistry.glob.source,
      RealmRegistry.glob.target,
      false
    )) {
      if (type instanceof MetaConcept && type != this) {
        types.add(type);
        await type.ensureTypeCache();
      }
      // type is actually a MetaConcept, should use MetaConcept.superTypes instead of closure below
    }
    for (let t: BaseTopic of types) {
      if (t instanceof MetaConcept) {
        this._baseTypesCache.add(t);
      }
    }
    const reduced: MetaConcept[] = Concept.reduceTypes([
      ...this._baseTypesCache
    ]);
    if (reduced.length < this._baseTypesCache.size) {
      // console.log(`reduce: ${this._baseTypesCache.size} -> ${reduced.length}`)
      this._baseTypesCache = new Set(reduced);
    }
    // console.log("C.ensureTypeCache yields: " + reduced.map(x=>x.name).join(", ")+" for "+this.id)
  }

  baseTypes(): Set<MetaConcept> {
    return this._baseTypesCache;
  }

  inCollections(): Collection[] {
    // TODO
    return [];
  }

  static reduceTypes(types: MetaConcept[]): MetaConcept[] {
    let result: MetaConcept[] = [];
    for (const typeA: MetaConcept of types) {
      result = result.filter((typeB: MetaConcept) => !typeA.isSubtypeOf(typeB));
      let found = false;
      for (const typeB: MetaConcept of result) {
        if (typeB.isSubtypeOf(typeA)) {
          found = true;
          break;
        }
      }
      if (!found) {
        result.push(typeA);
      }
    }
    return result;
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    await super.recastPhase2(original, newType);
    if (original instanceof Concept) {
      this.attributes = original.attributes;
      for (const attributeL: Attribute[] of this.attributes.values()) {
        for (const attribute: Attribute of attributeL) {
          attribute.target = this;
        }
      }
    }
    // recompute types
    await this.ensureTypeCache();
  }
}

/**
A relation is a concept that links to other concepts (actors) playing roles.
It should have vocabulary.Relation among its types.
*/
export class Relation extends Concept {
  _bindings: Map<MetaConcept, RoleBinding[]>;
  static inherentTypeName = "Relation";
  constructor(id: kId, realm: Realm) {
    super(id, realm);
    this._bindings = new Map();
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    if (this._bindings.size)
      result.bindings = [...this.bindings()].map(b => b.toJSON("bindings", ctx));
    return result;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<IRelation> {
    // SimpleRelation also comes here
    if (json.source || json.target)
      return await SimpleRelation.reviver(json, realm, ctx);
    return await super.reviver(json, realm);
  }

  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    if (json.bindings)
      for (const attrib: Object of json.bindings) {
        const binding = await RoleBinding.reviver(attrib, this.realm, ctx, this);
        await this.addBinding(binding);
      }
    return this;
  }

  async *actorsByRole(
    role: ?MetaConcept,
    clear_cache: ?boolean
  ): AsyncGenerator<BaseTopic, void, void> {
    if (role) {
      for (let a_role: MetaConcept of this._bindings.keys()) {
        if (a_role) {
          a_role.ensureTypeCache(clear_cache);
          if (a_role.isSubtypeOf(role)) {
            const bindings: ?(RoleBinding[]) = this._bindings.get(a_role);
            if (!bindings) {
              continue;
            }
            for (const binding of bindings) {
              const actor = await binding.actor();
              if (actor) yield actor;
            }
          }
        }
      }
    } else {
      for (let bindings: ?(RoleBinding[]) of this._bindings.values()) {
        if (!bindings) {
          continue;
        }
        for (const binding of bindings) {
          const actor = await binding.actor();
          if (actor) yield actor;
        }
      }
    }
  }



  bindingsMap(): Map<MetaConcept, RoleBinding[]> {
    return this._bindings;
  }
  *bindings(): Generator<RoleBinding, void, void> {
    for (const bindings of this._bindings.values()) {
      for (const binding of bindings) yield binding;
    }
  }
  replace(targetNode: BaseTopic, originalId: kId, originalRealm: Realm) {
    for (const bindings of this._bindings.values()) {
      for (const binding of bindings) {
        if (
          binding.actorRef.id == originalId &&
          binding.actorRef.realmId == originalRealm.id
        ) {
          binding.actorRef = targetNode.asTopicXRef()
        }
        if (
          binding.type.id == originalId &&
          binding.type.realm == originalRealm &&
          targetNode instanceof MetaConcept
        ) {
          binding.type = targetNode;
        }
      }
    }
  }
  deleteBinding(binding: RoleBinding): boolean {
    let bindings: ?(RoleBinding[]) = this._bindings.get(binding.role);
    if (!bindings) return false;
    for (let i: number of bindings.keys()) {
      const aBinding: RoleBinding = bindings[i];
      if (
        aBinding.actorRef.id == binding.actorRef.id &&
        aBinding.actorRef.realmId == binding.actorRef.realmId
      ) {
        if (bindings.length) {
          bindings.splice(i, 1);
        } else {
          this._bindings.delete(binding.role);
        }
        return true;
      }
    }
    return false;
  }
  async addBinding(
    binding: RoleBinding,
    beforeActorId: ?kId,
    beforeActorRealmId: ?kRealmId
  ): Promise<IRelation> {
    const role = binding.role;
    const bindings: ?(RoleBinding[]) = this._bindings.get(role);
    if (bindings) {
      if (beforeActorId) {
        let i: number = 0;
        for (i of bindings.keys()) {
          const b: RoleBinding = bindings[i];
          if (
            b.actorRef.id == beforeActorId &&
            b.actorRef.realmId == beforeActorRealmId
          ) {
            bindings.splice(i, 0, binding);
            break;
          }
        }
        // not found before; we could put at end, but canceling for now.
        if (i >= bindings.length) {
          throw new Error("Could not found position");
        }
      } else {
        bindings.push(binding);
      }
    } else if (beforeActorId) {
      throw new Error("Could not find position");
    } else {
      this._bindings.set(role, [binding]);
    }
    const simpleType = this.canBeSimpleRelation();
    if (simpleType) {
      const recast = new SimpleRelation(this.id, this.realm, simpleType);
      await recast.recastPhase2(this);
      return recast;
    }
    return this;
  }
  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    await super.recastPhase2(original, newType);
    if (original instanceof Relation) {
      this._bindings = original._bindings;
      for (const bindingL: RoleBinding[] of this._bindings.values()) {
        for (const binding: RoleBinding of bindingL) {
          binding.inRelation = this;
        }
      }
    } else if (original instanceof SimpleRelation) {
      if (original.source) await this.addBinding(original.source);
      if (original.target) await this.addBinding(original.target);
    }
  }
  canBeSimpleRelation(newType: ?MetaConcept): ?MetaConcept {
    if (this.attributes.size) return null;
    if (this._bindings.size > 2) return null;
    let numSource = 0;
    let numTarget = 0;
    for (const roleType: MetaConcept of this._bindings.keys()) {
      const isSource = roleType.isSubtypeOf(RealmRegistry.glob.source);
      const isTarget = roleType.isSubtypeOf(RealmRegistry.glob.target);
      if (Number(isSource) + Number(isTarget) != 1) return null;
      const bindings = this._bindings.get(roleType)
      if (bindings && bindings.length > 1)
        return null
      if (isSource) numSource += 1;
      if (isTarget) numTarget += 1;
    }
    if (numSource > 1 || numTarget > 1) return null;
    let baseTypes: MetaConcept[] = [...this.baseTypes()];
    if (newType) {
      baseTypes.push(newType);
      baseTypes = Concept.reduceTypes(baseTypes);
    }
    if (baseTypes.length == 1) return baseTypes[0];
    return null;
  }
}

/**
A simple relation, with a single type, source and destination.
Avoids a potential infinite regress with the instanceOf relation
itself needing an instanceOf relation.
*/
export class SimpleRelation extends SingleTypeTopic {
  source: ?RoleBinding;
  target: ?RoleBinding;
  static inherentTypeName = "Relation";
  constructor(id: kId, realm: Realm, type: MetaConcept) {
    super(id, realm, type);
  }
  canBeSimpleRelation(newType: ?MetaConcept): ?MetaConcept {
    return this.type
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    if (this.source) result.source = this.source.toJSON("source", ctx);
    if (this.target) result.target = this.target.toJSON("target", ctx);
    // result.bindings = [result.source, result.target].filter(Boolean);
    return result;
  }
  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<SimpleRelation> {
    const result = new this(
      json["@id"],
      realm,
      await SingleTypeTopic.extractType(json, realm)
    );
    return await result.reviverPhase2(json, ctx);
  }

  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    if (json.source)
      this.source = await RoleBinding.reviver(json.source, this.realm, ctx, this);
    if (json.target)
      this.source = await RoleBinding.reviver(json.target, this.realm, ctx, this);
    return this;
  }

  bindingsMap(): Map<MetaConcept, RoleBinding[]> {
    const map = new Map();
    if (this.source) {
      map.set(this.source.type, [this.source]);
    }
    if (this.target) {
      map.set(this.target.type, [this.target]);
    }
    return map;
  }
  *bindings(): Generator<RoleBinding, void, void> {
    if (this.source) yield this.source;
    if (this.target) yield this.target;
  }
  async *actorsByRole(
    role: ?MetaConcept,
    clear_cache: ?boolean
  ): AsyncGenerator<BaseTopic, void, void> {
    for (const binding: ?RoleBinding of [this.source, this.target]) {
      if (!binding) {
        continue;
      }
      if (role) {
        await binding.type.ensureTypeCache(clear_cache);
      }
      if (!role || binding.type.isSubtypeOf(role)) {
        const actor = await binding.actor();
        if (actor) {
          yield actor;
        }
      }
    }
  }
  replace(targetNode: BaseTopic, originalId: kId, originalRealm: Realm) {
    for (const binding: ?RoleBinding of [this.source, this.target]) {
      if (!binding) {
        continue;
      }
      if (
        binding.actorRef.id == originalId &&
        binding.actorRef.realmId == originalRealm.id
      ) {
        binding.actorRef = new TopicXRef(targetNode)
      }
      if (
        binding.type.id == originalId &&
        binding.type.realm == originalRealm &&
        targetNode instanceof MetaConcept
      ) {
        binding.type = targetNode;
      }
    }
  }
  deleteBinding(binding: RoleBinding): boolean {
    if (binding == this.source) {
      this.source = null;
    } else if (binding == this.target) {
      this.target = null;
    } else return false;
    return true;
  }
  async addBinding(
    binding: RoleBinding,
    beforeActorId: ?kId,
    beforeActorRealmId: ?kRealmId
  ): Promise<IRelation> {
    if (!beforeActorId) {
      if (binding.role.isSubtypeOf(RealmRegistry.glob.source) && !this.source) {
        this.source = binding;
        binding.inRelation = this;
        return this;
      }
      if (binding.role.isSubtypeOf(RealmRegistry.glob.target) && !this.target) {
        this.target = binding;
        binding.inRelation = this;
        return this;
      }
    }
    const recast = new Relation(this.id, this.realm);
    await recast.recastPhase2(this);
    return await recast.addBinding(binding);
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    if (original instanceof Relation) {
      const bindings: Map<MetaConcept, RoleBinding[]> = original.bindingsMap();
      if (bindings.size > 2)
        throw new Error(
          "There should be at most 2 bindings in a SimpleRelation"
        );
      for (const bindingL: RoleBinding[] of bindings.values()) {
        if (bindingL.length != 1)
          throw new Error(
            "There should be a single binding per type in a SimpleRelation"
          );
        const binding: RoleBinding = bindingL[0];
        const isSource: boolean = binding.hasType(RealmRegistry.glob.source);
        const isTarget: boolean = binding.hasType(RealmRegistry.glob.target);
        if (Number(isSource) + Number(isTarget) != 1)
          throw new Error(
            "SimpleRelation bindings should be either source or target"
          );
        if (isSource) this.source = binding;
        if (isTarget) this.target = binding;
      }
      // take new type, discard original's type?
      await super.recastPhase2(original, newType);
    }
  }
}

export type IRelation = Relation | SimpleRelation;

/**
The metaclass for concepts. Also used for typing relations and roles.
*/
export class MetaConcept extends Concept {
  _superTypes: Set<MetaConcept>;
  name: ?kName;
  static inherentTypeName = "Class";
  constructor(id: kId, realm: Realm) {
    super(id, realm);
    // this._superTypes = new Set();
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    if (this.name) result.name = this.name;
    return result;
  }
  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    this.name = json.name;
    return this;
  }

  isSubtypeOf(type: MetaConcept): boolean {
    if (type == this) return true;
    const types = this.superTypes();
    if (types == null) {
      console.error("null types?")
      return false
    }
    return types.has(type);
  }

  async ensureTypeCache(reset: ?boolean) {
    if (this._superTypes && !reset)
      return
    const superTypes: Set<BaseTopic> = new Set();
    this._superTypes = new Set()
    this._superTypes.add(this);
    await super.ensureTypeCache(reset)
    // if (this.hasType(RealmRegistry.glob.Class)) ????
    await this.relationClosure(
      superTypes,
      RealmRegistry.glob.subClassOf,
      RealmRegistry.glob.source,
      RealmRegistry.glob.target,
      false
    );
    // set needs a union update...
    for (let c: BaseTopic of superTypes) {
      if (c instanceof MetaConcept) {
        await c.ensureTypeCache(); // propagate reset? I'm assuming not.
        this._superTypes.add(c);
      }
    }
    // TODO: check if modified, and if so propagate on subtypes...
    // console.log('MC.ensureTypeCache yields: '+[...this._superTypes].map(x=>x.name||x.id).join(', ')+" for "+this.name)
  }

  /**
  The supertypes of this as a type
  */
  superTypes(clear_cache: ?boolean): Set<MetaConcept> {
    return this._superTypes;
  }

  /**
  All the types that are subtypes of the given type.
  */
  async subtypes(typeId: MetaConcept): Promise<Set<MetaConcept>> {
    const result: Set<BaseTopic> = new Set();
    await this.relationClosure(
      result,
      RealmRegistry.glob.subClassOf,
      RealmRegistry.glob.target,
      RealmRegistry.glob.source
    );
    return new Set(
      (([...result].filter(c => c instanceof MetaConcept): any): MetaConcept[])
    );
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    await super.recastPhase2(original, newType);
    // TODO: extract the name from an appropriate relation, if any.
  }
}

/**
A collection is a view of other concepts.
It may be defined by extension (with explicit roles)
or by intension (with a formula.)
*/
export class Collection extends Relation {
  static inherentTypeName = "Collection";

  constructor(id: kId, realm: Realm) {
    super(id, realm);
  }
  contains(): BaseTopic[] {
    // TODO
    return [];
  }
}

/**
A VirtualProxy is the result of a merge operation:
some concepts were found similar enough that they are considered as a single concept.
Many operations normalize from the concept to the VirtualProxy.
(Terminology from TopicMaps, to be reviewed)
*/
export class VirtualProxy extends Collection {
  static inherentTypeName = "VirtualProxy";

  constructor(id: kId, realm: Realm) {
    super(id, realm);
  }
}

export interface ITopicSource {
  realm: Realm;
  byId(id: kId): Promise<?BaseTopic>;
}

export interface IEventSource {
  eventById(id: kId): Promise<?Object>;
  eventsStartingFrom(eventId: ?kId): AsyncGenerator<Object, void, void>;
  nextEventId(): Promise<?kId>;
  shutdown(): Promise<void>
}

export interface IEventStream extends IEventSource {
  spec: IEventStreamSpec;
  setCallback(cb: IRawEventCallbackFn): void;
  clearCallback(): void;
  appendEvent(event: Object): Promise<kId>;
  resetEvents(until_event: ?kId): Promise<void>;
  isResettable(): boolean
}

export interface IEventSink extends IEventSource {
  submitEvent(json: Object): BaseEvent[];
}

export interface IProcess {
  id: kId;
  spec: ?kAccessSpec;
  expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]>;
  canExpand(): Set<kName>;
  toJSON(key: ?string, ctx: ?JLDContext): Object;
}
export interface IProcessDispatcher {
  expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]>;
  getProcesses(): Array<IProcess>;
}

/**
A process consumes and generates events to enact a transformation on the data.
In particular, it will transform semantic events into elementary events.
*/
export class Process extends Concept implements IProcess {
  static inherentTypeName = "Process";
  spec: ?kAccessSpec;
  constructor(id: kId, realm: Realm) {
    super(id, realm);
  }
  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("Abstract class");
  }
  toJSON(key: ?string, ctx: ?JLDContext): any {
    const result = super.toJSON(key, ctx);
    const expand = this.canExpand();
    if (expand) result.expands = [...expand.values()];
    return result;
  }

  canExpand(): Set<kName> {
    return new Set();
  }
}

/**
A process dispatcher acts as a process, but actually dispatches commands to
known processes
*/
export class ProcessDispatcher extends Process implements IProcessDispatcher {
  preferredProcessor: Map<kName, IProcess>;
  constructor(id: kId, realm: Realm) {
    super(id, realm);
    this.preferredProcessor = new Map();
    this.registerAll(this);
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const commandType = command.inherentTypeNameSafe;
    let process: ?IProcess = this.preferredProcessor.get(commandType);
    if (process === null)
      // i.e. we do not want this command
      return;
    if (!process) {
      // look at processes known to us.
      for (process of this.getProcesses()) {
        if (process.canExpand().has(commandType)) break;
        process = null;
      }
    }
    if (process) {
      const gen: AsyncGenerator<BaseEvent,
        void,
        BaseEvent[]> = process.expandCommand(command);
      let events: ?(BaseEvent[]);
      while (true) {
        const r = await gen.next(events);
        if (r.value) events = yield r.value;
        if (r.done) break;
      }
    }
  }

  getProcesses(): IProcess[] {
    // Implementation choice: Procesess are shared between realms in a server.
    return RealmRegistry.getProcesses();
  }

  registerProcess(commandName: kName, process: IProcess): void {
    this.preferredProcessor.set(commandName, process);
  }
  registerAll(process: Process): void {
    for (const name: kName of this.canExpand()) {
      this.registerProcess(name, process);
    }
  }
  toJSON(key: ?string, ctx: ?JLDContext): any {
    const result = super.toJSON(key, ctx);
    result.processes = this.getProcesses().map(p => p.toJSON(null, ctx));
    result.preferredProcessor = strMapToObj(
      new Map([...this.preferredProcessor.entries()].map(([k, p]) => [k, p.id]))
    );
    return result;
  }

  async recastPhase2(original: BaseTopic, newType: ?MetaConcept) {
    await super.recastPhase2(original, newType);
    // TODO: preferredProcessors from realm data
  }
  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    ctx = ctx||this.realm.context
    for (const procJson: Object of json.processes) {
      const id = procJson["@id"];
      if (id && !RealmRegistry.getProcess(id)) {
        const process: ?IProcess = await this.realm.revive(procJson, ctx);
        if (process) {
          RealmRegistry.registerProcess(process);
        }
      }
    }
    const processes: Map<kId, IProcess> = new Map();
    for (const [commandName: kName, processId: kId] of Object.entries(
      json.preferredProcessor
    )) {
      if (typeof processId == "string") {
        let process: ?IProcess = RealmRegistry.getProcess(processId);
        if (process) this.registerProcess(commandName, process);
      }
    }
    return this;
  }

}

/**
A Realm is the unit of ownership of Concepts.
Every concept lives in a realm, though forks can live in other realms.
The realm exposes concepts through an event outbox stream,
and receives change requests through an event inbox stream.
*/
export class Realm extends ProcessDispatcher
  implements ITopicSource {
  // id here is a kRealmId, but hard to redefine properly.
  /**
      Named local (usually meta-)concepts by name
   */
  context: JLDContext;
  contextUrl: ?kId;
  baseRealm: ?Realm;
  /**
      Local concepts, by Id
   */
  concepts: Map<kId, BaseTopic>;
  /**
      tentative: this realm is an extension of another realm,
      and all that realm's concepts are in-scope for this realm.
   */
  backupRealms: Realm[];
  logStream: IEventStream;
  inboxStream: IEventStream;
  logStreamSpec: ?IEventStreamSpec
  inboxStreamSpec: ?IEventStreamSpec
  lastProcessedEvents: Map<kRealmId, kId>;
  logStreamCallbacks: IEventCallbackFn[];
  inboxStreamCallbacks: IEventCallbackFn[];
  // caches
  /**
      global cache for relations of a concept, by concept.id.
      If the relation and concept exist in different realms,
      the cache will be held in the concept's realm, so the
      cache may contain relations belonging to other realms.
   */
  relationCache: Map<kId, ?Set<IRelation>>;
  /**
      cache: reverse of Concept.sourceRealmIds, indexed by realm
   */
  remoteToLocalIdCache: Map<kRealmId, Map<kId, kId>>;
  sourceRealmEventIds: Map<kId, kId>;
  static inherentTypeName = "Realm";

  constructor(id: kRealmId, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec, baseRealm?: Realm) {
    super(id, ((null: any): Realm));
    this.realm = this;
    this.logStreamSpec = logStreamSpec
    this.inboxStreamSpec = inboxStreamSpec
    this.baseRealm = baseRealm
    this.logStreamCallbacks = []
    this.inboxStreamCallbacks = []
    this.resetRealm();
  }
  async init(context?: JLDContext) {
    if (this.context)
      throw new Error("Realm initialized twice")
    // Do I want to recreate the event stream from a revive? Only locally
    if (this.logStreamSpec) {
      const stream: ?IEventStream = await RealmRegistry.getEventStream(this.logStreamSpec)
      if (!stream)
        throw new Error(`missing stream: {JSON.stringify(this.logStreamSpec)}`)
      this.logStream = stream
    }
    if (this.inboxStreamSpec) {
      const stream: ?IEventStream = await RealmRegistry.getEventStream(this.inboxStreamSpec)
      if (!stream)
        throw new Error(`missing stream: {JSON.stringify(this.inboxStreamSpec)}`)
      this.inboxStream = stream
      this.inboxStream.setCallback(async (ev: Object) => {
        // Clear ID from inbox
        ev['@id'] = null
        const event = await this.realm.revive(ev)
        if (event && event instanceof BaseEvent) {
          await this.applyCommand(event)
          for (const cb of this.inboxStreamCallbacks)
            cb(event)
        }
      })
      this.logStream.setCallback(async (ev: Object) => {
        if (this.logStreamCallbacks.length) {
          const event = await this.eventById(ev['@id'])
          if (event)
            for (const cb of this.logStreamCallbacks)
              cb(event)
        }
      })
    }
    if (context) {
      this.context = context
    } else {
      const baseRealm = this.baseRealm || RealmRegistry.rootRealm;
      // hhmmm... overriding inherited?
      this.context = this.makeContext(baseRealm.context)
    }
    if (!this.context.context) {
      await this.context.init()
    }
    await this.ensureTypeCache()
    for await (const event: BaseEvent of this.eventsStartingFrom()) {
      await this.applyEvent(event);
    }
  }

  makeContext(baseContext: JLDContext): JLDContext {
    const base: Object = {'@base': this.id}
    if (this.logStreamSpec) {
      base.ev = {'@id':this.logStreamSpec.url, '@prefix':true}
    }
    if (this.inboxStreamSpec) {
      base.inbox = {'@id':this.inboxStreamSpec.url, '@prefix':true}
    }
    return new JLDContext({'@context': base}, this.contextUrl, this.id, baseContext);
  }

  toJSON(key: ?string, ctx: ?JLDContext): any {
    ctx = ctx || this.context
    if (key) return this.id;
    const result = super.toJSON(key, ctx);
    // no compaction in this case
    result['@id'] = this.id
    result['@context'] = this.contextUrl || this.context.asContext(true)
    if (this.logStreamSpec)
      result.logStreamSpec = this.logStreamSpec
    if (this.inboxStreamSpec)
      result.inboxStreamSpec = this.inboxStreamSpec
    if (this.backupRealms.length)
      result.backupRealms = this.backupRealms.map(r => r.id);
    // TODO: a capability to the event stream.
    // The concepts will derive.
    return result;
  }
  wrapJSONLD(json: Object | Array<any>, ctx_in_json?: boolean): Object {
    if (!Array.isArray(json))
      json = [json]
    const r: Object = {
      '@id': this.id,
      '@graph': json
    }
    if (ctx_in_json) {
      r['@context'] = this.contextUrl || this.context.asContext(true)
    }
    return r
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<Realm> {
    const result = new this(json["@id"], json.logStreamSpec, json.inboxStreamSpec)
    return await result.reviverPhase2(json, ctx);
  }

  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    if (json['@context']) {
      this.context = new JLDContext(json['@context'])
      await this.context.init()
      if (this.context.url == base_jsonld_url) {
        // Single-layer context is probably base context.
        // TODO: Think through all the real cases.
        this.context = this.makeContext(this.context)
        await this.context.init()
      }
    }
    // TODO: Get baseContext
    await super.reviverPhase2(json, ctx);
    if (json.backupRealms) {
      this.backupRealms = [];
      for (const rid: kRealmId of json.backupRealms) {
        const realm = await RealmRegistry.getRealm(rid);
        if (realm) this.backupRealms.push(realm);
      }
    }
    return this;
  }

  async topicJSON(ctx: ?JLDContext): Promise<Object[]> {
    ctx = ctx || this.context
    const result: Object[] = [];
    for (const c of this.concepts.values()) {
      if (c instanceof Concept || c instanceof SimpleRelation)
        result.push(await c.toJSON(null, ctx));
    }
    return result;
  }
  async eventsJSON(fromId: ?kId, ctx: ?JLDContext): Promise<Object[]> {
    ctx = ctx || this.context
    const result: Object[] = [];
    if (this.logStream) {
      for await (const ev: Object of this.logStream.eventsStartingFrom(fromId))
        result.push(ev);
    }
    return result;
  }
  addCallback(cb: IEventCallbackFn, forInbox: boolean) {
    if (forInbox) {
      this.inboxStreamCallbacks.push(cb)
    } else {
      this.logStreamCallbacks.push(cb)
    }
  }
  localizeId(remoteConceptId: kId, remoteRealmId: kRealmId): ?kId {
    const remoteToLocalId = this.remoteToLocalIdCache.get(remoteRealmId);
    if (remoteToLocalId) {
      const localId = remoteToLocalId.get(remoteConceptId);
      if (localId) {
        return localId;
      }
    }
  }

  localizeRef(ref: TopicXRef): ?kId {
    return this.localizeId(ref.id, ref.realmId)
  }

  async localize(remoteTopic: BaseTopic): Promise<?BaseTopic> {
    if (remoteTopic.realm == this) return remoteTopic;
    const localId = this.localizeId(remoteTopic.id, remoteTopic.realm.id);
    if (localId) return await this.byId(localId);
    // maybe it's a global Id.. not sure about this. But should fail in most cases.
    // return await this.byId(remoteTopic.id);
  }
  registerRemoteId(realmId: kRealmId, localId: kId, remoteId: kId) {
    let map = this.remoteToLocalIdCache.get(realmId);
    if (!map) {
      map = new Map();
      this.remoteToLocalIdCache.set(realmId, map);
    }
    map.set(remoteId, localId);
  }

  reset() {
    this.resetRealm();
  }
  resetRealm() {
    this.concepts = new Map();
    this.relationCache = new Map();
    this.remoteToLocalIdCache = new Map();
    this.backupRealms = [];
    this.sourceRealmEventIds = new Map();
    this.lastProcessedEvents = new Map();
    //this.resetEvents();
  }

  async resetEvents(until_event: ?kId): Promise<void> {
    if (this.isResettable()) await this.logStream.resetEvents(until_event);
  }
  isResettable(): boolean {
    return (this.logStream && this.logStream.isResettable())
  }

  expandIri(iri: kName): kId {
    return this.context.expandIri(iri)
  }
  compactIri(iri: kId): kName | kId {
    return this.context.compactIri(iri)
  }
  async byId(id: kId): Promise<?BaseTopic> {
    let r: ?BaseTopic = this.concepts.get(id);
    if (r) return r;
    if (this.baseRealm) {
      r = await this.baseRealm.byId(id)
      if (r) return r
    } else if (this.baseRealm !== RealmRegistry.rootRealm)
      return await RealmRegistry.rootRealm.byId(id)
  }

  byIdCached(id: kId): ?BaseTopic {
    let r: ?BaseTopic = this.concepts.get(id);
    if (r) return r;
    if (this.baseRealm) {
      r = this.baseRealm.byIdCached(id)
      if (r) return r
    } else if (this.baseRealm !== RealmRegistry.rootRealm)
      return RealmRegistry.rootRealm.byIdCached(id)
  }
  /**
  Get a concept using a local human-readable name.
  Notably used for types in json-ld.
  Do not attempt to name all concepts.
  */
  async byName(name: kName): Promise<?BaseTopic> {
    return await this.byId(this.context.expandIri(name))
  }
  /**
  Get a concept using a local human-readable name.
  Notably used for types in json-ld.
  Do not attempt to name all concepts.
  */
  byNameCached(name: kName): ?BaseTopic {
    return this.byIdCached(this.context.expandIri(name))
  }
  /**
  Cache the relations globally.
  */
  relationsOfTopic(topicId: kId): ?Set<IRelation> {
    return this.relationCache.get(topicId);
  }
  deriveEvent(event: BaseEvent): ?BaseEvent {
    // TODO
  }
  /**
  replace a node by another. May come from a same-realm recast, or a cross-realm import.
  */
  replace(targetNode: BaseTopic, originalId: ?kId, originalRealm: ?Realm) {
    if (targetNode.realm != this) {
      throw new Error("replace must be called on node's realm");
    }
    originalId = originalId || targetNode.id;
    originalRealm = originalRealm || targetNode.realm;
    this.concepts.set(targetNode.id, targetNode);
    // not certain in what case the name would change...
    if (targetNode instanceof MetaConcept && targetNode.name) {
      this.context.addTerm(targetNode.name, targetNode.id);
    }
    let relCache: Set<IRelation>;
    if (originalRealm == this) {
      relCache = this.relationCache.get(originalId) || new Set();
    } else {
      // make as if affected by same relations
      relCache = new Set(originalRealm.relationCache.get(originalId));
    }
    this.relationCache.set(targetNode.id, relCache);
    for (const rel of relCache) {
      rel.replace(targetNode, originalId, originalRealm);
    }
    // new cases? esp. bindings.
  }

  createId(): kId {
    throw new Error("Abstract");
  }

  async recast(newType: ?MetaConcept): Promise<BaseTopic> {
    throw new Error("Cannot recast realms");
  }
  async appendEvent(event: BaseEvent): Promise<kId> {
    if (!this.logStream)
      throw new Error("Cannot append without a stream")
    // TODO: Change this!
    if (event._fromCommand) {
      // make sure fromCommand is defined at this point
      await event._fromCommand.waitForId()
    }
    const eventJson = JSON.parse(JSON.stringify(event))
    const idPromise: Promise<kId> = this.logStream.appendEvent(eventJson);
    event.idPromise = idPromise
    idPromise.then((id) => {event.id = id; event.idPromise = undefined})
    return idPromise
  }
  async shutdown(): Promise<void> {
      if (this.logStream) await this.logStream.shutdown();
  }
  async *eventsStartingFrom(
    eventId: ?kId
  ): AsyncGenerator<BaseEvent, void, void> {
    if (this.logStream) {
      for await (let event: Object of this.logStream.eventsStartingFrom(eventId)) {
        if (event && !(event instanceof BaseEvent)) {
          if (event["@type"])
            event = await this.revive(event)
          else {
            console.error(event)
            debugger
          }
        }
        if (event) yield event
      }
    }
  }

  async nextEventId(): Promise<?kId> {
    if (this.logStream) return await this.logStream.nextEventId();
  }
  async eventById(id: kId | kName): Promise<?BaseEvent> {
    // console.log(`eventById: ${id} -> ${this.expandIri(id)}`)
    if (this.logStream) {
      let event = await this.logStream.eventById(this.expandIri(id));
      if (event && !(event instanceof BaseEvent)) {
          if (event["@type"])
            event = await this.revive(event)
          else {
            console.error(event)
            debugger
            return null
          }
      }
      return event
    }
  }

  async applyEvent(command: BaseEvent): Promise<void> {
    throw new Error("abstract")
  }

  async applyCommand(command: BaseEvent, replaying?: boolean): Promise<BaseEvent[]> {
    // TODO: Make this fail atomically. Maybe use Redux. Then resetEvents to currentId.
    const currentId = await this.nextEventId();
    await this.appendEvent(command);
    let events: BaseEvent[] = [];
    let subevents: BaseEvent[] = [];
    try {
      if (!replaying) {
        const gen = this.expandCommand(command);
        let notDone = true;
        while (notDone) {
          const r = await gen.next(subevents);
          if (r.value) {
            const subcommand: BaseEvent = r.value;
            events.push(subcommand)
            subevents = await this.applyCommand(subcommand);
            if (hasError(subevents.values()))
              return events;
          }
          notDone = !r.done;
        }
      }
      if (hasError(events.values())) {
        return events;
      }
      await this.applyEvent(command);
    } catch (exception) {
      console.error(exception);
      const event = new ErrorEvent(command, exception.message);
      events.push(event)
      await this.appendEvent(event);
      // better would be the ability to resetEvents...
      command.addEvent(event)
    }
    // console.log("applyCommand "+command.inherentTypeNameSafe+'->'+JSON.stringify(await command.events()))
    return events;
  }
  async applySimpleCommand(command: BaseEvent): Promise<BaseEvent[]> {
    // or return an error?
    throw new Error("Generic processes do not apply simple commands");
  }
  static async realmInJsonLd(json: Object, contextUrl?: string): Promise<?Realm> {
    if (json['@graph'] && json['@id']) {
      return await RealmRegistry.getRealm(json['@id']);
    }
    const ctxData = json['@context'] || contextUrl
    if (ctxData) {
      const context = new JLDContext(ctxData)
      await context.init()
      if (context.base)
        return await RealmRegistry.getRealm(context.base);
    }
  }
  async revive(json: Object, context: ?JLDContext): Promise<any> {
    // console.log('revive:'+JSON.stringify(json)+' in '+((context)?(context.base || ''):'no ctx'))
    if (Array.isArray(json)) {
      const result = [];
      for (const el of json) {
        const rev = await this.revive(el, context);
        result.push(rev);
      }
      return result;
    }
    if (json === null) return null;
    if (typeof json === "object") {
      if (json['@context']) {
        // TODO: Shortcut if realm's context
        context = new JLDContext(json['@context'])
        await context.init()
      }
      if (json['@graph']) {
        if (json['@id'] && json['@id'] != this.id) {
          const realm = await Realm.realmInJsonLd(json)
          if (!realm)
            throw new Error("Unknown realm: "+json['@id'])
          return await realm.revive(json, context)
        }
        return this.revive(json['@graph'], context)
      }
      // passed-in context has priority
      context = context || this.context
      if (json['@type']) {
        let types = json['@type']
        if (!Array.isArray(types)) types = [types]
        const possibleClasses: Array<Class<AbstractBase>> = []
        for (const typeName of types) {
          const fullName = this.expandIri(typeName)
          let reviverCls = RealmRegistry.revivers.get(fullName)
          if (reviverCls) {
            // shortcut
            possibleClasses.push(reviverCls)
          } else {
            const type = await this.byId(fullName)
            // console.log(type)
            if (type && type instanceof MetaConcept) {
              for (const cls of type.superTypes()) {
                reviverCls = RealmRegistry.revivers.get(cls.id)
                if (reviverCls) {
                  possibleClasses.push(reviverCls)
                  break
                }
              }
            }
          }
        }
        let possibleClasses2: Array<Class<AbstractBase>> = []
        for (const cls1 of possibleClasses) {
          let found = false
          for (const cls2 of possibleClasses) {
            if (cls1 != cls2 && (cls2: any).prototype instanceof cls1) {
              found = true
              break
            }
          }
          if (!found)
            possibleClasses2.push(cls1)
        }
        if (possibleClasses2.length > 1) {
          console.warn(possibleClasses2.map(x=>x.name).join(', '))
        }
        // console.log(possibleClasses2.map(x=>x.name).join(', '))
        if (possibleClasses2.length > 0) {
          const cls = possibleClasses2[0]
          let realm = this;
          if (json.realm) {
            const realmId = context.expandIri(json.realm)
            if (realmId != this.id)
              realm = await RealmRegistry.getRealm(realmId);
          }
          if (realm) {
            return await cls.reviver(json, realm, context);
          }
        }
      }
    }
    return json;
  }
}

/**
The realm containing the bootstrap data.
TODO: Read from a RDF file.
*/
export class RootRealm extends Realm {
  constructor(context: JLDContext, vocabulary: Object) {
    super(context.expandIri('rootRealm'));
    this.context = context
    for (const [name: kName, id0] of Object.entries(vocabulary)) {
      const id: kId = ((id0: any): kId);
      const concept = new MetaConcept(id, this);
      concept.name = name;
      this.concepts.set(id, concept);
    }
    const sourceType = this.byIdCached(vocabulary.source);
    const destType = this.byIdCached(vocabulary.target);
    if (!(sourceType && sourceType instanceof MetaConcept)) throw new Error();
    if (!(destType && destType instanceof MetaConcept)) throw new Error();
    for (const [relName: kName, rels] of Object.entries(baseRelations)) {
      for (const [source: kName, target] of Object.entries((rels: Object))) {
        const relType = this.byNameCached(relName);
        if (!(relType && relType instanceof MetaConcept)) throw new Error();
        const sourceId: ?kId = this.context.expandIri(source);
        const destId: ?kId = this.context.expandIri(((target: any): kName));
        if (!sourceId || !destId){
          console.error("missing something")
          throw new Error();
        }
        // console.log(`${sourceId} ${relName} ${destId}`)
        const rel_id: kId = `${sourceId}_${relName}`;
        const rel = new SimpleRelation(rel_id, this, relType);
        this.concepts.set(rel_id, rel);
        rel.source = new RoleBinding(
          rel_id + "_S",
          rel,
          sourceType,
          new TopicXRef(sourceId, this)
        );
        this.concepts.set(rel.source.id, rel.source);
        rel.target = new RoleBinding(rel_id + "_D", rel, destType, new TopicXRef(destId, this))
        this.concepts.set(rel.target.id, rel.target);
        let cache = this.relationCache.get(destId)
        if (!cache) {
          cache = new Set()
          this.relationCache.set(destId, cache)
        }
        cache.add(rel)
        cache = this.relationCache.get(sourceId)
        if (!cache) {
          cache = new Set()
          this.relationCache.set(sourceId, cache)
        }
        cache.add(rel)
      }
    }
  }
  async initCaches(): Promise<void> {
    for (const cpt of this.concepts.values()) {
      await cpt.ensureTypeCache()
    }
  }

  async byId(id: kId): Promise<?BaseTopic> {
    return this.concepts.get(id);
  }

  byIdCached(id: kId): ?BaseTopic {
    return this.concepts.get(id);
  }
}

/**
The singleton that manages Realm objects
*/
class RealmRegistryClass implements IRealmRegistry {
  realms: Map<kId, Realm>;
  vocabulary: { [key: kName]: kGlobalId };
  glob: { [key: kName]: MetaConcept };
  rootRealm: RootRealm;
  preRegistry: Array<Class<AbstractBase>>;
  revivers: Map<kName, Class<AbstractBase>>;
  inited: boolean;
  realmFactories: Array<IRealmFactoryFn>;
  eventStreamFactories: Map<kName, IEventStreamFactoryFn>;
  processes: Map<kId, IProcess>;
  streamRegistry: Map<string, IEventStream>

  constructor() {
    this.glob = {};
    this.revivers = new Map();
    this.realmFactories = [];
    this.eventStreamFactories = new Map()
    this.processes = new Map();
    this.streamRegistry = new Map()
    this.preRegistry = []
    this.reset();
    // TODO: Create a local realm.
  }
  reset(): void {
    this.realms = new Map();
    if (this.rootRealm)
      this.registerRealm(this.rootRealm)
  }
  setRootRealm(rootRealm: RootRealm) {
    this.rootRealm = rootRealm;
    this.registerRealm(this.rootRealm);
    for (const id of ((Object.values(this.vocabulary): any[]): kId[])) {
      const concept = rootRealm.byIdCached(id);
      if (concept && concept instanceof MetaConcept && concept.name) {
        this.glob[concept.name] = concept;
      }
    }
  }

  registerRealm(realm: Realm): void {
    this.realms.set(realm.id, realm);
  }

  async initBaseTypes(): Promise<void> {
    if (this.inited) return;
    const jldCtx = new JLDContext(base_jsonld_url)
    await jldCtx.init()
    this.vocabulary = {}
    for (const key: string of jldCtx.context.mappings.keys()) {
      if (key.charAt(0)=='@') continue;
      if (jldCtx.context.mappings.get(key)._prefix) continue;
      this.vocabulary[key] = jldCtx.expandIri(key);
    }
    this.setRootRealm(new RootRealm(jldCtx, this.vocabulary))
    // do not call init on rootRealm because ctx initialized
    for (const id: kId of ((Object.values(jldCtx): any[]): kId[])) {
      const concept = await this.rootRealm.byId(id);
      if (concept && concept instanceof Concept) {
        await concept.ensureTypeCache();
      }
    }
    await this.rootRealm.initCaches()
    this.registerRevivers([
      BaseTopic,
      SingleTypeTopic,
      BaseEvent,
      ErrorEvent,
      Attribute,
      RoleBinding,
      Concept,
      Relation,
      SimpleRelation,
      MetaConcept,
      Collection,
      VirtualProxy,
      Process,
      Realm,
      RootRealm,
    ])
    await this.doRegistration()
    this.inited = true;
  }

  /**
  Get the realm using its Id, which should always be a dereferencable URL
  with a JSON-LD representation. It should contain default (public)
  capabilities, or the registry should have extra capabilities.
  */
  async getRealm(id: kRealmId, allowCreation: ?boolean, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec): Promise<?Realm> {
    let realm: ?Realm = this.realms.get(id);
    if (realm) return realm;
    if (allowCreation === true || allowCreation === undefined) {
      for (const f of this.realmFactories) {
        realm = await f(id, logStreamSpec, inboxStreamSpec);
        if (realm) {
          this.realms.set(realm.id, realm);
          return realm;
        }
      }
    }
    return null
  }

  /**
  Get a MetaConcept declared globally from the appropriate (root) realm.
  */
  getGlobalType(id: kGlobalId): ?MetaConcept {
    const type = this.rootRealm.byIdCached(id);
    if (type && type instanceof MetaConcept) {
      return type;
    }
  }

  async getEventStream(spec: IEventStreamSpec, nocreate: ?boolean): Promise<?IEventStream> {
    let stream = this.streamRegistry.get(spec.name)
    if (stream && stream.spec.type != spec.type)
      throw new Error("stream conflict")
    if (!nocreate && !stream) {
      const factoryFn = this.eventStreamFactories.get(spec.type)
      if (factoryFn) {
        const stream = await factoryFn(spec, nocreate);
        if (stream) {
          this.streamRegistry.set(spec.name, stream)
          return stream
        }
      }
    }
    return stream
  }

  registerReviver(cls: Class<AbstractBase>): void {
    this.preRegistry.push(cls)
  }
  async doRegistration(): Promise<void> {
    while (this.preRegistry.length) {
      const cls = this.preRegistry.pop()
      const name = cls.inherentTypeName
      if (name && cls.hasOwnProperty("inherentTypeName")) {
        const id = this.rootRealm.expandIri(name)
        this.revivers.set(id, cls);
        const metaclass = await this.rootRealm.byName(name)
        if (metaclass && metaclass instanceof MetaConcept)
          cls._inherentType = metaclass
        else
          console.warn("Missing class: "+name)
      }
    }
  }

  registerRevivers(classes: Array<Class<AbstractBase>>): void {
    this.preRegistry = [...this.preRegistry, ...classes]
  }

  registerRealmFactory(f: IRealmFactoryFn): void {
    this.realmFactories.push(f);
  }
  registerEventStreamFactory(type: string, f: IEventStreamFactoryFn): void {
    this.eventStreamFactories.set(type, f);
  }
  registerProcess(process: IProcess): void {
    // make it last, because order matters
    if (this.processes.has(process.id)) {
      this.processes.delete(process.id);
    }
    this.processes.set(process.id, process);
  }

  getProcess(id: kId): ?IProcess {
    return this.processes.get(id);
  }
  getProcesses(): Array<IProcess> {
    return [...this.processes.values()];
  }

}

export var RealmRegistry = new RealmRegistryClass();
// RealmRegistry.initBaseTypes().then().catch();
