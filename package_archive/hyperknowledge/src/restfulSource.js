// @flow
import * as axios from "axios"
import type { kId, URLType, kAccessSpec, kName } from "./types";
import { BaseEvent, RealmRegistry, Realm, BaseTopic, AbstractBase } from "./baseTopics";
import type { IEventStream, IEventSource, IEventStreamSpec, IRawEventCallbackFn, ITopicSource, IProcess } from "./baseTopics";
import { JLDContext } from "./jsonld"

export function urlFromRestSpec(spec: kAccessSpec, baseUrl: URLType): URLType {
  if (spec.startsWith("rest:")) {
    return new URL(spec.substr(5), baseUrl).href;
  }
  // maybe spec already a URL?
  try {
    return new URL(spec).href;
  } catch (error) {}
  throw new Error("Malformed spec");
}

export class RestfulSource implements ITopicSource {
  spec: kAccessSpec;
  url: URLType;
  realm: Realm;

  constructor(realm: Realm, spec: kAccessSpec) {
    this.realm = realm;
    this.spec = spec;
    this.url = urlFromRestSpec(spec, realm.id);
  }

  async byId(id: kId): Promise<?BaseTopic> {
    const uri = (id.startsWith(this.url))? id : this.url
    const params = (uri===this.url)? '?id='+id : undefined
    try {
      const response = await axios.get(uri, {params})
      const ctx: ?JLDContext = await JLDContext.contextFromResponse(response)
      let result = await this.realm.revive(response.data, ctx)
      if (result) {
        if (Array.isArray(result)) result = result[0]
        return result
      }
    } catch (error) {
      console.warn(error)
    }
  }
}

export function topicSourceFactory(
  realm: Realm,
  spec: kAccessSpec
): ?ITopicSource {
  if (spec.startsWith("rest:")) {
    return new RestfulSource(realm, spec);
  }
}
export class RestEventStream implements IEventStream {
  static cacheSize = 16
  spec: IEventStreamSpec
  url: URLType
  lastId: ?kId
  lastNextId: ?kId
  cache: Map<kId, ?BaseTopic>
  callback: ?IRawEventCallbackFn

  constructor(spec: IEventStreamSpec) {
    this.spec = spec
    this.url = spec.data || spec.url
    this.cache = new Map()
  }

  setCallback(cb: IRawEventCallbackFn): void {
    this.callback = cb
    this.checkForNewEvents()
  }

  async checkForNewEvents() {
    if (!this.callback) return
    // TODO: Replace with websocket!
    const lastNextId = this.lastNextId || this.lastId
    const nextId = await this.nextEventId()
    if (nextId != lastNextId) {
      for await (const ev of this.eventsStartingFrom(lastNextId)) {
        if (this.callback)
          await this.callback(ev)
        else
          break
      }
    }
    if (this.callback)
      setTimeout(()=>{this.checkForNewEvents()}, 1000)
  }

  clearCallback(): void {
    this.callback = null
  }

  async exists(): Promise<boolean> {
    const response = await axios.head(this.url, {validateStatus:null})
    return response.status == 200
  }

  async create(nocheck: ?boolean): Promise<void> {
    if (!nocheck && this.exists())
      return
    const response = await axios.put(this.url)
  }

  async appendEvent(event: Object): Promise<kId> {
    const response = await axios.post(this.url, event);
    const lastId: kId = response.headers.location
    this.lastId = lastId
    if (!event['@id'])
      event['@id'] = lastId
    if (this.callback) {
      try {
        await this.callback(event)
      } catch (error) {
        console.error(error)
      }
    }
    return lastId
  }

  async nextEventId(): Promise<?kId> {
    const response = await axios.head(this.url);
    const linkHeader = response.headers.link
    if (linkHeader) {
      const matches = linkHeader.match(/<([^>]+)>; rel="nextId"/)
      if (matches) {
        this.lastNextId = matches[1]
        return matches[1]
      }
    }
  }

  async *eventsStartingFrom(
    eventId: ?kId
  ): AsyncGenerator<Object, void, void> {
    // TODO: use origin, paginate
    let params = null
    if (eventId) {
      if (eventId.startsWith(this.url))
        eventId = 'ev:'+eventId.substring(this.url.length)
      params = { from: eventId }
    }
    try {
      const response = await axios.get(this.url, { params });
      for (const ev of response.data) {
        this.lastId = ev['@id']
        yield ev
      }
    } catch (error) {
      console.warn(error)
    }
  }

  async getEventById(id: kId): Promise<?Object> {
    const uri = (id.startsWith(this.url))? id : this.url
    const params = (uri===this.url)? {id} : undefined
    try {
      const response = await axios.get(uri, {params});
      return response.data
    } catch (error) {
      console.warn(error)
    }
  }

  async eventById(id: kId): Promise<?Object> {
    let result: ?Object;
    if (this.cache.has(id)) {
      result = this.cache.get(id)
      this.cache.delete(id) // delete to reinsert as new
    } else {
      result = await this.getEventById(id)
    }
    this.cache.set(id, result)
    while (this.cache.size > RestEventStream.cacheSize) {
      // delete first, which is oldest
      for (const id of this.cache.keys()) {
        this.cache.delete(id)
        break
      }
    }
    return result
  }

  async resetEvents(until_event: ?kId): Promise<void> {
    if (until_event)
      throw new Error("Not resettable")
    else {
      await axios.delete(this.url)
    }
  }

  async shutdown(): Promise<void> { 
  }

  isResettable(): boolean {
    return false
  }

}

class RemoteProcess extends AbstractBase implements IProcess {
  static inherentTypeName = "RemoteProcess";
  spec: ?kAccessSpec;

  constructor(id: ?kId, realm: Realm, spec: kAccessSpec) {
    super(id, realm);
    this.spec = spec;
    if (!spec) throw new Error("spec required in remote process");
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("Abstract");
  }

  canExpand(): Set<kName> {
    return new Set();
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    return {
      '@type': RemoteProcess.inherentTypeName,
      id: this.id,
      spec: this.spec,
      realm: this.realm.id
    };
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<RemoteProcess> {
    if (json.spec) {
      const proc: ?IProcess = await remoteProcessFactory(
        json["@id"],
        realm,
        json.spec
      );
      if (proc && proc instanceof RemoteProcess) return proc;
    }
    throw new Error("malformed json");
  }
}

RealmRegistry.registerReviver(RemoteProcess);

class RestRemoteProcessClient extends RemoteProcess {
  url: URLType;
  canExpandSet: ?Set<kName>;

  constructor(id: ?kId, realm: Realm, spec: kAccessSpec) {
    super(id, realm, spec);
    this.url = urlFromRestSpec(spec, realm.id);
  }

  canExpand(): Set<kName> {
    if (!this.canExpandSet) throw new Error("not inited");
    return this.canExpandSet;
  }

  async init(): Promise<boolean> {
    const response = await axios.get(this.url);
    let json = response.data
    if (json['@graph']) json = json['@graph']
    if (Array.isArray(json)) json = json[0]
    const canExpandSet = new Set();
    this.canExpandSet = canExpandSet;
    const id: ?kId = json["@id"];
    if (!id || !json.expands || !Array.isArray(json.expands)) return false;
    this.id = id;
    this.canExpandSet = new Set(json.expands);
    // todo: check they're all strings.
    return this.canExpandSet.size > 0;
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const url: ?URLType = await this.start(this.realm, command);
    if (!url) return;
    let steps: ?(BaseEvent[]) = [];
    while (true) {
      try {
        const ev: ?BaseEvent = await this.next(url, steps);
        if (!ev) break;
        steps = yield ev;
      } catch (error) {
        await this.stop(url);
        throw error;
      }
    }
  }

  async start(realm: Realm, command: BaseEvent): Promise<?URLType> {
    const json = realm.wrapJSONLD(command.toJSON(null, realm.context), true)
    const response = await axios.post(this.url, json);
    return response.headers.location;
  }

  async next(uri: URLType, events: ?(BaseEvent[])): Promise<?BaseEvent> {
    const json = (events)?this.realm.wrapJSONLD(events, true):undefined
    const response = await axios.post(uri, json);
    let info: any = response.data;
    if (typeof info == "string") {
      info = JSON.parse(info);
    }
    if (info != null) {
      const ctx: ?JLDContext = await JLDContext.contextFromResponse(response)
      let result = await this.realm.revive(info, ctx);
      if (result) {
        if (result.length > 1)
          console.warn("more than one result:" + JSON.stringify(result))
        result = result[0]
        // override realm for events
        // hmmm... maybe not right. Who does the command belong to?
        // Maybe sender after all.
        // But it may also not be visible yet.
        // result.realm = this.realm
        if (result && result instanceof BaseEvent) return result;
      }
    }
  }

  async stop(uri: URLType): Promise<void> {
    await axios.post(uri, { stop: true });
  }
}

export async function eventStreamFactory(
  spec: IEventStreamSpec,
  nocreate: ?boolean
): Promise<?IEventStream> {
  if (spec.type=='rest') {
    const stream = new RestEventStream(spec);
    const exists: boolean = await stream.exists()
    if (!exists) {
      if (nocreate)
        return null
      else
        await stream.create(true)
    }
    return stream
  }
}


export async function remoteProcessFactory(
  id: ?kId,
  realm: Realm,
  spec: kAccessSpec
): Promise<?IProcess> {
  if (spec.startsWith("rest:")) {
    const r = new RestRemoteProcessClient(id, realm, spec);
    if (await r.init()) return r;
  }
}

RealmRegistry.registerEventStreamFactory('rest', eventStreamFactory)
