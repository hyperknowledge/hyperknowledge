// @flow
import type { kId, URLType } from "./types";
import { BaseEvent, RealmRegistry } from "./baseTopics";
import type { IEventStream, IEventStreamSpec, IRawEventCallbackFn } from "./baseTopics";

export class MemoryEventStream implements IEventStream {
  events: Object[];
  spec: IEventStreamSpec
  callback: ?IRawEventCallbackFn

  constructor(spec: IEventStreamSpec) {
    this.spec = spec
    this.resetEvents();
  }
  setCallback(cb: IRawEventCallbackFn): void {
    this.callback = cb
  }

  clearCallback(): void {
    this.callback = null
  }

  async appendEvent(event: Object): Promise<kId> {
    if (!event['@id']) {
      // simplification. Could also be content-addressable.
      const id = this.spec.url + this.events.length;
      event['@id'] = id
    }
    this.events.push(event);
    if (this.callback) {
      try {
        await this.callback(event)
      } catch (error) {
        console.error(error)
      }
    }
    return event['@id']
  }

  async nextEventId(): Promise<?kId> {
    return this.spec.url + this.events.length;
  }

  async *eventsStartingFrom(
    eventId: ?kId
  ): AsyncGenerator<Object, void, void> {
    // TODO parse the id
    let found: boolean = eventId === undefined;
    for (const event of this.events) {
      if (!found && event['@id'] == eventId) {
        found = true;
      }
      if (found) {
        yield event;
      }
    }
  }
  async eventById(id: kId): Promise<?Object> {
    for (const event: Object of this.events) {
      if (event['@id'] == id) return event;
    }
  }

  async resetEvents(until_event: ?kId): Promise<void> {
    if (until_event) {
      for (let i = 0; i < this.events.length; i++) {
        if (this.events[i]['@id'] == until_event) {
          this.events.splice(i + 1);
          return;
        }
      }
    }
    this.events = [];
  }
  async shutdown(): Promise<void> { 
  }
  isResettable(): boolean {
    return true
  }

}


RealmRegistry.registerEventStreamFactory('mem', async (spec: IEventStreamSpec, nocreate: ?boolean)=>new MemoryEventStream(spec))
