// @flow
import * as axios from "axios"

import {
  type kId,
  type kGlobalId,
  type kName,
  type URLType,
  type kRealmId,
  type kAccessSpec
} from "./types";
import { chain } from "./utils";
import {
  BaseTopic,
  TopicXRef,
  Concept,
  MetaConcept,
  Realm,
  RealmRegistry,
  Attribute,
  ErrorEvent,
  BaseEvent,
  RoleBinding,
  SimpleRelation,
  type IRelation,
  Relation,
  Process,
  SingleTypeTopic,
  hasError,
  AbstractBase,
  type ITopicSource,
  type IEventStreamSpec,
  type IEventSource,
  type IEventSink,
  type IProcess
} from "./baseTopics"
import { RestfulSource, RestEventStream, urlFromRestSpec, remoteProcessFactory, topicSourceFactory } from "./restfulSource"
import { MemoryEventStream } from "./eventStream"
import {
  CreateTopicEvent,
  DeleteTopicEvent,
  ImportTopicEvent,
  AddAttributeEvent,
  AddRoleTargetEvent,
  AddToCollectionEvent,
  EnterRelationEvent,
  ExitRelationEvent,
  RemoveFromCollectionEvent,
  RejectEvent,
  extractTargetId,
  TranscludeTopicEvent,
  CreateTopicCommand,
  DeleteTopicCommand,
  ImportTopicCommand,
  AddAttributeCommand,
  AddRoleTargetCommand,
  AddToCollectionCommand,
  RemoveFromCollectionCommand,
  SplitConceptCommand,
  MergeConceptsCommand,
  UnmergeConceptCommand,
  TranscludeTopicACommand
} from "./baseEvents";
import { JLDContext } from "./jsonld"

/**
A Concrete Realm handles events
*/
export class ConcreteRealm extends Realm {
  static inherentTypeName = "Realm";

  constructor(id: kRealmId, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec) {
    super(id, logStreamSpec, inboxStreamSpec)
    this.contextUrl = id + "context"
    const rootRealm: ?Realm = RealmRegistry.rootRealm;
    if (rootRealm) {
      this.backupRealms.push(rootRealm);
    }
  }
  async applyEvent(event: BaseEvent): Promise<void> {
    if (event instanceof RejectEvent) return
    if (event instanceof ErrorEvent) return
    if (event.realm != this) {
      // should we adapt the event in this case?
      throw new Error(`Wrong realm: ${JSON.stringify(event)} (${typeof(event)}) comes from ${(event.realm)?event.realm.id:'missing'}`)
    }
    // console.log("applyEvent:"+JSON.stringify(event))
    let gen: ?AsyncGenerator<BaseEvent, void, BaseEvent[]> = null;
    switch (event.inherentTypeNameSafe) {
      // do not expand events
      case CreateTopicEvent.inherentTypeNameSafe:
        return await this.applyCreateTopicEvent(
          ((event: any): CreateTopicEvent))
      case DeleteTopicEvent.inherentTypeNameSafe:
        return await this.applyDeleteTopicEvent(
          ((event: any): DeleteTopicEvent))
      case ImportTopicEvent.inherentTypeNameSafe:
        return await this.applyImportTopicEvent(
          ((event: any): ImportTopicEvent))
      case TranscludeTopicEvent.inherentTypeNameSafe:
        return await this.applyTranscludeTopicEvent(
          ((event: any): TranscludeTopicEvent))
      case AddAttributeEvent.inherentTypeNameSafe:
        return await this.applyAddAttributeEvent(
          ((event: any): AddAttributeEvent))
      case AddRoleTargetEvent.inherentTypeNameSafe:
        return await this.applyAddRoleTargetEvent(
          ((event: any): AddRoleTargetEvent))
      case AddToCollectionEvent.inherentTypeNameSafe:
        return await this.applyAddToCollectionEvent(
          ((event: any): AddToCollectionEvent))
      case RemoveFromCollectionEvent.inherentTypeNameSafe:
        return await this.applyRemoveFromCollectionEvent(
          ((event: any): RemoveFromCollectionEvent))
      case EnterRelationEvent.inherentTypeNameSafe:
        return await this.applyEnterRelationEvent(
          ((event: any): EnterRelationEvent))
      case ExitRelationEvent.inherentTypeNameSafe:
        return await this.applyExitRelationEvent(
          ((event: any): ExitRelationEvent))
      case ErrorEvent.inherentTypeNameSafe:
      case RejectEvent.inherentTypeNameSafe:
        return
    }
    console.warn("applying unknown command:" + event.inherentTypeNameSafe)
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    if (command instanceof RejectEvent) return
    if (command instanceof ErrorEvent) return
    if (command.realm != this) {
      // should we adapt the event in this case?
      yield new ErrorEvent(this, `Wrong realm: ${JSON.stringify(command)} comes from ${command.realm.id}`)
      return
    }
    let gen: ?AsyncGenerator<BaseEvent, void, BaseEvent[]> = null;
    try {
      switch (command.inherentTypeNameSafe) {
        // do not expand events
        case CreateTopicEvent.inherentTypeNameSafe:
        case DeleteTopicEvent.inherentTypeNameSafe:
        case ImportTopicEvent.inherentTypeNameSafe:
        case TranscludeTopicEvent.inherentTypeNameSafe:
        case AddAttributeEvent.inherentTypeNameSafe:
        case AddRoleTargetEvent.inherentTypeNameSafe:
        case AddToCollectionEvent.inherentTypeNameSafe:
        case RemoveFromCollectionEvent.inherentTypeNameSafe:
        case EnterRelationEvent.inherentTypeNameSafe:
        case ExitRelationEvent.inherentTypeNameSafe:
        case RejectEvent.inherentTypeNameSafe:
        case ErrorEvent.inherentTypeNameSafe:
          return
        case CreateTopicCommand.inherentTypeName:
          gen = this.expandCreateTopicCommand(
            ((command: any): CreateTopicCommand)
          );
        break;
        case DeleteTopicCommand.inherentTypeName:
          gen = this.expandDeleteTopicCommand(
            ((command: any): DeleteTopicCommand)
          );
        break;
        case ImportTopicCommand.inherentTypeName:
          gen = this.expandImportTopicCommand(
            ((command: any): ImportTopicCommand)
          );
        break;
        case TranscludeTopicACommand.inherentTypeName:
          gen = this.expandTranscludeTopicCommand(
            ((command: any): TranscludeTopicACommand)
          );
        break;
        case AddAttributeCommand.inherentTypeName:
          gen = this.expandAddAttributeCommand(
            ((command: any): AddAttributeCommand)
          );
        break;
        case AddRoleTargetCommand.inherentTypeName:
          gen = this.expandAddRoleTargetCommand(
            ((command: any): AddRoleTargetCommand)
          );
        break;
        case AddToCollectionCommand.inherentTypeName:
          gen = this.expandAddToCollectionCommand(
            ((command: any): AddToCollectionCommand)
          );
        break;
        case RemoveFromCollectionCommand.inherentTypeName:
          gen = this.expandRemoveFromCollectionCommand(
            ((command: any): RemoveFromCollectionCommand)
          );
        break;
        case SplitConceptCommand.inherentTypeName:
          gen = this.expandSplitConceptCommand(
            ((command: any): SplitConceptCommand)
          );
        break;
        case MergeConceptsCommand.inherentTypeName:
          gen = this.expandMergeConceptsCommand(
            ((command: any): MergeConceptsCommand)
          );
        break;
        case UnmergeConceptCommand.inherentTypeName:
          gen = this.expandUnmergeConceptCommand(
            ((command: any): UnmergeConceptCommand)
          );
        break;
      }
    } catch (error) {
      if (command.hasEvents()) {
        const events: BaseEvent[] = await command.events();
        events.reverse();
        for (const ev of events) {
          const reverse: ?BaseEvent = await this.reverseSimpleEvent(ev);
          if (reverse) {
            // events.push(reverse)
            try {
              const revents = await this.applyCommand(command);
              for (const rev of revents) {
                yield rev
              }
            } catch (err2) {
              yield new ErrorEvent(command, err2);
              break;
            }
          }
        }
      }
      return;
    }
    if (gen) {
      let events: BaseEvent[] = []
      for await (const ev: BaseEvent of gen) {
        events = yield ev
      }
      return
    } else {
      // maybe someone below knows?
      yield* super.expandCommand(command)
    }
    if (!command.hasEvents()) {
      //yield new ErrorEvent(command, "Unknow command: "+command.inherentTypeNameSafe);
      console.warn("Unknow command: "+command.inherentTypeNameSafe)
    }
  }

  async *expandCreateTopicCommand(command: CreateTopicCommand): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const conceptId = this.createId()
    let events = yield new CreateTopicEvent(command, conceptId)
    if (events && hasError(events.values())) return;
    const baseTypeRef = command.baseType
    if (baseTypeRef) {
      const type = await baseTypeRef.resolve()
      if (type && type instanceof MetaConcept) {
        if (type.isSubtypeOf(RealmRegistry.glob.Relation)) {
          // type handled by inherent type
        } else if (type.id == RealmRegistry.glob.Concept.id) {
          // again, implicit
        } else {
          // add the type relation
          const instanceTypeRef = RealmRegistry.glob.instanceOf.asTopicXRef()
          let subcommand = new CreateTopicCommand(command, instanceTypeRef)
          events = yield subcommand
          if (events && hasError(events.values())) return;
          const typeRelId: ?kId = await extractTargetId(subcommand);
          if (!typeRelId) {
            yield new ErrorEvent(subcommand, "could not create type relation")
            return;
          }
          subcommand = new AddRoleTargetCommand(
            command,
            typeRelId,
            new TopicXRef(
              RealmRegistry.vocabulary.source,
              RealmRegistry.vocabulary.rootRealm),
            new TopicXRef(conceptId, command.realm)
          );
          events = yield subcommand;
          if (events && hasError(events.values())) return;
          subcommand = new AddRoleTargetCommand(
            command,
            typeRelId,
            new TopicXRef(
              RealmRegistry.vocabulary.target,
              RealmRegistry.vocabulary.rootRealm),
            type.asTopicXRef()
          );
          events = yield subcommand;
          if (events && hasError(events.values())) return;
        }
      }
    }
  }

  async applyCreateTopicEvent(event: CreateTopicEvent): Promise<void> {
    const command = event.fromCommand
    const baseTypeRef = command.baseType
    let type: ?MetaConcept
    if (baseTypeRef) {
      const typeT = await baseTypeRef.resolve()
      if (typeT && typeT instanceof MetaConcept)
        type = typeT
    }
    let concept: ?BaseTopic
    if (type) {
      if (type.isSubtypeOf(RealmRegistry.glob.Relation)) {
        concept = new SimpleRelation(event.targetId, this, type)
      } else if (type.isSubtypeOf(RealmRegistry.glob.Class)) {
        concept = new MetaConcept(event.targetId, this)
      } else if (type.isSubtypeOf(RealmRegistry.glob.AttributeBinding)
          || type.isSubtypeOf(RealmRegistry.glob.RoleBinding)
          || type.isSubtypeOf(RealmRegistry.glob.BaseEvent)
          ) {
            throw new Error("create those topics with the appropriate events")
      }
    }
    if (!concept)
      concept = new Concept(event.targetId, this);
    // what if the concept exists?
    this.concepts.set(concept.id, concept)

    await concept.ensureTypeCache()
  }

  async *expandDeleteTopicCommand(command: DeleteTopicCommand): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    // TODO: What does it mean to delete a topic which exists in a backup realm?
    // Am I masking the topic? YES.
    const topic: ?BaseTopic = this.concepts.get(command.topicId);
    if (!topic) throw new Error("Concept does not exist");
    if (topic instanceof Attribute) {
      yield *this.deleteAttribute(command, topic);
    } else if (topic instanceof RoleBinding) {
      yield *this.deleteRoleBindingCommand(command, topic);
    } else if (topic instanceof Concept || topic instanceof SimpleRelation) {
      yield *this.deleteConceptCommand(command, topic);
    } else if (topic instanceof BaseEvent) {
      throw new Error("cannot delete event");
    } else {
      throw new Error("what is this?");
    }
  }

  async *deleteAttribute(command: DeleteTopicCommand, topic: Attribute): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO")
  }

  async *deleteConceptCommand(command: DeleteTopicCommand, topic: BaseTopic): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    if (topic instanceof Relation || topic instanceof SimpleRelation) {
      for (let binding: RoleBinding of topic.bindings()) {
        yield new DeleteTopicEvent(command, binding.id, topic.id)
        if (binding.actorRef.realmId == this.id) {
          const actor = await binding.actor()
          if (actor)
            yield new ExitRelationEvent(command, binding.id, actor.keyTopicId(this))
          // should I throw otherwise?
        }
      }
    } else if (topic.realm != this)
      throw new Error("do not delete remote concepts");
    const relations = topic.inRelations();
    if (relations) {
      // Just a thought: if this is a local copy, should we revert to remote copy?
      // should that be a separate operation? I think we need an "unimport."
      for await (let rel: IRelation of relations) {
        if (rel instanceof Relation) {
          for (let [
            role: MetaConcept,
            bindings: RoleBinding[]
          ] of rel.bindingsMap()) {
            for (const binding of bindings) {
              if (binding.actorRef.id == topic.id && binding.actorRef.realmId == this.id)
                yield new DeleteTopicEvent(command, binding.id, rel.id)
            }
          }
          // TODO: Check if relation should be deleted
        } else if (rel instanceof SimpleRelation) {
          const deleteIt = (
            (
              rel.source &&
              rel.source.actorRef.id == topic.id &&
              rel.source.actorRef.realmId == this.id
            ) || (
              rel.target &&
              rel.target.actorRef.id == topic.id &&
              rel.target.actorRef.realmId == this.id
            ))
          if (deleteIt) {
            if (rel.source)
              yield new DeleteTopicEvent(command, rel.id+"_S", rel.id)
            if (rel.target)
              yield new DeleteTopicEvent(command, rel.id+"_T", rel.id)
            yield new DeleteTopicEvent(command, rel.id, rel.id)
          }
        }
      }
    }
    yield new DeleteTopicEvent(command, topic.id, topic.id);
  }

  async applyDeleteTopicEvent(event: DeleteTopicEvent): Promise<void> {
    const topic: ?BaseTopic = this.concepts.get(event.topicId);
    if (!topic) throw new Error("Concept does not exist");
    if (topic instanceof Relation || topic instanceof SimpleRelation) {
      // sanity check
      for (let binding: RoleBinding of topic.bindings())
        throw new Error("deleting non-empty relation")
    }
    const relations = topic.inRelations();
    if (relations) {
      const rel = await relations.next()
      if (rel)
        throw new Error("topic still in relations")
    }
    if (topic instanceof RoleBinding) {
      await this.deleteRoleBindingEvent(event, topic)
      // what follows should be part of deleteRoleBindingEvent. Do check.
      const relation: ?BaseTopic = await event.realm.byId(event.targetId)
      if (relation) {
        const role = topic.type
        if (relation instanceof Relation) {
          let found = false
          const bindings = relation._bindings.get(role)
          if (bindings) {
            for (let i = 0; i < bindings.length; i++) {
              const aBinding = bindings[i]
              if (aBinding == topic) {
                if (bindings.length) {
                  const new_bindings = bindings.splice(i,1)
                  relation._bindings.set(role, new_bindings);
                } else {
                  relation._bindings.delete(role);
                }
                found = true
              }
              break
            }
          }
          if (!found) {
            throw new Error("Could not delete binding")
          }
        } else if (relation instanceof SimpleRelation) {
          if (role.hasType(RealmRegistry.glob.source)) {
            relation.source = null
          }
          else if (role.hasType(RealmRegistry.glob.target)) {
            relation.target = null
          } else {
            throw new Error("Unknown binding to delete")
          }
        }
      }
    }
    if (topic instanceof Attribute) {
      const target: ?BaseTopic = await event.realm.byId(event.topicId)
      throw new Error("TODO")
    }
    // Just a thought: if this is a local copy, should we revert to remote copy?
    // should that be a separate operation? I think we need an "unimport."
    if (topic instanceof MetaConcept && topic.name) {
      this.context.removeTerm(topic.name);
    }
    this.concepts.delete(topic.id);
    // TODO: How do I know if a non-MetaConcept has a kName?
  }

  async *deleteRoleBindingCommand(command: DeleteTopicCommand, binding: RoleBinding): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    let relation: IRelation = binding.inRelation;
    // special case: foreign binding.
    if (binding.realm != this.realm && relation.realm != this.realm) {
        const localRelation = await this.realm.localize(relation)
        if (!localRelation)
          throw new Error("localize the relation before editing its bindigs")
    }
    yield new DeleteTopicEvent(command, binding.id, binding.keyTopicId(this));
    if (binding.realm == this)
      yield new ExitRelationEvent(command, binding.id, binding.keyTopicId(this))
  }

  async deleteRoleBindingEvent(command: DeleteTopicEvent, binding: RoleBinding): Promise<void> {
    let relation: IRelation = binding.inRelation;
    // special case: foreign binding.
    if (binding.realm != this.realm) {
      if (relation.realm != this.realm) {
        const localRelation = await this.realm.localize(relation);
        if (!localRelation)
          throw new Error("localize the relation before editing its bindigs");
        if (
          !(
            localRelation instanceof Relation ||
            localRelation instanceof SimpleRelation
          )
        )
          throw new Error();
        relation = localRelation;
      }
    }
    if (!relation.deleteBinding(binding)) throw new Error("could not delete binding");
    const actor = await binding.actor();
    if (!actor) {
      throw new Error("Could not find actor")
    }
    // TODO: does this take remote actors into accounts

    if (relation instanceof Relation) {
      const simpleType = relation.canBeSimpleRelation();
      if (simpleType) {
        const sRelation = new SimpleRelation(
          relation.id,
          relation.realm,
          simpleType
        );
        await sRelation.recastPhase2(relation);
        relation = sRelation;
      }
    }

    // TODO: maybe the relation itself dies when the last actor of a given role is deleted.
    // Should that be handled in expansion step? I do not think
    // it should be data-dependent... yet rule-based processes will
    // add events in a data-dependent manner.
    const relsCache: ?Set<IRelation> = actor.realm.relationCache.get(actor.id);
    if (relsCache) {
      let found = false;
      for (const binding of relation.bindings()) {
        if (binding.actorRef.id == actor.id && binding.actorRef.realmId == actor.realm.id) {
          found = true;
          break;
        }
      }
      if (!found) {
        relsCache.delete(relation);
        if (relsCache.size == 0) {
          actor.realm.relationCache.delete(actor.id);
        }
      }
    }

    const role = binding.role;
    if (relation.hasType(RealmRegistry.glob.instanceOf)) {
      if (role.hasType(RealmRegistry.glob.target)) {
        for await (let c: BaseTopic of relation.actorsByRole(
          RealmRegistry.glob.source
        )) {
          await c.recast();
        }
      } else if (role.hasType(RealmRegistry.glob.source)) {
        await actor.recast();
      }
    }
    if (relation.hasType(RealmRegistry.glob.subClassOf)) {
      if (role.hasType(RealmRegistry.glob.target)) {
        for await (let c: BaseTopic of relation.actorsByRole(
          RealmRegistry.glob.source
        )) {
          if (c instanceof MetaConcept) {
            c.superTypes(true);
            // recast on all targets?
          }
        }
      } else if (
        role.hasType(RealmRegistry.glob.source) &&
        actor instanceof MetaConcept
      ) {
        actor.superTypes(true);
        // recast on all targets?
      }
    }
  }

  async *expandTranscludeTopicCommand(
    command: TranscludeTopicACommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }
  async applyTranscludeTopicEvent(command: TranscludeTopicEvent): Promise<void> {
    throw new Error("TODO");
  }
  async *expandImportTopicCommand(command: ImportTopicCommand): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const new_id = this.createId();
    const remoteConcept: ?BaseTopic = await command.originalTopic.resolve()
    let topicId = new_id
    if (remoteConcept instanceof Attribute) {
      const localConcept = await this.localize(remoteConcept.target)
      if (localConcept && localConcept instanceof Concept)
        topicId = localConcept.id
    } else if (remoteConcept instanceof RoleBinding) {
      const localConcept = await this.localize(remoteConcept.inRelation)
      if (localConcept && (localConcept instanceof Relation || localConcept instanceof SimpleRelation))
        topicId = localConcept.id
    }
    yield new ImportTopicEvent(
        command,
        new_id,
        topicId,
        command.originalTopic
      );
  }

  async applyImportTopicEvent(command: ImportTopicEvent): Promise<void> {
    const remoteConcept: ?BaseTopic = await command.sourceTopic.resolve()
    const new_id = command.topicId
    if (!remoteConcept) {
      throw new Error("Missing original topic");
    }
    if (remoteConcept instanceof BaseEvent) {
      throw new Error("Cannot import events");
    } else if (remoteConcept instanceof Realm) {
      throw new Error("Cannot import realms");
    }
    let newConcept: BaseTopic;
    if (remoteConcept instanceof Attribute) {
      // TODO: replace remote
      const localConcept = await this.localize(remoteConcept.target);
      if (localConcept && localConcept instanceof Concept) {
        newConcept = new Attribute(
          new_id,
          localConcept,
          remoteConcept.type,
          remoteConcept.value
        );
        // TODO: replace in place!
        localConcept.addAttribute(newConcept);
      } else
        throw new Error(
          "Cannot import an attribute unless the target is localized"
        );
    } else if (remoteConcept instanceof RoleBinding) {
      // TODO: replace remote
      const relation: ?BaseTopic = await this.localize(
        remoteConcept.inRelation
      );
      if (
        relation &&
        (relation instanceof Relation || relation instanceof SimpleRelation) &&
        relation.realm == this
      ) {
        const actorId: ?kId = this.localizeRef(remoteConcept.actorRef)
        const actorRef = (actorId)?new TopicXRef(actorId, this):remoteConcept.actorRef
        newConcept = new RoleBinding(
          new_id,
          relation,
          remoteConcept.type,
          actorRef
        );
        // TODO: Ensure order?
        relation.addBinding(newConcept);
        // TODO: replace in place!
      } else
        throw new Error(
          "Cannot import a binding unless the relation is localized"
        );
    } else if (remoteConcept instanceof SimpleRelation) {
      newConcept = new SimpleRelation(new_id, this, remoteConcept.type);
    } else if (remoteConcept instanceof Relation || remoteConcept instanceof SimpleRelation) {
      const sRelType = remoteConcept.canBeSimpleRelation()
      if (sRelType) {
        // TODO: if not from root realm, localize
        newConcept = new SimpleRelation(new_id, this, sRelType)
      }
      else
        newConcept = new Relation(new_id, this);
    } else if (remoteConcept instanceof Concept) {
      newConcept = new remoteConcept.constructor(new_id, this);
    } else {
      throw new Error("unknown type");
    }
    await newConcept.ensureTypeCache()
    // this concept is involved in remote relations, and maybe inherited ones.
    const remoteRelations1 = remoteConcept.inRemoteRelations || [];
    const remoteRelations2 =
      remoteConcept.realm.relationsOfTopic(remoteConcept.id) || [];
    // console.log("remote relations:"+JSON.stringify(remoteRelations1)+"\n"+JSON.stringify(remoteRelations2))
    if (remoteRelations1.length || remoteRelations2.size)
      newConcept.inRemoteRelations = [...remoteRelations1, ...remoteRelations2];
    // Now look at sourceRealmIds and remoteToLocalIdCache
    newConcept.sourceRealmIds = remoteConcept.sourceRealmIds
      ? new Map(remoteConcept.sourceRealmIds)
      : new Map();
    newConcept.sourceRealmIds.set(remoteConcept.realm.id, remoteConcept.id);
    let remoteCache = this.remoteToLocalIdCache.get(remoteConcept.realm.id);
    if (!remoteCache) {
      remoteCache = new Map();
      this.remoteToLocalIdCache.set(remoteConcept.realm.id, remoteCache);
    }
    remoteCache.set(remoteConcept.id, newConcept.id);
    this.replace(newConcept, remoteConcept.id, remoteConcept.realm);
    this.concepts.set(new_id, newConcept);
  }

  async *expandAddAttributeCommand(
    command: AddAttributeCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    let target: ?BaseTopic = await this.byId(command.topicId);
    if (!target) {
      throw new Error("Missing relation");
    }
    if (!(target instanceof Concept || target instanceof SimpleRelation))
      throw new Error("Cannot add attribute to non-concept");
    let attributeId: kId = (command.replacingAttributeRef
      ) ? command.replacingAttributeRef.id : this.createId()
    yield new AddAttributeEvent(command, attributeId, target.id);
  }

  async applyAddAttributeEvent(
    event: AddAttributeEvent
  ): Promise<void> {
    const command: AddAttributeCommand = event.fromCommand
    const conceptId = event.keyId
    if (!conceptId)
      throw Error("missing conceptId")
    let target: ?BaseTopic = await this.byId(conceptId);
    if (!target) {
      throw new Error("Missing relation");
    }
    if (target instanceof SimpleRelation) {
      // upcast to relation
      const result = new Relation(target.id, target.realm);
      await result.recastPhase2(target);
      target = result;
    }
    if (target instanceof Concept) {
      const attributeType = await command.attributeTypeRef.resolve()
      if (!attributeType || !(attributeType instanceof MetaConcept))
        throw new Error(`Cannot find attribute type ${command.attributeTypeRef.id} in ${command.attributeTypeRef.realmId}`)
      const replacingAttributeRef = command.replacingAttributeRef
      if (replacingAttributeRef) {
        let found = false;
        for (const attributes of target.attributes.values()) {
          for (const attribute of attributes) {
            if (
              attribute.id == replacingAttributeRef.id &&
              attribute.realm.id == replacingAttributeRef.realmId
            ) {
              attribute.type = attributeType;
              attribute.value = command.value;
              found = true;
              break;
            }
          }
          if (found) break;
        }
        if (!found) throw new Error("Could not find replacement attribute");
      } else {
        const attributeId = event.topicId
        const attribute = new Attribute(
          attributeId,
          target,
          attributeType,
          command.value
        );
        target.addAttribute(attribute);
        this.concepts.set(attributeId, attribute);
      }
    } else throw new Error("Cannot add attribute to non-concept");
  }


  async *expandAddRoleTargetCommand(
    command: AddRoleTargetCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    const target: ?BaseTopic = await this.byId(command.topicId);
    if (!target) {
      throw new Error("Missing relation");
    }
    if (!(target instanceof Concept || target instanceof SimpleRelation)) {
      throw new Error("Not a relation");
    }
    const actor = await command.actorRef.resolve()
    if (!actor) {
      throw new Error("Missing actor");
    }
    if (!(target instanceof Relation || target instanceof SimpleRelation)) {
      // check if incompatible type
      if (target instanceof MetaConcept || target instanceof Process) {
        throw new Error("Target cannot be made into a relation");
      }
      // check if simple concept later. First promote to relation.
      // TODO: Add a Simple relation to Relation type. yield this.
    }
    // TODO: if simple relation, and this is to become a non-simple,
    // and type is not just relation, add a relation to type
    const bindingId = this.createId();
    if (!command) throw Error("wtf1")
    yield new AddRoleTargetEvent(command, bindingId, target.id);
    if (actor.realm == this)
      yield new EnterRelationEvent(command, bindingId, actor.id)
  }

  async applyAddRoleTargetEvent(
    event: AddRoleTargetEvent
  ): Promise<void> {
    // topicId: kId;
    // bindingId: ?kId;
    // roleId: kId;
    // roleRealmId: ?kRealmId;
    // actorId: kId;
    // actorRealmId: ?kRealmId;
    // beforeActorId: ?kId;
    // beforeActorRealmId: ?kRealmId;
    const command: AddRoleTargetCommand = event.fromCommand
    if (!command) throw Error("missing command")
    const relId = command.topicId
    if (!relId)
      throw new Error("missing relation Id")
    const target: ?BaseTopic = await this.byId(relId);
    if (!target) {
      throw new Error("Missing relation");
    }
    if (!(target instanceof Concept || target instanceof SimpleRelation)) {
      throw new Error("Not a relation");
    }
    const actor = await command.actorRef.resolve()
    if (!actor) {
      throw new Error("Missing actor");
    }
    const role = await command.roleRef.resolve()
    if (!(role && role instanceof MetaConcept)) {
      throw new Error("missing Role");
    }
    let targetR: IRelation;
    if (target instanceof Relation || target instanceof SimpleRelation) {
      targetR = target;
    } else {
      // check if incompatible type
      if (target instanceof MetaConcept || target instanceof Process) {
        throw new Error("Target cannot be made into a relation");
      }
      // check if simple concept later. First promote to relation.
      targetR = new Relation(target.id, target.realm);
      await targetR.recastPhase2(target);
    }
    const bindingId = event.topicId
    const binding = new RoleBinding(
      bindingId,
      targetR,
      role,
      new TopicXRef(actor)
    );
    const beforeActorRef = command.beforeActorRef
    const beforeActorId = beforeActorRef ? beforeActorRef.id : undefined
    const beforeActorRealmId = beforeActorRef ? beforeActorRef.realmId : undefined
    targetR = await targetR.addBinding(
      binding,
      beforeActorId,
      beforeActorRealmId
    );

    if (targetR instanceof Relation) {
      const bindings: ?(RoleBinding[]) = targetR._bindings.get(role);
      if (bindings) {
        if (beforeActorRef) {
          let i: number = 0;
          for (i of bindings.keys()) {
            const b: RoleBinding = bindings[i];
            if (
              b.actorRef.id == beforeActorRef.id &&
              b.actorRef.realmId == beforeActorRef.realmId
            ) {
              bindings.splice(i, 0, binding);
              break;
            }
          }
          // not found before; we could put at end, but canceling for now.
          if (i >= bindings.length) {
            throw new Error("Could not found position");
          }
        } else {
          bindings.push(binding);
        }
      } else {
        targetR._bindings.set(role, [binding]);
      }
      const simpleType = targetR.canBeSimpleRelation();
      if (simpleType) {
        const targetSR = new SimpleRelation(
          target.id,
          target.realm,
          simpleType
        );
        await targetSR.recastPhase2(targetR);
        targetR = targetSR;
      }
    }
    let relsCache: ?Set<IRelation> = actor.realm.relationCache.get(actor.id);
    if (relsCache) {
      relsCache.add(targetR);
    } else {
      relsCache = new Set([targetR]);
      this.relationCache.set(actor.id, relsCache);
    }
    if (targetR.hasType(RealmRegistry.glob.instanceOf)) {
      if (role.hasType(RealmRegistry.glob.target)) {
        for await (let c: BaseTopic of targetR.actorsByRole(
          RealmRegistry.glob.source
        )) {
          if (c instanceof SimpleRelation) {
            // TODO urgently: upcast to Relation
          } else if (c instanceof SingleTypeTopic) {
            // TODO: if compatible, replace; otherwise throw.
          } else if (!c instanceof Concept) {
            throw new Error("Cannot recast non-concept");
          } else {
            await c.recast();
          }
        }
      } else if (role.hasType(RealmRegistry.glob.source)) {
        await actor.recast();
      }
    }
    if (targetR.hasType(RealmRegistry.glob.subClassOf)) {
      if (role.hasType(RealmRegistry.glob.target)) {
        for await (let c: BaseTopic of targetR.actorsByRole(
          RealmRegistry.glob.source
        )) {
          if (c instanceof MetaConcept) {
            c.superTypes(true);
            // recast on all targets?
          }
        }
      } else if (
        role.hasType(RealmRegistry.glob.source) &&
        actor instanceof MetaConcept
      ) {
        actor.superTypes(true);
        // recast on all targets?
      }
    }
    this.concepts.set(bindingId, binding);
  }
  async applyEnterRelationEvent(event: EnterRelationEvent): Promise<void> {
    // noop
  }

  async applyExitRelationEvent(event: ExitRelationEvent): Promise<void> {
    // noop
  }

  async *expandAddToCollectionCommand(
    command: AddToCollectionCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }

  async applyAddToCollectionEvent(event: AddToCollectionEvent): Promise<void> {
    throw new Error("TODO");
  }

  async *expandRemoveFromCollectionCommand(
    command: RemoveFromCollectionCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }

  async applyRemoveFromCollectionEvent(event: RemoveFromCollectionEvent): Promise<void> {
    throw new Error("TODO");
  }

  async *expandSplitConceptCommand(
    command: SplitConceptCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }

  // async applySplitConceptEvent(event: SplitConceptEvent): Promise<void> {
  //   throw new Error("TODO");
  // }

  async *expandMergeConceptsCommand(
    command: MergeConceptsCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }
  // async applyMergeConceptsEvent(event: MergeConceptsEvent): Promise<void> {
  //   throw new Error("TODO");
  // }

  async *expandUnmergeConceptCommand(
    command: UnmergeConceptCommand
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    throw new Error("TODO");
  }
  // async applyUnmergeConceptEvent(event: UnmergeConceptEvent): Promise<void> {
  //   throw new Error("TODO");
  // }
  async reverseSimpleEvent(ev: BaseEvent): Promise<?BaseEvent> {
    const targetId = await extractTargetId(ev);
    let fromCommand = ev;
    while (fromCommand._fromCommand)
      fromCommand = fromCommand._fromCommand
    switch (ev.inherentTypeNameSafe) {
      case CreateTopicEvent.inherentTypeName:
      case AddRoleTargetEvent.inherentTypeName:
      case AddAttributeEvent.inherentTypeName:
      case ImportTopicEvent.inherentTypeName:
        if (targetId)
          return new DeleteTopicCommand(fromCommand, targetId);
        break;
      case DeleteTopicEvent.inherentTypeName:
        throw new Error("Cannot undelete");
      case AddToCollectionEvent.inherentTypeName:
      // TODO
      case RemoveFromCollectionEvent.inherentTypeName:
      // TODO
    }
  }
}

/**
A Realm that this process fully controls, knowing all the Concepts and Relations.
*/
export class LocalRealm extends ConcreteRealm {
  id_clock: number;
  constructor(id: kId, logStreamSpec: ?IEventStreamSpec, inboxStreamSpec: ?IEventStreamSpec) {
    super(id, logStreamSpec, inboxStreamSpec);
    // this.last_event = null;
    // this.event_reader; // and writer, or same object?
    // this.snapshot_reader;
    // this.query_reader;
    // this.merge_strategy; // how do we merge events from that realm into ours
    // this.node_equivalences = {}
    this.resetLocalRealm();
    // temporary: register all processes. Should look into relations of realm instead.
    for (const process: IProcess of RealmRegistry.getProcesses())
      for (const name of process.canExpand())
        this.registerProcess(name, process);
  }
  reset() {
    super.reset();
    this.resetLocalRealm();
  }
  resetLocalRealm() {
    this.id_clock = 0;
  }

  createId(): kId {
    // Simple-minded implementation. Maybe UUID?
    this.id_clock += 1;
    return this.id + "topic/" + String(this.id_clock);
    // ah, in the case of an event, I would like the event to be immutable
    // and the id to be the hash of the content (minus the ID). So send content?
  }
}

/**
A Realm controlled by another process, of which this process keeps a mirror image
*/
export class RemoteRealm extends ConcreteRealm {
  eventCache: Map<kId, BaseEvent>;
  lastCommand: ?BaseEvent;
  topicSources: ?(ITopicSource[]);
  eventSources: ?(IEventSource[]);
  remoteProcesses: ?(IProcess[]);
  constructor(id: kId) {
    super(id);
  }
  /**
  Subscribe to this realm's event stream
  */
  subscribe(fn: (ev: BaseEvent) => void): void {
    // TODO
  }

  /**
  Propose an event to this realm's stream.
  The event may adapt or reject the base event.
  */
  propose(event: BaseEvent): ?BaseEvent {
    // TODO
  }

  /**
  Get a topic from a remote realm.
  */
  async byId(id: kId): Promise<?BaseTopic> {
    let topic: ?BaseTopic = await super.byId(id);
    if (topic === undefined && this.topicSources) {
      for (const topicSource of this.topicSources) {
        topic = await topicSource.byId(id);
        if (topic) break;
      }
    }
    return topic;
  }
  /**
  Fetch relations. Cache globally.
  */
  relationsOfTopic(topicId: kId): ?Set<IRelation> {
    var relations: ?Set<IRelation> = this.relationCache.get(topicId);
    if (relations === undefined) {
      // TODO: Load from remote
      this.relationCache.set(topicId, relations);
    }
    return relations;
  }
  // search function
  async appendEvent(event: BaseEvent): Promise<kId> {
    // use propose
    throw new Error("Cannot append directly");
  }

  async nextEventId(): Promise<?kId> {
    // TODO: Maybe add to EventSource protocol?
    throw new Error("Unknown");
  }

  async *expandCommand(
    command: BaseEvent
  ): AsyncGenerator<BaseEvent, void, BaseEvent[]> {
    if (this.remoteProcesses) {
      for (const process of this.remoteProcesses) {
        if (!process.canExpand().has(command.inherentTypeNameSafe)) continue;
        let found = false;
        const gen: AsyncGenerator<BaseEvent,
          void,
          BaseEvent[]> = process.expandCommand(command);
        let steps: ?(BaseEvent[]);
        while (true) {
          const r = await gen.next(steps);
          if (r.value) {
            steps = yield r.value;
            if (steps && hasError(steps.values())) {
              if (!r.done) gen.throw(new Error());
              // TODO: reverse saga
              break;
            }
            found = true;
          }
          if (r.done) break;
        }
        if (found) break;
      }
    }
  }

  async *eventsStartingFrom(
    eventId: ?kId
  ): AsyncGenerator<BaseEvent, void, void> {
    if (this.eventSources) {
      let found = false;
      for (const eventSource of this.eventSources) {
        for await (let ev: Object of eventSource.eventsStartingFrom(eventId)) {
          if (ev && !(ev instanceof BaseEvent))
            ev = await this.revive(ev)
          found = true;
          yield ev;
        }
        if (found) break;
      }
    }
  }

  async eventById(id: kId): Promise<?BaseEvent> {
    let command: ?BaseEvent = this.lastCommand;
    while (command) {
      if (command.id == id) return command;
      command = command._fromCommand;
    }
    let event: ?BaseTopic = null;
    // TODO: is there a better way to know if event belongs in a source?
    if (this.eventSources) {
      for (const eventSource of this.eventSources) {
        event = await eventSource.eventById(id);
        if (event) break;
      }
    }
    if (event && !(event instanceof BaseEvent)) {
      event = await this.revive(event)
    }
    if (event && event instanceof BaseEvent) {
      if (event.hasEvents()) this.lastCommand = event;
      return event;
    }
  }

  static async remoteReviver(json: Object, context: JLDContext): Promise<RemoteRealm> {
    // Note that this reviver expects a context rather than a realm
    const result = new this(json["@id"]);
    result.context = context
    result.contextUrl = context.url
    return await result.reviverPhase2(json, context);
  }

  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx);
    const access = json.access;
    this.contextUrl = this.context.url
    if (!this.contextUrl && json['@context'] && typeof(json['@context']) == 'string') {
      this.contextUrl = json['@context']
    }
    if (access.topics) {
      this.topicSources = access.topics
        .map(s => topicSourceFactory(this, s))
        .filter(Boolean);
    }
    if (access.events) {
      this.eventSources = []
      const sources: IEventSource[] = []
      for (const spec of access.events) {
        if (spec.startsWith('rest:')) {
          // this should not be computed here, but upstream
          const matches = this.id.match(/(http.*\/)(\w+)\//)
          if (matches) {
            const stream = await RealmRegistry.getEventStream({
              name: matches[2], data: spec.substring(5), type: "rest",
              url: `${matches[1]}evs/${matches[2]}/`
            })
            if (stream) {
              sources.push(stream)
            }
          }
        }
      }
      this.eventSources = sources
    }
    // if (access.inbox) {
    //   const stream = eventSourceFactory(this, access.inbox)
    //   const stream = await RealmRegistry.getEventStream(access.inbox)
    //   if (stream)
    //     this.inboxStream = stream
    // }
    if (access.process) {
      const remoteProcesses: IProcess[] = [];
      for (const spec of access.process) {
        const p = await remoteProcessFactory(null, this, spec);
        if (p) remoteProcesses.push(p);
      }
      this.remoteProcesses = remoteProcesses;
    }
    return this;
  }
  // event endpoints
  // snapshot endpoint
  // query endpoint
}

async function RemoteRealmFactoryFn(id: kRealmId, remoteStreamSpec: ?IEventStreamSpec): Promise<?Realm> {
  try {
    const response = await axios.get(id)
    // Was revive...
    const ctx: ?JLDContext = await JLDContext.contextFromResponse(response)
    if (!ctx) {
      console.error(response)
      throw new Error("missing context information")
    }
    return await RemoteRealm.remoteReviver(response.data, ctx);
  } catch (error) {
    console.warn(error)
  }
}

RealmRegistry.registerRealmFactory(RemoteRealmFactoryFn);
