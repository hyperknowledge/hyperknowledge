// @flow
import { type kId, type kName, type URLType, type kRealmId } from "./types";
import {
  TopicXRef,
  Realm,
  ErrorEvent,
  BaseEvent,
  SubEvent,
  RealmRegistry
} from "./baseTopics";
import { JLDContext } from "./jsonld"

class TargetTopicEventT<CommandT: BaseEvent> extends SubEvent<CommandT> {
  topicId: kId;
  constructor(command: CommandT, topicId: kId, keyId: kId) {
    super(command, keyId);
    this.topicId = topicId;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<TargetTopicEventT<CommandT>> {
    const fromCommand = await SubEvent.getBaseCommand<CommandT>(json, realm, ctx);
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const keyId = ctx.expandIri(json.keyId)
    const result = new this(fromCommand, topicId, keyId);
    return await result.reviverPhase2(json, ctx);
  }
  get targetId(): kId {
    return this.topicId
  }
}

/**
A concept was created
*/
export class CreateTopicEvent extends SubEvent<CreateTopicCommand> {
  static inherentTypeName = "ecr";
  /**
  In this case, target and key are identical
  */
  get targetId(): kId {
    if (!this.keyId)
      throw new Error()
    return this.keyId
  }

}

/**
a topic (concept, relation, attribute, role binding...) got deleted.
*/
export class DeleteTopicEvent extends TargetTopicEventT<DeleteTopicCommand> {
  static inherentTypeName = "edl";
}

/**
A topic was imported from another Realm.
May have replaced a transclusion implicitly.
TODO: Make that replacement explicit
*/
export class ImportTopicEvent extends TargetTopicEventT<BaseEvent> {
  static inherentTypeName = "eim";
  sourceTopic: TopicXRef;
  constructor(
    command: BaseEvent,
    topicId: kId,
    keyId: kId,
    sourceTopic: TopicXRef
  ) {
    super(command, topicId, keyId)
    this.sourceTopic = sourceTopic
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.sourceTopic = this.sourceTopic.toJSON(null, ctx);
    return result;
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<ImportTopicEvent> {
    const fromCommand = await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx);
    const sourceTopic = await TopicXRef.reviver(json.sourceTopic, realm, ctx)
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const keyId = ctx.expandIri(json.keyId)
    const result = new this(fromCommand, topicId, keyId, sourceTopic);
    return await result.reviverPhase2(json, ctx);
  }
}

/**
A topic from another realm is used in this realm. (Mostly intended for bindings;
otherwise remote concepts are transcluded as actors in local bindings.
*/
export class TranscludeTopicEvent extends SubEvent<BaseEvent> {
  static inherentTypeName = "etr";
  sourceTopic: TopicXRef

  constructor(
    command: BaseEvent,
    keyId: kId,
    sourceTopic: TopicXRef
  ) {
    super(command, keyId);
    this.sourceTopic = sourceTopic;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.sourceTopic = this.sourceTopic.toJSON(null, ctx);
    return result;
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<TranscludeTopicEvent> {
    const fromCommand = await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx);
    const sourceTopic = await TopicXRef.reviver(json.sourceTopic, realm, ctx)
    ctx = ctx || realm.context
    const keyId = ctx.expandIri(json.keyId)
    const result = new this(fromCommand, keyId, sourceTopic)
    return await result.reviverPhase2(json, ctx);
  }
}

/**
An attribute was added to a Concept
*/
export class AddAttributeEvent extends TargetTopicEventT<AddAttributeCommand> {
  static inherentTypeName = "eaa";
}

/**
A role binding (target) is being added to a IRelation (key).
See EnterRelationEvent.
*/
export class AddRoleTargetEvent extends TargetTopicEventT<AddRoleTargetCommand> {
  static inherentTypeName = "ear";
}

/**
The topic entered a new relation. The topic here is still the Binding, but the key is the actor's key.
Usually created at the same time as the AddRoleTargetEvent
*/
export class EnterRelationEvent extends TargetTopicEventT<AddRoleTargetCommand> {
  static inherentTypeName = "eenr";
}

/**
The topic exited a relation. The topic here is the deleted Binding, but the key is the actor's key.
Usually created at the same time as a DeleteTopicEvent
*/
export class ExitRelationEvent extends TargetTopicEventT<DeleteTopicCommand> {
  static inherentTypeName = "eexr";
}

export class AddToCollectionEvent extends TargetTopicEventT<AddToCollectionCommand> {
  static inherentTypeName = "eac";
}

export class RemoveFromCollectionEvent extends SubEvent<BaseEvent> {
  // treat as DeleteTopic on the binding? Not in case of intensional collections.
  static inherentTypeName = "erc";
}

/**
An event from another Realm was explicitly rejected while synchronizing with this realm,
and we want to remember that decision.
*/
export class RejectEvent extends SubEvent<BaseEvent> {
  static inherentTypeName = "ere";
  rejectedEventId: kId;
  rejectedEventRealmId: kRealmId;

  constructor(
    command: BaseEvent,
    keyId: ?kId,
    rejectedEventId: kId,
    rejectedEventRealmId: kRealmId
  ) {
    super(command, keyId);
    this.rejectedEventId = rejectedEventId;
    this.rejectedEventRealmId = rejectedEventRealmId;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.rejectedEventId = this.rejectedEventId;
    result.rejectedEventRealmId = this.rejectedEventRealmId;
    return result;
  }

  static async reviver(json: Object, realm: Realm, ctx?: JLDContext): Promise<RejectEvent> {
    const fromCommand = await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx);
    ctx = ctx || realm.context
    const keyId = (json.keyId) ? ctx.expandIri(json.keyId) : undefined
    const result = new this(
      fromCommand,
      keyId,
      json.rejectedEventId,
      json.rejectedEventRealmId
      );
      return await result.reviverPhase2(json, ctx);
  }

}

export class CreateTopicCommand extends BaseEvent {
  static inherentTypeName = "ccr";
  baseType: ?TopicXRef;

  constructor(realmOrFromCommand: BaseEvent | Realm, baseType: ?TopicXRef) {
    super(realmOrFromCommand);
    this.baseType = baseType
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const baseType = this.baseType
    if (baseType) {
      ctx = ctx || this.realm.context
      result.baseType = baseType.toJSON(null, ctx)
    }
    return result;
  }
  async reviverPhase2(json: Object, ctx?: JLDContext): Promise<this> {
    await super.reviverPhase2(json, ctx)
    if (json.baseType) {
      this.baseType = await TopicXRef.reviver(json.baseType, this.realm, ctx)
    }
    return this
  }
}

export class DeleteTopicCommand extends BaseEvent {
  static inherentTypeName = "cdl";
  topicId: kId;
  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
  ) {
    super(realmOrFromCommand);
    this.topicId = topicId;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<DeleteTopicCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const result = new this(fromCommand || realm, topicId);
    return await result.reviverPhase2(json, ctx);
  }

  }

export class TranscludeTopicACommand extends BaseEvent {
  static inherentTypeName = "ctr";
  originalTopic: TopicXRef

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    originalTopic: TopicXRef
  ) {
    super(realmOrFromCommand);
    this.originalTopic = originalTopic;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    result.originalTopic = this.originalTopic.toJSON(null, ctx);
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
    ): Promise<ImportTopicCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    const originalTopic = await TopicXRef.reviver(json.originalTopic, realm, ctx)
    const result = new this(fromCommand || realm, originalTopic);
    return await result.reviverPhase2(json, ctx);
  }

}

export class ImportTopicCommand extends TranscludeTopicACommand {
  static inherentTypeName = "cim";

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    originalTopic: TopicXRef
  ) {
    super(realmOrFromCommand, originalTopic);
  }
}

export class AddAttributeCommand extends BaseEvent {
  static inherentTypeName = "caa";
  topicId: kId;
  attributeTypeRef: TopicXRef
  replacingAttributeRef: ?TopicXRef
  value: any;

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    attributeTypeRef: TopicXRef,
    replacingAttributeRef: ?TopicXRef,
    value: any
  ) {
    super(realmOrFromCommand);
    this.topicId = topicId;
    this.attributeTypeRef = attributeTypeRef;
    this.replacingAttributeRef = replacingAttributeRef;
    this.value = value;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    result.attributeType = this.attributeTypeRef.toJSON(null, _ctx)
    if (this.replacingAttributeRef)
      result.replacingAttribute = this.replacingAttributeRef.toJSON(null, _ctx)
    result.value = this.value;
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<AddAttributeCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const attributeTypeRef = await TopicXRef.reviver(json.attributeType, realm, ctx)
    const replacingAttributeRef = (json.replacingAttribute
      ) ? await TopicXRef.reviver(json.replacingAttribute, realm, ctx) : undefined

    const result = new this(
      fromCommand || realm,
      topicId,
      attributeTypeRef,
      replacingAttributeRef,
      json.value
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export class AddRoleTargetCommand extends BaseEvent {
  static inherentTypeName = "car";
  topicId: kId;
  roleRef: TopicXRef;
  actorRef: TopicXRef;
  beforeActorRef: ?TopicXRef;

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    roleRef: TopicXRef,
    actorRef: TopicXRef,
    beforeActorRef: ?TopicXRef,
  ) {
    super(realmOrFromCommand);
    this.topicId = topicId;
    this.roleRef = roleRef
    this.actorRef = actorRef
    this.beforeActorRef = beforeActorRef
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    result.role = this.roleRef.toJSON(null, _ctx)
    result.actor = this.actorRef.toJSON(null, _ctx)
    if (this.beforeActorRef)
      result.beforeActor = this.beforeActorRef.toJSON(null, _ctx)
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<AddRoleTargetCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const actorRef = await TopicXRef.reviver(json.actor, realm, ctx)
    const roleRef = await TopicXRef.reviver(json.role, realm, ctx)
    const beforeActorRef = (json.beforeActor) ? await TopicXRef.reviver(json.beforeActor, realm, ctx) : undefined
    const result = new this(
      fromCommand || realm,
      topicId,
      roleRef,
      actorRef,
      json.beforeActorRef
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export class AddToCollectionCommand extends AddRoleTargetCommand {
  static inherentTypeName = "cac";

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    actorRef: TopicXRef
  ) {
    super(
      realmOrFromCommand,
      topicId,
      new TopicXRef(
        RealmRegistry.vocabulary.inCollection,
        RealmRegistry.vocabulary.rootRealm),
      actorRef
    );
  }
}

export class RemoveFromCollectionCommand extends BaseEvent {
  // treat as DeleteTopic on the binding? Not in case of intensional collections.
  static inherentTypeName = "crc";
  topicId: kId;
  bindingId: ?kId;
  actorId: kId;
  actorRealmId: ?kRealmId;

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    bindingId: ?kId,
    actorId: kId,
    actorRealmId: kRealmId
  ) {
    super(realmOrFromCommand);
    this.topicId = topicId;
    this.bindingId = bindingId;
    this.actorId = actorId;
    this.actorRealmId = actorRealmId;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    result.bindingId = this.bindingId;
    result.actorId = this.actorId;
    result.actorRealmId = this.actorRealmId;
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<RemoveFromCollectionCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const result = new this(
      fromCommand || realm,
      topicId,
      json.bindingId,
      json.actorId,
      json.actorRealmId
    );
    return await result.reviverPhase2(json, ctx);
  }

}

export class SplitConceptCommand extends BaseEvent {
  static inherentTypeName = "csp";
  topicId: kId;
  // TODO

  constructor(realmOrFromCommand: BaseEvent | Realm, topicId: kId) {
    super(realmOrFromCommand);
    this.topicId = topicId;
  }
  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    return result;
  }
  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<SplitConceptCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const result = new this(fromCommand || realm, topicId);
    return await result.reviverPhase2(json, ctx);
  }

}

export class MergeConceptsCommand extends AddRoleTargetCommand {
  static inherentTypeName = "cmg";

  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    mergedConceptRef: TopicXRef
  ) {
    super(
      realmOrFromCommand,
      topicId,
      new TopicXRef(
        RealmRegistry.vocabulary.mergedInto,
        RealmRegistry.vocabulary.rootRealm),
      mergedConceptRef
    );
  }
}

export class UnmergeConceptCommand extends BaseEvent {
  static inherentTypeName = "cumg";
  topicId: kId;
  bindingId: ?kId;
  mergedConceptId: kId;
  mergedConceptRealmId: ?kRealmId;
  constructor(
    realmOrFromCommand: BaseEvent | Realm,
    topicId: kId,
    bindingId: kId,
    mergedConceptId: kId,
    mergedConceptRealmId: kRealmId
  ) {
    super(realmOrFromCommand);
    this.topicId = topicId;
    this.bindingId = bindingId;
    this.mergedConceptId = mergedConceptId;
    this.mergedConceptRealmId = mergedConceptRealmId;
  }

  toJSON(key: ?string, ctx: ?JLDContext): Object {
    const result = super.toJSON(key, ctx);
    const _ctx: JLDContext = ctx || this.realm.context
    result.topicId = _ctx.compactIri(this.topicId);
    result.bindingId = this.bindingId;
    result.mergedConceptId = this.mergedConceptId;
    result.mergedConceptRealmId = this.mergedConceptRealmId;
    return result;
  }

  static async reviver(
    json: Object,
    realm: Realm,
    ctx?: JLDContext
  ): Promise<UnmergeConceptCommand> {
    const fromCommand = json.fromCommand
      ? await SubEvent.getBaseCommand<BaseEvent>(json, realm, ctx)
      : null;
    ctx = ctx || realm.context
    const topicId = ctx.expandIri(json.topicId)
    const result = new this(
      fromCommand || realm,
      topicId,
      json.bindingId,
      json.mergedConceptId,
      json.mergedConceptRealmId
    );
    return await result.reviverPhase2(json, ctx);
  }

}
export async function extractTargetId(
  event: BaseEvent
): Promise<?kId> {
  if (event instanceof TargetTopicEventT || event instanceof CreateTopicEvent) {
    return event.targetId;
  }
  if (event.hasEvents()) {
    for await (const ev of event.eventsG()) {
      const id = await extractTargetId(ev)
      if (id) return id
    }
  }
}

export async function extractTargetRealm(
  event: BaseEvent
): Promise<?Realm> {
  if (event instanceof TargetTopicEventT || event instanceof CreateTopicEvent) {
    return event.realm;
  }
  if (event.hasEvents()) {
    for await (const ev of event.eventsG()) {
      const realm = await extractTargetRealm(ev)
      if (realm) return realm
    }
  }
}

/**
All event types. Closed list.
*/
export const simple_event_types: Map<kName, typeof BaseEvent> = new Map()

for (const evtype of [
  CreateTopicEvent,
  DeleteTopicEvent,
  ImportTopicEvent,
  TranscludeTopicEvent,
  AddAttributeEvent,
  AddRoleTargetEvent,
  AddToCollectionEvent,
  RemoveFromCollectionEvent,
  RejectEvent,
  ErrorEvent,
  EnterRelationEvent,
  ExitRelationEvent,
  CreateTopicCommand,
  DeleteTopicCommand,
  ImportTopicCommand,
  AddAttributeCommand,
  AddRoleTargetCommand,
  AddToCollectionCommand,
  RemoveFromCollectionCommand,
  SplitConceptCommand,
  MergeConceptsCommand,
  UnmergeConceptCommand,
  ]) {
    simple_event_types.set(evtype.inherentTypeNameSafe, evtype)
    RealmRegistry.registerReviver(evtype)
}
