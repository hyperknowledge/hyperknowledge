
## Data model, again

We are trying to unify many data models.

1. RDF triples of the form (S, P, O)
2. Wikidata named triples
3. Named graphs, where links may have attributes
4. Tuple spaces
5. Topic maps, where associations are frames with multiple named slots (aka roles). The association is a topic in its own right, i.e. can have literal properties and be an actor in a role. 
    There is a bit of ambiguity: can multiple actors play a single role? (I say yes.) Is the biography of an association attached to the association as a whole or to each binding? I have been arguing for the latter, but I'm now angling towards the former.

Here is what I propose:
First, use the richest possible model (with one exception.)
So I'm proposing we essentially use the topic maps model, with the association as a frame. I now think it should be possible to refer to the association as a whole, not one binding at a time.

In Javascript, we'd have something inspired by Json-LD:

```json
  {
    "prop1": {"@value" : "text", "@lang": "en"},
    "role1": "topic 1 ID",
    "role2": ["topic 2 ID", "topic 3 ID"],
    "type": "association type ID"
  }
```

Second, I'm proposing we use the IPLD trick: Canonicalize this JSON, calculate a strong hash (SHA256), and we get an URN based on a hash. (We'll probably use a `hk:` custom scheme, but I'm considering using IPLD directyl.) This URN can be used as an identifier in further constructions. This allows us to refer to any association by name; if we import RDF data, we can essentially take the properties used for triple reification as a basis for constructing the triple's URN. Because of the hk: namespace, we know it's the identity of a topic. It does mean that the association is immutable; it should only contain the bare minimum of identifying properties, generally none.
Using this trick, we can even use a standard triple store, in theory; or at least export meaningfully to a RDF representation. (In practice, we'll want to use a quint-store, see below.)

probably aux table with indexed URIs so we can use numeric IDs in arrays of URIs?

```sql
CREATE TYPE iri_type AS ENUM ('basic', 'stream', 'literalref', 'assoc', 'type', 'assoc_type', 'role_type', 'literal_type', 'agent', 'collection');
CREATE TYPE event_types AS ENUM ('assoc', 'topic_merge', 'topic_unmerge', 'stream_merge', 'stream_unmerge', 'composite', 'add_to_collection');  # incomplete. Maybe should not be a closed list.

CREATE TYPE event_ref AS (
    stream bigint,
    event_id bigint # serial per stream...
);

# allows to use numeric id's in arrays. Never update or delete.
CREATE TABLE iri (
    id bigserial PRIMARY KEY,
    iri varchar NOT NULL,
    content_hash bytea(16),  # Generated? not sure about this.
    reftype iri_type NOT NULL DEFAULT 'basic',
    UNIQUE(iri),
    UNIQUE(content_hash)
);

# Maybe create faster specialized indices for each reftype? Prob. not worth it.

CREATE TABLE literals (
    id bigint PRIMARY KEY REFERENCES iri,
    # constrain on reftype='literalref' in trigger
    type_id bigint REFERENCES iri, # constrain on reftype = literal_type
    locale varchar(12),
    hash bytea(16) NOT NULL, # generated?
    blob content,
    # maybe add translation reference data...
    UNIQUE(hash)
)

CREATE FUNCTION to_event_refs(bigint, bigint[]) returns event_ref[]
AS
$$
DECLARE
   stream_id ALIAS FOR $1;
   event_ids ALIAS FOR $2;
   retVal event_ref[];
BEGIN
   FOR I IN array_lower(event_ids, 1)..array_upper(event_ids, 1) LOOP
    retVal[I] := (streamId, event_ids[I]);
   END LOOP;
RETURN retVal;
END;
$$
LANGUAGE plpgsql
   STABLE
RETURNS NULL ON NULL INPUT;

CREATE TABLE events (
    stream_id bigint references streams on delete cascade,
    event_local_id bigint,
    ref_id event_ref GENERATED ALWAYS AS (stream_id, event_local_id),
    event_type event_types NOT NULL,
    creation timestamp,
    creator bigint NOT NULL references iri, # constrain on reftype='agent'
    frame_content bson,
    target_collection bigint references iri,
    frame_hash bytea(16) GENERATED ALWAYS AS hashContent(frameContent) STORED,
    frame_ids ARRAY(bigint) GENERATED ALWAYS AS extractRefs(frameContent) STORED,
    replaces_events ARRAY(bigint), # event_local_id in the same stream
    replaces_assocs ARRAY(bigint) references iri,  # constrain on reftype="assoc". May be redundant with previous.
    PRIMARY KEY (stream_id, event_local_id), # or use ref_id? I doubt it
    # UNIQUE (stream_id, timestamp), ?
);
CREATE INDEX stream_topics_idx ON events using gin(stream_id, frame_ids);
CREATE INDEX stream_replaces_evt_idx ON events using gin(to_event_refs(stream_id, replaces_events));

```
(Note that event full ID must be a dereferenceable URL with the eventLocalId as suffix.)

But what if there's dissensus on properties? I must speak of the claim as made by X, vs the claim as made by Y. Is it the same claim? Let's push further. 

possibility 1: It's possible to create a tuple reference: `<claimID, roleID>` (to refer to a binding); or `<topicID, streamID>` to refer to the topic as it emerged in a certain stream (i.e. to distinguish by provenance); `<topicID, streamID, eventID>` for event-specific references. So we'd have composite references (and we'd need to specify how a composite is comparable with the bare topic reference.)
possibility 2: We can say that a claim is a contending replacement for another claim, and put the contentious attributes in the replacing claim. So if you want to distinguish two versions of the same tuple, make the distiguishing factor part of the tuple. (Actually... easy enough to add a distinguishing property, such as stream-of-origin.)

Another issue: Given topics `A, B, C, D, R=#(r1:A, r2:B)`; given `C =~ A`; we should get `S = #(r1:C, r2:B) =~ T`.
Do we do this eagerly or on-demand? A may be involved in a ridiculous number of frames, and surgery is hard to reverse (whereas equivalence should be easy to reverse.) So I would like to say on demand. The equivalence relation should always have a privileged representative. It does mean that any hk: topic has to be normalized to hashes of constructions of those priviledged representatives, which is a non-trivial operation. It can obviously be done at the input level, but doing it at the indexing level is painful. I think... we probably need a materialized on-demand equivalence table, so we can join on that at search time. It means doing the normalization eagerly, but on a table of equivalence, vs. every occurence in the indices.

so another table:

```sql
CREATE TABLE equivalences (
    stream_id bigint references iri NOT NULL,
    topic_id bigint references iri NOT NULL,
    canonical_id bigint references iri NOT NULL,
    invalidated_in_event bigint,
);
```

Adjust continuously, ideally with DB functions. Maybe a materialized view for the topic in 1st pgsql implementation.
If equivalence varies in time, which events do I want to select?
ok, say topic A is identified with topic B between T1 and T2.
If I'm asking for B events, which A events should I receive?

If we're asking for the B snapshot contents, I would say none, except for the unmerge event itself, so that a further re-merge would know the history and reasons for the unmerge (and can replace it.)
If we're asking for the full B history... I would probably want all events before T2, because it's what I'd have received if I had asked before T2. Hence the `invalidated_in_event` column

## References

A reference to a topic can be local, foreign, global, perspective-bound, timebound... 
Given a reference, we need to know which of those is true.
Local means bound to a local URL, foreign means bound to a known foreign URL, maybe global is indefinite?
Not sure that a realm-local but not perspective-local ref is useful, but it's still a frequent case in practice.
Use http headers like memento? Not good for IPLD, but that can use another mechanism.
Two basic operations:
1. Create a local cache of a slice of a foreign stream. Any query can be an extension of an earlier query (i.e. this is what I had, give me what's missing to get X)
Subscription is a basic-ish operation, but start with query. 
Maybe order queries by priority? Eg update ideas before posts. But I think that's for later.
2. Find a way to do distributed queries. I.e. who else has events about this topic (or topic pattern??? If you name a pattern, we may be able to distribute the search... Ok that's big.)

This involves a good way to know equivalence classes across streams.
(Streams cannot have multiple IDs. Stream ID must be cryptographic, I guess... so distinct from stream URL?)
(Again in local storage, this means I must quickly go ID+stream -> equiv. classes (ordered by time))
(or stream+id? I think it's good to have all the equivalences of an ID irrespective of stream)

For a server, we can assume references of the form <realm><stream><topic id><event id>...
But it's possible that a given server uses only global URNs. Then what would a stream-aware reference look like? And what, moreover, if it's a distributed process? The idea is that each realm must give a process to go from a reference to that tuple, and there must be a process to get at the realm first if necessary!

(Can we always assume realm != perspective?)

SO realm + ref -> ref-tuple. (probably with normalized TopicID? That requires a round-trip which may be avoidable in some cases. Make that optional.)
In some cases, the ref implies an unknown realm; others not. There must be a way to sniff the optional realm from a ref.

Similarly, there should be a way to build a ref URL from a ref tuple. (Ideally standardized, though it may vary per realm.)

What's a slice? It's a set of patterns, which are equivalence-aware. (Some patterns may be path patterns.)
Patterns may refer to sets, and sets may grow through the pattern. (Ideally monotonically...)
## What does the query language look like?

Named collections.
Variables.
Typing.
paths.
So a pattern is:

1. assoc pattern
[
    v_assoc in assocColl1 (type: AssocTypeColl1)
    role1: v_topic1 in topicColl1 (type: TypeColl1)
    role2: v_topic2 in topicColl2 (type: TypeColl2)
]

Note that this gives you all events that build up the role bindings (and probably the typings)
Does it give you any other role of such an assoc?
Only if requested. Idem with attributes of assoc or topic.

2. literal pattern:
[
    v_topic1 (type: TypeColl1)
    prop1: <literal query patterns?>
]

ok... literal query patterns should be as sql, I guess.


3. path pattern
use accretion?
Coll1 = [topic1]
[
    v_assoc (type: someAssoc)
    role1: vt1 in Coll1
    role2: vt2
] (& possibly other conditions) => add vt2 to Coll1
and recurse until a stable point.
(Very close to sql CTE)

Note that each search pattern is materialized! This goes with Gyuri's notion of materializing queries. It allows the assignment of the topic to the collection to have provenance.

But it means that a repeat/similar query must be identified with previous. I'd have to know until when was the collection materialized, and materialize the delta, OR materialize reactively. Still tempting.
Note 2: are collections and other such large-cardinality links in-band (as arrays) as I had planned? Or should I use a secondary table, at least for collections? So is collection membership very, very different from role bindings? What justifies this? OR should I do it the other way around, and have each role binding be a collection?
(if the assoc has an ID, the assoc+role can trivially be a derived collection ID... or more simply a double PK. The role could be null, or very trivial for basic collections.) Still collections are a basic construct in that scenario, and it's good. And proper foreign keys, me like. Of course the stream ID is also there.


Note 3: Collection value is obviously perspective-local. When can I optimize not recalculating? This stream is derived from this one, so the collection is necessarily an addition to that one. Is it the same collection when it exists in another stream? Or should I have an array of streams where the binding applies? I think that this is really premature optimization.

What is not premature optimization is disjunctive normal form, again. If I materialize...
Also in some cases, the union will be the union of the same query over many streams. (Do I ever define conjunctive streams? That's risky because constraint satisfaction may differ.)

How do I deregister a topic of interest? Hmmm... I suspect I cannot. I'd have to deregister interest in the collection.
(I may conveniently forget the previous version of data that was sent to me...)

Note that, in some cases, I'll want to keep a private queue on a remote server.

(Question: Can I make this query language into an extension or variant of GraphQL?)

3. Conjunction

4. disjunctive normal form

What of negation?
Negation in predicates; 
Generic "except" logic.
(so instead of disjunctive normal form, use disjunction of
 C1 except C2, C3 except C4, etc.
ok, question : do I need to materialize role bindings? Probably.

question 2: When an event is imported in a new stream, do I duplicate the event? The issue is that I may need to adapt the canonical ID for that stream. BUT that is also true even within-stream, as topics may be renamed after the event...
if there is a fusion, do all previous events appear in-scope? Do they disappear after defusion?
