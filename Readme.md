
## Status

This is work in progress, we do not have a full specification yet. This repository contains earlier experiments, both as code and attempts at formalizing the specification. We want to host a conversation on the design choices as we formalize this.

## Motivation


We want to establish a Web of Knowledge. This allows to see how any
idea is connected with other ideas in rich conversations, through
evidence, citations, influence, consequences, etc. There have been, and
still are many attempts to create such a Web of Knowledge, and rather
than be one more such attempt on our own, we want to help unify the
forces of knowledge workers, both individuals and organizations, into an
interoperable mesh of knowledge where everyone can contribute. We also
want to make it practical to develop knowledge applications that consume
and expose knowledge on this mesh.



We want to create a protocol that supports interoperability of
knowledge at personal and organisational levels and federation of
knowledge sources so that knowledge can be synthesised to augment our
ability to learn, understand, to be understood, solve problems and
achieve goals.

### Knowledge representation

Many systems exist to represent knowledge; the semantic web is the best
known, but has many predecessors and successors. It is a stated goal to
be able to represent anything that can be represented in those systems,
though doing so may require added layers at time, as we strive to
identify a common core rather than an addition of features.

#### Topics and statements

At the conceptual level, we are dealing with units of thought but also
of discourse. We speak of *topics*, something about which something can
be said; and that something is a *statement* about the topic. Those will
be fundamental units of our system.

When speaking about a topic, it is not uncommon to discover that we had
different meanings for a single name, or two names for a single meaning.
We will not attempt to model meaning as independent objects, but will
allow complex identity relationships between topics coming from
different perspectives.

#### Situated in perspective and time

All statements are enunciated by someone, at a given moment, and apply
to a given context. Elements of context include the time when a
situation happened, hypothetical situations, statements or beliefs
attributed to someone else, etc. We will model those elements of context
and origin of discourse as a perspective. Moreover, the perspective
changes in time, as new statements are made or taken into account. Thus,
when referring to a topic, the set of relevant statements (and so the
implicit meaning of the topic) will depend on identifying the
perspective, and a point in the flow of statements of that perspective
(and in the flow of statements of each perspective that the primary
perspective is aware of.)

### Ecosystem

The goal is that any component on the ecosystem can both expose and
consume knowledge, and enrich their own knowledge from the knowledge
developed in other components. We believe that knowledge is both
situated and dynamic; elements of knowledge are enriched one statement
at a time, from an explicitly stated perspective. Knowledge applications
must consume knowledge as a flux of situated statements, and be able to
combine those disparate sources of knowledge elements into a coherent
view, when possible, or point out incoherences in a meaningful way so
people can work on resolving them.

#### Decentralized

One aspect of this ecosystem is that it should allow knowledge workers
to work on their own data, controlling access without depending on
centralized servers. For this reason, we are looking at solutions from
the decentralized Web.

On the other hand, we do not want to exclude centralized servers, which
can allow data enrichment and data mining. We want to balance this data
sovereignty with controlled partial access and
discoverability.

### Branch and merge model of collaboration

We aim to encourage collaboration on knowledge models, and are inspired
by techniques of software development. It should be easy to create a new
perspective based on one or many existing perspectives, make some local
changes, and reintegrate those changes in the target
perspectives.


## Approach

We propose a specific model to achieve those goals; it borrows directly
from many existing techniques and models which it intends to
inter-operate with. It is certainly not the only model that could
interoperate with the models identified, but we have tried to balance
expressivity and terseness.

### Event sourcing

The whole data model will be implemented using event sourcing. Multiple
systems, each carrying multiple event queues, will each make certain
statements about topics. The event layer has to work independently of
the statement model. Different software processes will each carry one or
multiple event queues, and share events about topics between those
queues. We are looking at distributed web implementations of those event
queues, but that should be implementation dependent. What needs to be
shared by implementations is the following:

1.  Any software process must be able to query for events from one of the event queue of another software process. There may be many concrete transport layers to allow this. We need to express the transport layer in a URL that describes the transport queue endpoint.

2.  This query should also allow subscription

3.  The mechanism should have mechanisms for merging event queues (actually through event references) and eventually rebasing event queues (though this should not be necessary in general, as events are expected to follow CRDT rules.)

4.  This mechanism must allow partial queries (and subscriptions) to a subset of events that depend on the underlying event structure in a well-defined way, as developed below.
### Queue slicing

1.  This partial subscription must be rich enough to allow specific mechanisms described below
2.  This partial subscription must be simple enough that subset computation does not become a DOS vector
3.  It is unclear whether cost sharing considerations should be introduced at this layer
4.  It is likely we will have to agree on shared named algorithms (see notion of closure below)

what's important is the possibility to define a Queue slice. Topics are one slice axis.
Event types are another.
A smart queue can optimize some slice algorithms, such as topics.
So SliceableQueue?
Do we allow arbitrary slice functions?
Do we allow the slicing generator to change the slice definition midway? (We decided against this)
Another qn. is mergeability above or below sliceability? It seems they're traits at the same level.
I can do slicing badly on merge, or merge less badly on slice.
Ok. 1st: slices have to be unambiguous and immutable. So I cannot say "events up to time T on `my topics`" without
fixing the topic set at the same time. If the topic set changes, it has to be part of the merge operation.
Time and topic set has to increase monotonically, I think. I cannot "forget" a topic. (Is that so?
Basically it would allow foreign events to come in and out of scope. We could do that explicitly, I guess.
Maybe make the notion of fixed point explicit? It is explicit in other good systems.
A fixed point is a more useful concept than a transaction in a CRDT context!
And it allows tentative endless computations: Can you reach a fixpoint in X amount of gas

#### Merge algorithm

At any point in the event queue, an event may be a merge event. In that
case, it refers to events in another event queue (preferably in the same
software process) up to a certain position in that queue. Those events
are now counted as part of the queue. Some of those events may also be
merge events, and would add to the set of included events. If multiple
merge events mention the same queue, the resulting position is the
maximum of included positions. At any point, it should be possible to
obtain the reference position in each event queues of perspectives known
to a given perspective.

In the case of queues in another software process, the best practice
would be to make a local copy of an appropriate subset of the foreign
queue, and to refer to that copy in the internal inter-queue merge
events. This copy may involve an adapted version of events, in which
case a reference to the original event must be part of the adapted
events.

The bottom line is that a process, given an event queue and a position
in that event queue, must be able to quickly calculate the set of
relevant foreign events from all event queues referenced up to that that
position.

### Data model

As mentioned, statements about topics are at the heart of the model.
Statements are about either literal properties of topics, or (n-ary)
relations between topics (following Topic Maps, as described in
[TMRM07]).



Following Transitional Modeling [Rön18], statements actually combine
a posit, which is the content of the topic, with a reliability valuation
(in the range [-1, 1]), a perspective (extending Rönnbäck's positor),
and an assertion time (given as a position in the perspective's event
stream.)

However, we have chosen to not follow Rönnbäck's decomposition of posit
into value and dereferencing set of appearances; such decomposition is
valuable, but a specific optimisation that should be left to
implementations, and need not be reflected into the transport
layer.

#### Topics as names

The elementary data unit is the (immutable) statement, not the topic
about which a statement is made (though the statement can also be a
topic.) Topics are names that the statement refers to, and the (mutable)
state of the topic in a given event queue (representing a perspective,
see below) is simply the set of active statements mentioning that topic
at any point in the event queue.

Following RDF, names are URIs (usually grounded in a realm URI, q.v.).
However, there is no absolute requirement that the URI should be
dereferencable and yield the state of the topic. It is good practice to
do so, but in that case the resulting state snapshot MUST include (in
internal data or HTTP headers) enough information to discover the event
queue, the position in the event queue, and possibly other parameters
(process version, closure specification, etc.) from which this snapshot
was generated. (At bare minimum, an immutable reference to the last
relevant event in the event chain might yield enough
information.)

Note that the topic might be grounded in an immutable data core: either
a statement, a document, a phrase, etc. It is possible to use a hash of
that identity core as the topic name, as an implementation strategy. But
the topic itself is to be understood as evolving with each statement
that is made about it, and as such should not be confused with its seed
data. The name is, in effect, a reference to a process that can give us
the active set of statements at a given time, either as a snapshot or as
a dereferencable event queue.

Statements are made about the concept as it exists at the moment the
statement is made (or included in a perspective), but understood to
potentially apply to the topic as it evolves, unless explicitly
specified otherwise. (It would be possible, and perhaps desirable, for a
statement reference to include both the name to the evolving state and
the identifier of the state snapshot.) When a topic undergoes major
changes, it is good practice to review existing statements in the
perspective to see if any need to be updated or invalidated.

Note: in [other
work](https://github.com/ipfs/camp/blob/master/DEEP_DIVES/47-dynamic-dag-traversal.md),
I make the opposite choice, and the reference is to the immutable state
by default, and references to the updated state must be made explicitly.
I believe that, though mutable references are more error-prone, as long
as updates are an explicit operation, this is a controlled risk
justified by the cost of updating all references to a frequently moving
topic explicitly in the alternative. Also, reference to immutable state
requires that state to be materialized, which is not a design decision
we take for granted in an event-sourcing paradigm.

##### Philosophical note

In some cases, it makes sense to speak of the immutable core identity of an unambiguous topic. Natural numbers come to mind. Then, statements made about the concept could refer to its immutable core rather than to an always-mutable set of statements made around that concept. Though that is our basic intuition about basic communication, I believe it is wrong in general. Even natural numbers are hotly debated in the appropriate specialist venues. I do think that ideas are actually affected every time we have a new thought about them. And in practice, that means that previous statements may no longer be relevant. On the other hand, we have to be pragmatic; in most cases, previous statements do remain relevant and checking them all every time a new utterance is made is utterly impractical. More important, the notion of consistency itself is a topic of debate among logicians.

This is why I make consistency checking a perspective concern, rather than a protocol-level definition, and I expect all previous statements to still apply to the revised topic, unless specified otherwise. It would be possible to distinguish a set of core characteristics, the modification of which would mandate checking previous statement's continued applicability; but that, again, is a perspective-level choice of strategy for internal consistency checking. In topic maps, identifying attributes are posited to be immutable, and thus constitute such an immutable core. We should probably create utilities that make that specific strategy especially convenient, as it is a particularly reasonable one. However, we will not assume, as IPLD or InfoCentral does, that there is a core immutable concept identity, and that statements always refer to.

#### Collection casting

In some models, it is assumed that a single topic will play a given
role in a relation. Topic maps call the binding of a topic to a relation
in a given role the casting, and we follow this usage. However, in many
cases, many topics can play a single given role, and so we distinguish
the common single casting from the more generic collection casting. A
collection can be defined by intension (as a query, which is not
guaranteed to terminate) and/or by extension (as a list) or both. A
collection defined by extension can be Open (not necessarily exhaustive,
as in RDF's open world hypothesis), Exhaustive, or Ordered (which
implies exhaustive). Singletons are left as a special case of exhaustive
collections.

#### Perspectives

A statement is made by an agent at a certain time, from a certain
perspective. The statement also applies to a certain time, which is
distinct from the time of articulation. The notion of perspective is not
unlike that of mental space [Fau94]; an agent can hold multiple
perspectives, for example subjunctive worlds or mental image of
another's agent's perspective. We will focus on perspectives as
communicational artefacts; a perspective will be defined by a sequence
of statements that are said to hold in that perspective, about a certain
number of topics relevant to that perspective. The notion of perspective
can also be compared to subgraphs in conceptual graphs
[Sow08].



Perspectives are modeled as a set of topics under consideration, and a
stream of "statement events" that pertain to those topics.

#### Named statements and reification

It is possible to make statements about statements, i.e. statements are
themselves topics. To interoperate with many systems that do not allow
this, it is necessary to allow for explicit transition from an anonymous
statement (referred to by content) to a named (reified) statement. More
specifically, it is possible to traverse from any given topic to its
property valuations, to its castings, to the castings it is party to,
etc. It is understood that such constructs may not be given a name
(reified) at the outset, depending on realm implementation; but it must
be possible to reify them, i.e. to make them into named topics, about
which statements can be made, when desired.

### Realms

Because we aim to interoperate with many different systems, we want to avoid strong assumptions about how topics (and statements) are named. Different systems, or different instances of different systems, can have their own naming scheme for topics. Being able to translate between multiple local naming systems is the main goal. It must be possible to express a realm's identifier as a URI, to enable interoperability with RDF. Such realm-local identifiers should ideally be rooted in an identifier for the realm itself. Besides realm-local identifiers, topics may be referred to using global names that do not mention a specific realm. In that case, translating such names to realm-local identifiers is normally a Perspective concern. A specific event type declares the equivalence of a global topic name to local topic identifier.

Some realms may choose to use global names as identifiers; though this is possible, it must be possible for a realm to refer to a given topic as understood in a given target perspective (within the same or another realm) . If both realms use global names for topic identifiers, these localized foreign references will become harder to interpret, so this strategy is discouraged. (OR: allow for full references, i.e. including persective and position, grounded in a global name... but that means canotical decomposition. maybe using a Realm regexp?)

Another realm responsibility is holding localized copies of remote event queues. If an event queue within a realm merges elements from an event queue within another realm, it is risky to expect the other realm to remain available, and more reasonable to make a local copy of that remote queue. That local copy may be total or partial (with an event subset corresponding to a subset of topics), and may be integral or localized, i.e. using local identifiers.

note: here I am conflating two notions in realm: namespace and server. It makes sense because local names depend on implementation. BUT can two perspectives on a same server choose different global names, and hence base themeselves on different namespace resolution? I said that merge equivalence and name equivalence are tied, but are they? local names are optional, after all. (Not fully: non-global names are optional, but canonical names are important. er... are claim signatures based on local names, losing all generality?)
When sharing events, it is implicit that the event is stored using local names, and possibly even displayed with such names, but that global names for the topics are provided alongside the event queue.

So when considering topic references, one has to consider the following:

1. Realm-local topic identifiers
2. Foreign identifiers which are local to another realm; the local realm should define a local name for the foreign topic as used remotely.
3. Global names associated to a local name (through an event, at the perspective level)
4. Topic fusion and fission

#### Topic fusion and fission

It is possible for topics that were defined independently to be found to be the same concept at some level. In the topic mapping world, this happens automatically when two topics share certain core attributes (as defined in a legend, at application level.) (It is known as topic merging.) Another especially frequent case is when asserting that a topic in a foreign realm is the same as a topic local to this realm. But more generally, it is possible to discover the identity of two topics through the process of information accumulation.

A special association is used to associate topics to one another. This defines an equivalence relation, and any statement made about any topic in the equivalence group is expected to hold for all topics in the same group. Note that groups are event-dependent, and hence exist within the scope of a perspective. In topic maps, the equivalence group is materialized as a single "virtual proxy"; subsequent statements may refer to that proxy directly. (Not doing so is especially problematic if the statement is attached to a single non-virtual topic, and that topic is later un-fused from that virtual proxy; then we lose the intended association of that statement to other topics in the virtual proxy.)

There is a similarity between topic equivalence class and abstraction, where two topics are subtypes of a common abstraction, and statements made about the abstraction are expected to apply to the subcases; but statements made about the sibling subcases apply in equivalence classes and not in abstractions.

Note: Because we're working on immutable events, we're considering making virtual proxies hierarchical, although that is not strictly required.

The opposite of topic fusion is fission: discovering that two distinct concepts had been conflated, and creating a new distinct topic for one of them. (It would generally make sense to declare that the original topic is now a common abstraction, so most common statements continue to refer to that abstraction, and to create two or more new topics, redefining specific statements at that level and invalidating the common statements for those differentiating properties.

#### References

There have been many attempts to create global namespaces for many
topics; in hyperknowledge, all these namespaces exist as so many realms,
and "global" names can be registered as aliases for topics in other
realms. However, naming the topic using its identity is only a small
part of identifying a topic. A topic is always situated in a
perspective, and considered at a moment in time (i.e. at a point in the
event stream)

A hyperknowledge URL can exist in any protocol, but its later segments
should follow the following pattern:

    topic_url = <realm_url> '/' <topic_id> (<perspective> <position>? <path>?)?
    topic_id = \w+
    perspective = /\w+
    position = <timestamp> | <event_id>
    timestamp = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?'Z
    event_id = /e_\w+
    path = /[pqrst]\?\w+


the timestamp should be in iso-8601 UTC format.

The fragments are interpreted as follows:

In the simplest case, we only name a topic. (Single
fragment.)

In all other cases, we should name the perspective, so we are speaking
about a set of active statements. These statements define various
properties belonging to the named topic.



The set of active statement can be restricted by position, i.e. either
an event identifier, or a timestamp. In either case we are restricting
the set of active statements with a maximum position in the event
queue.



Further elements allow traversal in the URL, which further allows
reification-on-demand of some statement elements.

The path elements can then traverse from the named topic to unnamed
topics: either a role relation, forwards (r) or backwards (s), indexed
by the role identifier; or a binary property, forward (c) or backwards
(b), indexed by the property identifier; or, in the case where
traversing the role forward leads to a collection, the sublink within
that collection that leads to a given named target topic
(t).

Such elements can be used iteratively.

Whenever such an element is referred to by path operators in a context
where it becomes subject of a new statement, that element must be
reified, i.e. obtain a unique topic identifier. An event will mark the
reification, and rename the unnamed element into a named element. It is
possible that, in some systems (e.g. Blazegraph), the element would
already have a name, and that name was simply not given in the event
stream; in that case, a fake rename event must allow the element's name
to become public.

Note: instead, maybe we should align with memento, and provide an
equivalent mechanism for perspectives. We still need a mechanism for
reification through traversal. (it has side effects, but those should be
idempotent... so GET or POST?)


### Statement event model

The event model is meant to be extensible. But the basis is as
follows:

Event queue model events:

1.  Declare the creation of an event queue in another event queue

2.  Declare that this event queue contains events that result from a subscription to an event queue in another software process (realm) (or alter the subscription in the current event queue)

    a.  question: if we add topics, we need to add copied or translated events. Do we need to rewrite the queue to preserve order? Or should be build a virtual queue composed out of small subscriptions?

3.  Merge: Declare that an event queue (which will belong to a perspective) now includes statements up to a certain position from another event queue

Statement level events:

Most events simply carry statements, but we have a few special event
types for the following cases:

1.  Declare a new topic (Basically a statement that the topic exists)

    a.  Declare a merge relation between certain topics

2.  Make or change a property or casting of an existing topic

    b.  Add a global alias to a topic

    c.  Add or remove targets to a Collection casting

3.  Declare a new perspective (which would be a subtype of event queue creation)

4.  Rename (Change the unique identifier of a topic)

    d.  Reify (Give a new name to a previously unnamed topic)

5.  Alter a ValueTopic's value. (In a way specific to the value type, which could have its own event architecture if a complex type like a document. All must be CRDTs.)

6.  Add topics to the scope of a perspective (which would alter the subscription patterns of upstream mirror perspectives)


Statements have a creator and a point-of-view, which is often implicit (given in the perspective.)

### Concepts, data, documents

Some topics represent data objects (documents or document fragments, images, datasets, etc.) If the data object is mutable, there may be a set of event types defined for alterations to data objects of this type. (Usage of CRDT strategies is strongly encouraged and may turn out to be mandatory.) We will have a dedicated attribute for the data represented by a topic, rather than treat the data and metadata as separate topics.

It will be possible to express complex document structures as a description of a composition of many document fragments, and attendant view specifications. Conversely, it should be possible to create a topic representing a fragment of an existing document-as-topic, much in the same way as we name topics by reification of castings, etc. However, in this case, unlike reification, this may not force a decomposition of the original document into a composite document.

### Queries

From a federation standpoint, different processes must be able to access their respective event queues. Because event queues can grow considerably, it must be possible for one process to ask for a subset of another process' queue, namely all events that mention a given topic, in a certain range of positions in the queue. This is the basic query between processes.

However, a topic is rarely intelligible by itself; the set of related topics in the equivalence class induced by topic fusion, the set of metadata (including for example a typing system with all the ancestors of the type), etc. In general, we have certain designated associations between topics and we want closure of those associations. (It is understood that those closures are bounded; in general we will want all ancestor types but rarely indirect subtypes.) So the query system should allow specifying such closures, and getting all associated events.

Implementation note: Though a basic query language can be based simply on topics referenced in an event, sometimes we'll want more precise definitions. For this, we will probably need filter functions that are shared between peers, that happen to work on the shared event types. This breaks the layering between the event layer and the statement layer, but we can say that the filter functions can be specific to event subcategories (like statements) but the filter function sharing itself can live at the event-sharing layer.

However, it should be possible for the functions, not only to accept or reject events, but to enrich the target sets and closure definitions, so as to minimize communication overhead. (Multiple target sets can be useful, for example to differentiate topics and attribute types.)

But at some point, some of that enrichment is better done by the client. I considered letting the client enrich the sets as the query results come; and/or the server remembering which events have been sent to avoid duplication. A simpler solution is to ensure that the query language has an "except" operator; the client can resend the query with enriched closure target sets, yet give the previous query as a negative target set, and thus get only new events. (The server should still probably cache the results of the previous query, but that becomes an implementation detail.)


There is a totally different layer of graph-level querying that queries complex relationships between topics based on the state of the computed graph. Existing graph query languages (sparql, cypher, gremlin) would certainly need to be adjusted to take optional reification into account. This will be an area of research.

 
### Subscriptions

Processes will often define a set of topics (with appropriate closure operators) that they are interested in, and will expect to be notified of new events that pertain to those topics. Publication transport layers are many, and we expect room for experimentation.

From a client standpoint, anything that can be queried might yield a subscription; but that can create a serious load on the server, and we may want to think about computing capacity and its cost. In some cases, the process creating the queue may publish to a microservice that would select the appropriate subset to publish to the client. Many models will develop.

### Schemas

Some aspects of the schema are inherent to the protocol, in particular anything to do with naming, fusion and fission, fork and merge, associations and castings, collections, etc. But much depends on specific associations and roles, and will be application dependent. Like in RDF (pre schema.org), we do not want to prescribe a single schema to be used by all parties, but we need to provide primitives for schema definition and correspondances between schemas.

### Security

Security needs more consideration than it has received. The assumption is that each realm is in charge of identity and security of its event queues. We expect agents (whether human, corporate or software) to be topics, identified by an unambiguous global name (email address, DID, etc.) Individual events must contain a reference to the agent, but it should be an indirect reference in most cases. This is important for compliance with GDPR; events are immutable and should never disappear, but someone who wishes to be forgotten should be able to sever the association of their identity with the events in some cases. So it should be possible to create a repudiable ad hoc identity, where the fact that the event was once caused by a human agent is irrepudiable.

Each event's payload may be signed, and may be encrypted, and probably should in most cases. We have started thinking about this architecture in the field of public discourse, so we have not given adequate thought to data privacy, but it is a real concern and must be addressed. OTH, we must be aware of the performance cost of signing and encrypting each event.


## Formal model

TODO

## Reference implementations

There are three layers for reference implementations: 

1. we want a reference library that can maintain its own event stream, read a stream of events and reconstruct a set of statements about a topic, taking into account event queue merging, topic fusion, statement invalidation, etc. This library should allow multiple plugins, notably for queue transport mechanism.

2. we want a plugin to that library that can do all this with an event queue maintained in at least one distributed web infrastructure (like IPLD)

3. we want a more conventional client-server architecture for those functions, especially partial queue queries, that will allow sharing data between large-scale knowledge federation applications and smaller personal knowledge applications, or knowledge microservices. (This means creating both a reference knowledge web server and a client for this server that can act as a plugin for the library.) Subscriptions will be defined at this level.

We are hoping we can serve partial queue queries over the distributed web; in that case, the client-server model would only be necessary at very large scale.

### Event layers

#### Graph Event definition

Every event should have the following attributes. (some may be
implicit?)

1.  Realm
2.  Perspective
3.  Previous events (plural, esp. in the case of a merge event)
4.  Timestamp
5.  Event id (lamport clock in realm)
6.  creator id
7.  Main target topic id
8.  Secondary target topic ids (an event will often refer to \>1 topic. Indexing must know this.)
9.  Last known snapshot for main target topic (what about secondary?)
10. A reference to requesting composite event if any


Events below are requests, with result stored in a result event (which
has a success/failure code). Transactionality is given by result event
of top enclosing composite event being a success or failure.

Alternative : We could store complex events in the queue only after
resolution without storing the underlying requests.

####

#### Events and library

There are two main patterns of usage: Either we are doing things in an event queue we control, and we add events that reflect what we are doing (maybe by creating events directly, maybe because API calls to the library are translated to new events); or we want things to happen in an event queue we do not control, and we essentially create a temporary queue of events that represent our pull request to the process that controls the upstream queue. 

In the latter case, there must be a mechanism to signal the pull request to the upstream process. If the upstream process has a server, a POST endpoint should be provided, and authorized clients may even have access to a websocket. If the upstream process is using the distributed web, we would have to agree on a different protocol. (In IPFS, that may mean creating a libp2p protocol, for example.)

When a pull request is sent to another process (presumed to be in another realm), the events are first copied to a shadow queue on the remote process, and a merge is attempted. If the merge is accepted and passes validation, the merge event is added to the main queue and events are now authoritative. (The realm may choose to copy events instead of maintaining a merge event to a shadow queue, especially if the pull request queue is marked as transient, as defined below.) Otherwise, an event on the upstream queue should mark the merge attempt and reasons for failure.

At this point, the requesting application may drop the pull request event queue, if it was meant to be transient, and the shadow queue becomes authoritative. Or, if it maintains its own state with that queue, that queue may remain active.

#### Event queues

* `NewQueue(<Perspective, event_id>*) → TopicId`

    create a new queue based on an existing queue. (Each realm should have a base queue with ground truth about the HK schema.)

* `MergeQueue`

    Import all events from another queue, up to position P (given as a timestamp or event ID)

* `IncludeTopic(target_perspective, source_perspective, topic_id, event_id?) → topic_id`

    States that events about a topic in a merged (source) perspective are now in-scope for this (target) perspective. If the `target_perspective` already knows the `source_perspective`, use the current `event_id`. If the `source_perspective` is foreign, the topic will receive a new `topic_id`

* `NewPerspective() → TopicId`

    Is this different from `NewQueue`? Only if we want perspective layer to be distinct. But perspective also has identity concerns that queue does not have.

* `NewMirrorPerspective(Realm, perspectiveId, TopicId* topics) → TopicId`

    Create a local copy of a foreign perspective, with the specified topicIds mirrorred.

* `MergePerspective(target_perpective_id, source_perspective_id, event_id)`

    Again, probably the same as QueueMerge at the perspective layer. assume all foreign topics have been localized. we need to have a way to merge tentatively and check that constraints are satisfied.

#### Topic identity

* `NewTopic(type) → TopicId`

    Declare topic creation, give it an identifier.
* `FuseTopics(source topics*, existing merge proxy?) → TopicId`

    
* `DefuseTopic(existing_merge_proxy_id, target_topic_id)`
* `SplitTopic`
* `TopicRenamed(oldId, newId)` (result of implicit reification)

#### Statements

* `NewLiteralStatement(source, attribute_type_id, value, replace_statements*) → statement_id`
* `NewCastingStatement(relation_topic_id, role_type_id, target_topic_id, reify?) → reified_casting_id?`
* `RejectStatement` is a case of `AssertStatement` with no value
* `Snapshot(topic_id) → blob_id`  (content-addressable)

#### under consideration (unlikely):

* `ForgetTopic(Perspective, topic_id)`

    (assuming we can not forget a topic in a realm, but if it\'s gone from every perspective... this may fail if it's in the closure of a non-forgotten topic.)

* `ForgetPerspective`

   Not sure if that is necessary? It makes sense for transient pull requests etc. Also there should be a way to "move" events from one queue to another in that case while still preserving temporal order. Might preclude blockchain-ish behaviour.

* `AddReference`

   Inspired by Wikidata references. Is that the same as AddProvenance? Is that too wedded to Wikidata? How is it different from any other qualifier?

* `AssertRelation(source_id, source_role_type_id, target_id, target_role_type_id, relation_type_id, reify_source?, reify_target?) → (reln_topic_id, reified_source_casting_id?, reified_target_casting_id?)`

    may be constructed from `AssertStatement`, but it may still be nice to have a synthetic form as a higher-level abstraction.

* `StatementReified(statement_id) → TopicId`

    Very wedded to implementation. A special case of TopicRenamed. But I want it to be a separate operation. maybe that `statement_id  == event_id`, and as such not an addressable topic. Otherwise use the max of either event id.

* `TranscludeTopic(Target_realm, Source realm, perspective, topic_id, event_id?) → local TopicId`

    Do we need a transclusion operator? Probably not. Transclusion is obtained by a merge from a shadow perspective.
* `ShadowTopic` is a case of merging with a foreign (cross-realm) topic. Do we need to find a way to avoid new statements about imported topics? not anymore.
* `AddQualifier(statement_id, scope_prop_id, value)`

    i.e. scope in TM terms\... Probably not necessary, this is just an attribute/relation applied to the statement.


Forgetting about collection events for now\... but I think I'll need them. Not all collections can be reduced to relations, especially if defined by intension.

### API Layers

#### Security

#### User definition and access

#### Event queues and merging

The first layer is event queues; we need to be able to talk about event queues, individual events (by event ID or timestamp), merging and splitting events, and queue cloning across realms.

Also, events should be typed (where types are named topics) and expose which topics it is about, so we can filter events on topics.



##### Queue API

```javascript
class Event {
    eid: String;  // event id
    refersTo: String[] // topic ids
    type: String;  // type is a topic id
    when: Date;
    who: user_id;

    get_queue(): Queue;  // the origin queue should be inherent in the event
}

class MergeEvent extends Event {
    target: Queue;
    upToEid: String;
}

class DataEvent extends Event {
    payload: object;
}

class StatementEvent extends DataEvent {
    invalidates: List<statement_id>;
}

class Queue {
    id: String;
    constructor(basedOn: Queue?, upToEid: String?);
    async event(eid: String): Event;
    async *eventsUpTo(eid: String?): AsyncGenerator<Event, void, void>;
    async *eventsUpToTime(t: Date): AsyncGenerator<Event, void, void>;
    async *eventsForTopics(topics: String[], upToEid: String?);
    async *eventsForTopicsUpToTime(topics: String[], upToTime: Date);
    async dependencies(eid: String): Map<Queue, eid>;
    async push(e: Event);
}


```


#### Realms





#### Fission, fusion, abstraction

#### Statements

#### Data layer events

#### High-level events


## Interoperability

Hyperknowledge's intent is to be a federation protocol with other knowledge graphs formats and models, each of which makes different assumptions. As such, we have the following goals for interoperability with many specific protocols:

1. Any data residing in a tool using one of these formalisms should be referencable in the hyperknowledge data model
2. Any data described using one of those formalisms should be expressible naturally in the hyperknowledge data model
3. As much as possible, there should be a natural way to express hyperknowledge back in those respective formalisms, though in some cases it may involve extra layers of reification.

### Topic maps

### RDF

### Wikidata

### Graph databases

### AtomSpace

### Transitional modeling

### Prolog and DL


## Syntax

We will use JSON-LD as a base syntax. This is an example file as we develop the formalism.

## Open questions

1.  Is it valuable to special-case single-value castings vs collections of 1? It seems common enough to optimize but adds complexity. Maybe not at protocol level?

2.  Making all casting into collections means asking to specify more. This contradicts the general principle of late formalization. There must be an underdetermined form.

3.  ValueTopics vs Literals... are there MutableValueTopics, or do you change the casting when the value changes? (I tend towards MutableValueTopic, as long as it follows an event structure.)

4.  Should I put perspective in HTTP, as in memento pattern?

5.  I can condense property etc in URL because I'm using ids or names, but not so for the great RDF realm. AND even worse if I need to refer to object (whether full URL of object or literal.) So how? hash of subj/prop/literal?

    a.  Up to a point: Why bother? It is not part of federation, so renaming won't happen. Let me mirror it in a HK-aware realm first. BUT I need to be able to talk about it to the realm using the global identifiers.

    b.  If I reify twice, in different perspectives, do I know I'm reifying to the same value? This matters! Not true if reification is perspective-dependent, as I'm making it to be.

6.  Provisions for computational cost

## Bibliography

[clg18] Information technology - common logic (cl): a framework for
a family of logic-based languages. [Technical Report ISO/IEC
24707:2018(E)](https://www.iso.org/standard/66249.html), ISO/IEC, Geneva, 07 2018.

[DCK17] Felix Dietze, André Calero Valdez, Johannes Karoff, Christoph Greven, Ulrik Schroeder, and Martina Ziefle. [That’s so meta!](https://www.calerovaldez.com/pdf/dietze2017s.pdf) Usability of a Hypergraph-based Discussion Model, in Proceedings of the International Conference on Digital Human Modeling and Applications in Health, Safety, Ergonomics and Risk Management, 2017

[Fau94] Gilles Fauconnier. Mental Spaces: Aspects of Meaning Construction in Natural Language. Cambridge University Press, New York, NY, 1994. isbn: 978-0521449496

[GPG14] Ben Goertzel, Cassio Pennachin, and Nil Geisweiller. Engineering General Intelligence, volume 5-6 of Atlantis Thinking Machines. Atlantis Press, 1 edition, 2014. isbn:978-9462390263 and 978-9462390294

[Rön18] Lars Rönnbäck. [Modeling conflicting, unreliable, and varying information](http://www.anchormodeling.com/wp-content/uploads/2018/12/Transitional_Modeling_DOI.pdf). 12 2018.

[Sow08] John F. Sowa. Handbook of Knowledge Representation, chapter Conceptual Graphs, pages 213--237. Elsevier, 2008. isbn: 9780444522115

[TMRM07] Patrick Durusau, Steven R. Newcomb, and Robert Barta. [Topic maps reference model](http://www.isotopicmaps.org/TMRM/TMRM-7.0/tmrm7.pdf). ISO standard 13250-5 CD, 11 2007.

[Vep19] Linas Vepštas. [Sheaves: A topological approach to big data](http://linas.org/theory/sheaves.pdf). CoRR, abs/1901.01341, 2019.

[Geb19] Chris Gebhardt, [InfoCentral](https://infocentral.org/)


