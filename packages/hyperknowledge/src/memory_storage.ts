import type { eventSpec, eventId, JsonCompatibleR } from './types';
import type { BaseStreamStorage, BaseEventJson } from './events';

type accessor<T> = (ev: BaseEventJson) => T | undefined;

class MemoryStorage implements BaseStreamStorage {
  nextId = 0;
  eventsA: Array<BaseEventJson> = new Array();
  binarySearch<T>(
    start: number,
    end: number,
    val: T,
    acc: accessor<T>,
  ): number {
    if (start == end) {
      return start;
    }
    const mid = Math.floor(start + (end - start) / 2);
    const json = this.eventsA[mid];
    const v2 = acc(json);
    if (v2 === undefined) return -1;
    if (v2 < val) {
      return this.binarySearch(mid, end, val, acc);
    } else {
      return this.binarySearch(start, mid, val, acc);
    }
  }
  toPos(pos: eventSpec): number {
    if (typeof pos === 'number') {
      return pos;
    } else if (pos instanceof Date) {
      return this.binarySearch<Date>(
        0,
        this.eventsA.length,
        pos,
        (json: BaseEventJson) => json.when,
      );
    } else if (typeof pos === 'string') {
      return this.binarySearch<string>(
        0,
        this.eventsA.length,
        pos,
        (json: BaseEventJson) => json.id?.toString(),
      );
    }
    return -1;
  }
  async *events(
    from?: eventSpec,
    until?: eventSpec,
  ): AsyncGenerator<BaseEventJson> {
    const fromPos = from ? this.toPos(from) : 0;
    const untilPos = until ? this.toPos(until) : this.eventsA.length;
    if (fromPos < 0 || untilPos < 0) return;
    for (let i = fromPos; i <= untilPos; i++) {
      yield this.eventsA[i];
    }
  }
  async append(what: BaseEventJson): Promise<eventId> {
    const pos = this.eventsA.length;
    this.eventsA.push(what);
    return pos;
  }
}
