import type { BaseStream } from './events';
import { TopicEvent } from './topics';
import type { eventId, uri, JsonSerializable, JsonCompatible } from './types';

// TODO
class Claim implements JsonSerializable<Claim> {
  toJSON(): JsonCompatible<Claim> {
    return {};
  }
}

export class ClaimEvent extends TopicEvent<Claim> {
  static claimEventTypeId = 'placeholder';
  constructor(stream: BaseStream, claim: Claim, when?: Date, id?: eventId) {
    const topics = ClaimEvent.extractTopics(claim);
    super(stream, ClaimEvent.claimEventTypeId, claim, topics, when, id);
  }
  static extractTopics(claim: Claim): uri[] {
    // TODO
    return [];
  }
}
