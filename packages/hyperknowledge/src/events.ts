import { Subject } from 'rxjs';
import type {
  uri,
  streamUri,
  typeUri,
  eventId,
  eventSpec,
  Json,
  JsonCompatible,
  JsonCompatibleR,
  JsonSerializable,
} from './types';

export class BaseEvent<T extends JsonSerializable<T>> {
  readonly id: eventId | undefined;
  readonly type: typeUri;
  readonly when: Date;
  readonly stream: BaseStream;
  readonly what: T;
  readonly originalEvent?: BaseEvent<T>;

  constructor(
    stream: BaseStream,
    type: typeUri,
    what: T,
    when?: Date,
    id?: eventId,
    originalEvent?: BaseEvent<T>,
  ) {
    // should I have a payload?
    this.stream = stream;
    this.type = type;
    this.when = when || new Date();
    this.what = what;
    this.id = id;
    this.originalEvent = originalEvent;
  }
  toJSON(withoutId?: boolean): JsonCompatible<BaseEvent<T>> {
    const json: any = {
      '@type': this.type,
      when: this.when.toISOString(),
      stream: this.stream.id,
    };
    // note that what will depend on concrete subclasses
    if (!withoutId && this.id) {
      json['@id'] = this.id;
    }
    return json;
  }
}

export type BaseEventJson = JsonCompatible<BaseEvent<any>>;

export interface BaseStreamStorage {
  events(from?: eventSpec, to?: eventSpec): AsyncGenerator<BaseEventJson>;
  append(what: BaseEventJson): Promise<eventId>;
}

export class BaseStream extends Subject<BaseEvent<any>> {
  readonly id: streamUri;
  readonly external: boolean;
  readonly storage: BaseStreamStorage;
  constructor(id: streamUri, external: boolean, storage: BaseStreamStorage) {
    super();
    this.id = id;
    this.external = external;
    this.storage = storage;
  }

  async *events(
    from?: eventSpec,
    to?: eventSpec,
  ): AsyncGenerator<BaseEvent<any>> {
    for await (const evj of this.storage.events(from, to)) {
      const ev = evj as JsonCompatibleR<BaseEvent<any>>;
      // whose job is it to keep track of event types?
      yield new BaseEvent<any>(
        this,
        ev.type,
        ev.what,
        ev.when,
        ev.id,
        ev.originalEvent,
      );
    }
  }
  async append<T extends JsonSerializable<T>>(
    what: T,
    eventType: typeUri,
    baseEvent?: BaseEvent<T>,
  ): Promise<BaseEvent<T>> {
    const when = new Date();
    const jsonOb: Json = {
      what: what.toJSON(),
      when: when.toJSON(),
      type: eventType,
    };
    if (baseEvent) {
      jsonOb['baseEvent'] = baseEvent.toJSON();
    }
    const id = await this.storage.append(jsonOb);
    return new BaseEvent<T>(this, eventType, what, when, id, baseEvent);
  }
  async mergeEvent<T extends JsonSerializable<T>>(
    event: BaseEvent<T>,
  ): Promise<BaseEvent<T>> {
    const what = event.what; // TODO: this.localize(event.what)
    // should I use the original event type, or a cloning type?
    return this.append(event.what, event.type, event);
  }
}
